/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.siani.javafmi.plugin.inspector;

import com.intellij.openapi.actionSystem.DataProvider;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.SimpleToolWindowPanel;
import org.javafmi.framework.FmiContainer.Causality;
import org.javafmi.kernel.OS;
import org.javafmi.modeldescription.ModelDescription;
import org.javafmi.modeldescription.ScalarVariable;
import org.javafmi.modeldescription.v2.BooleanType;
import org.javafmi.modeldescription.v2.IntegerType;
import org.javafmi.modeldescription.v2.RealType;
import org.javafmi.modeldescription.v2.StringType;
import org.javafmi.proxy.FmuFile;
import org.javafmi.wrapper.Simulation;
import org.javafmi.wrapper.variables.SingleRead;
import org.jetbrains.annotations.NotNull;
import org.siani.javafmi.plugin.inspector.chart.Tuple;

import javax.swing.*;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.stream.Collectors;


class FmuExecutionManager extends SimpleToolWindowPanel implements DataProvider {

	private static final Logger LOG = Logger.getInstance("FmuExecutionManager");

	private final Project project;
	private final FmuInspectorView view;
	private FmuFile fmuFile;
	private ModelDescription modelDescription;
	private Simulation simulation;
	private Thread playThread;
	private PlayRunner runner;

	FmuExecutionManager(Project project) {
		super(true, true);
		this.project = project;
		view = new FmuInspectorView();
		initGUI();
	}


	private void init() {
		final String fmuPath = project.getBasePath() + File.separator + view.selectedFMU();
		if (new File(fmuPath).exists()) {
			fmuFile = new FmuFile(fmuPath);
			openFmu();
			if (canExecute()) initSimulation();
			else view.messagePanel().setVisible(true);
		}
	}

	private void play() {
		if (isPlaying()) pause();
		else {
			view.playingView();
			runner = new PlayRunner(view.stepSize());
			playThread = new Thread(runner);
			playThread.start();
		}
	}

	private void pause() {
		runner.stop();
		playThread.interrupt();
		view.pausedView();
	}

	private boolean isPlaying() {
		return playThread != null && playThread.isAlive();
	}

	private void reset() {
		terminate();
		init();
	}

	public void terminate() {
		if (isPlaying()) pause();
		if (simulation != null) simulation.terminate();
		simulation = null;
		modelDescription = null;
		terminateView();
	}

	private void step(double step) {
		setSimulationInput(view.inputVariables());
		simulation.doStep(step);
		final Map<String, Object> output = output();
		final Map<String, Object> input = input();
		view.updateVariables(input, output);
		view.updateSimulationView(simulation.getCurrentTime(), toChartValues(simulation.getCurrentTime(), join(output, input)));
	}

	private void openFmu() {
		view.showFmu(fmuFile);
	}

	private void initSimulation() {
		try {
			simulation = new Simulation(fmuFile);
			modelDescription = simulation.getModelDescription();
			simulation.init(view.startTime());
			initView();
		} catch (Exception | Error e) {
			view.messagePanel().setVisible(true);
		}
	}

	private boolean canExecute() {
		final List<String> operatingSystems = Arrays.asList(fmuFile.getOperatingSystems());
		return operatingSystems.contains(OS.name() + OS.architecture()) || operatingSystems.contains(OS.Java);
	}

	private void initView() {
		view.control().setVisible(true);
		view.enableSimulationTabs();
		final Map<String, Object> input = input();
		final Map<String, Object> output = output();
		view.updateVariables(input, output);
		view.addChartVariables(filteredVariables(input, output));
		view.play().setEnabled(true);
		view.forward().setEnabled(true);
	}

	private Map<String, Object> parameters() {
		Map<String, Object> variables = new LinkedHashMap<>();
		if (modelDescription == null) return variables;
		for (ScalarVariable v : modelDescription.getModelVariables())
			if (Causality.parameter.name().equalsIgnoreCase(v.getCausality()))
				variables.put(v.getName() + ":" + v.getTypeName(), readVariableValue(v));
		return variables;
	}

	@NotNull
	private List<String> filteredVariables(Map<String, Object> input, Map<String, Object> output) {
		List<String> allVariables = new ArrayList<>();
		allVariables.addAll(input.entrySet().stream().
				filter(this::representableValues).
				collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)).
				keySet().stream().map(k -> "Input: " + k.split(":")[0]).collect(Collectors.toList()));
		allVariables.addAll(output.entrySet().stream().
				filter(this::representableValues).
				collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)).keySet().stream().map(k -> "Output: " + k.split(":")[0]).collect(Collectors.toList()));
		return allVariables;
	}


	@NotNull
	private Map<String, Object> join(Map<String, Object> output, Map<String, Object> input) {
		Map<String, Object> allVariables = new HashMap<>();
		allVariables.putAll(input);
		allVariables.putAll(output);
		return allVariables;
	}

	private Map<String, Tuple> toChartValues(double currentTime, Map<String, Object> output) {
		Map<String, Tuple> map = new LinkedHashMap<>();
		output.entrySet().stream().filter(this::representableValues).forEach(entry -> map.put(entry.getKey(), new Tuple(currentTime, (Double) entry.getValue())));
		return map;
	}

	private boolean representableValues(Map.Entry<String, Object> entry) {
		return entry.getValue() instanceof Double || entry.getValue() instanceof Integer;
	}

	private void terminateView() {
		view.messagePanel().setVisible(false);
		view.updateSimulationView(0, Collections.emptyMap());
		view.control().setVisible(false);
		view.tabPanel().setVisible(false);
	}

	private Map<String, Object> input() {
		Map<String, Object> variables = new LinkedHashMap<>();
		if (modelDescription == null) return variables;
		for (ScalarVariable v : modelDescription.getModelVariables())
			if (Causality.input.name().equalsIgnoreCase(v.getCausality()) || Causality.parameter.name().equalsIgnoreCase(v.getCausality()))
				variables.put(v.getName() + ":" + v.getTypeName(), readVariableValue(v));
		return variables;
	}

	private Map<String, Object> output() {
		Map<String, Object> variables = new LinkedHashMap<>();
		if (modelDescription == null) return variables;
		for (ScalarVariable v : modelDescription.getModelVariables())
			if (Causality.output.name().equalsIgnoreCase(v.getCausality()))
				variables.put(v.getName() + ":" + v.getTypeName(), readVariableValue(v));
		return variables;
	}

	private void setSimulationInput(Map<String, String> variables) {
		for (String nameAndType : variables.keySet())
			write(nameAndType.split(":")[0], nameAndType.split(":")[1], variables.get(nameAndType));
	}

	private Object readVariableValue(ScalarVariable v) {
		SingleRead read = simulation.read(v.getName());
		if (v.getType() instanceof BooleanType) return read.asBoolean();
		if (v.getType() instanceof RealType) return round(read.asDouble());
		if (v.getType() instanceof IntegerType) return read.asInteger();
		if (v.getType() instanceof StringType) return read.asString();
		return "null";
	}

	private void initGUI() {
		add(view.getContentPane());
		reloadFmus();
		view.play().addActionListener((l) -> play());
		view.forward().addActionListener((l) -> step(view.stepSize()));
		view.reload().addActionListener(e -> {
			reset();
			reloadFmus();
		});
		view.fmuBox().addItemListener(e -> {
			terminate();
			init();
		});
		if (view.selectedFMU() != null) init();

	}

	private void reloadFmus() {
		final String selectedItem = view.fmuBox().getSelectedItem() != null ? view.fmuBox().getSelectedItem().toString() : "";
		final File[] fmuPath = new File(project.getBasePath()).listFiles((dir, name) -> {
			return name.endsWith(".fmu");
		});
		for (File file : fmuPath) if (!contains(file)) view.fmuBox().addItem(file.getName());
		if (new File(project.getBasePath() + File.separator + selectedItem).exists())
			view.fmuBox().setSelectedItem(selectedItem);
		else view.fmuBox().removeItem(selectedItem);
	}

	private boolean contains(File file) {
		for (int i = 0; i < view.fmuBox().getItemCount(); i++)
			if (view.fmuBox().getItemAt(i).equals(file.getName())) return true;
		return false;
	}

	private void write(String name, String type, String value) {
		try {
			switch (type) {
				case BooleanType.BOOLEAN_TYPE:
					simulation.write(name).with(Boolean.valueOf(value));
					break;
				case RealType.REAL_TYPE:
					simulation.write(name).with(round(Double.valueOf(value)));
					break;
				case IntegerType.INTEGER_TYPE:
					simulation.write(name).with(Integer.valueOf(value));
					break;
				case StringType.STRING_TYPE:
					simulation.write(name).with(value);
					break;
			}
		} catch (Exception e) {
			LOG.error("Impossible to set variable: " + name + " with value " + value, e);
		}
	}

	private double round(double value) {
		return (double) Math.round(value * 100000d) / 100000d;
	}

	private class PlayRunner implements Runnable {

		private final double step;
		private boolean stop = false;

		public PlayRunner(double step) {
			this.step = step;
		}

		@Override
		public void run() {
			while (!stop) {
				try {
					SwingUtilities.invokeAndWait(() -> {
						if (!stop) {
							step(step);
							view.playingView();
						}
					});
				} catch (InterruptedException | InvocationTargetException e) {
					LOG.error(e.getCause().getMessage(), e);
				}
				try {
					Thread.sleep(1000 - view.sleepTime());
				} catch (InterruptedException e) {
					LOG.error(e.getCause().getMessage(), e);
					e.printStackTrace();
				}
			}
		}

		public void stop() {
			this.stop = true;
		}
	}
}
