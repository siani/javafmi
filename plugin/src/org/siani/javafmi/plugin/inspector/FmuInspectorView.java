/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.siani.javafmi.plugin.inspector;

import com.intellij.openapi.ui.ComboBox;
import com.intellij.ui.JBColor;
import org.javafmi.proxy.FmuFile;
import org.jetbrains.annotations.NotNull;
import org.siani.javafmi.plugin.inspector.chart.ChartManager;
import org.siani.javafmi.plugin.inspector.chart.Tuple;

import javax.swing.*;
import java.awt.*;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.siani.javafmi.plugin.model.JavaFMIIcons.*;


public class FmuInspectorView extends JPanel {
	private JPanel contentPane;
	private JPanel inputPanel;
	private JPanel outputPanel;
	private JButton play;
	private JButton forward;
	private JComboBox<String> fmu;
	private JProgressBar progressBar;
	private JTextField step;
	private JLabel time;
	private JSlider slider;
	private JPanel variables;
	private JPanel control;
	private JComboBox<String> v1;
	private JComboBox<String> v2;
	private JComboBox<String> v3;
	private JLabel operatingSystems;
	private JLabel fmiVersion;
	private JLabel generationTool;
	private JTabbedPane tabPanel;
	private JPanel messagePanel;
	private JTextField stopTime;
	@SuppressWarnings("unused")
	private JPanel chart;
	private JButton reload;
	private ChartManager chartManager;

	public FmuInspectorView() {
		initTabs();
		control.setVisible(false);
		messagePanel.setVisible(false);
		play.setIcon(PLAY);
		reload.setIcon(RELOAD);
		forward.setIcon(FORWARD);
	}

	private void initTabs() {
		initTab(0, "Properties");
		initTab(1, "Variables");
		initTab(2, "Chart");
		tabPanel.setVisible(false);
	}

	private void initTab(int index, String text) {
		JLabel label = new JLabel(text, SwingConstants.CENTER);
		label.setPreferredSize(new Dimension(100, 40));
		tabPanel.setTabComponentAt(index, label);
	}

	JPanel getContentPane() {
		return contentPane;
	}

	JButton play() {
		return play;
	}

	JButton forward() {
		return forward;
	}

	double stepSize() {
		try {
			return Double.parseDouble(step.getText());
		} catch (NumberFormatException ignored) {
			return 0;
		}
	}

	void updateVariables(Map<String, Object> inputVariables, Map<String, Object> outputVariables) {
		variables.setVisible(true);
		inputPanel.removeAll();
		final GridLayout layout = new GridLayout(1, 2);
		for (Map.Entry<String, Object> v : inputVariables.entrySet()) {
			final JPanel jbPanel = new JPanel(layout);
			newInputRow(v, jbPanel);
			inputPanel.add(jbPanel);
		}
		inputPanel.updateUI();
		outputPanel.removeAll();
		for (Map.Entry<String, Object> v : outputVariables.entrySet()) {
			final JPanel jbPanel = new JPanel(layout);
			newOutputRow(v, jbPanel);
			outputPanel.add(jbPanel);
		}
		outputPanel.updateUI();
	}

	void addChartVariables(List<String> allVariables) {
		v1.removeAllItems();
		v2.removeAllItems();
		v3.removeAllItems();
		v1.addItem("");
		v2.addItem("");
		v3.addItem("");
		for (String variable : allVariables) {
			v1.addItem(variable);
			v2.addItem(variable);
			v3.addItem(variable);
		}
	}

	Map<String, String> inputVariables() {
		Map<String, String> variables = new LinkedHashMap<>();
		for (Component component : inputPanel.getComponents())
			variables.put(((JLabel) ((JPanel) component).getComponent(0)).getText(), valueOf((JPanel) component));
		return variables;
	}

	private String valueOf(JPanel panel) {
		Component valueComponent = panel.getComponent(1);
		if (valueComponent instanceof JTextField) return ((JTextField) valueComponent).getText();
		if (valueComponent instanceof JComboBox) return ((JComboBox) valueComponent).getSelectedItem().toString();
		return "null";
	}

	private void newInputRow(Map.Entry<String, Object> inputVar, JPanel jbPanel) {
		jbPanel.add(new JLabel(inputVar.getKey()));
		JComponent field = inputVar.getKey().split(":")[1].equals("Boolean") ? createCombo(inputVar.getValue().toString()) : createJTextField(inputVar);
		field.setEnabled(true);
		jbPanel.add(field);
	}

	private void newOutputRow(Map.Entry<String, Object> inputVar, JPanel jbPanel) {
		jbPanel.add(new JLabel(inputVar.getKey()));
		JComponent field = inputVar.getKey().split(":")[1].equals("Boolean") ? createCombo(inputVar.getValue().toString()) : createJTextField(inputVar);
		field.setEnabled(false);
		jbPanel.add(field);
	}

	@NotNull
	private JTextField createJTextField(Map.Entry<String, Object> inputVar) {
		final DecimalFormat formatter = new DecimalFormat("0.######E0");
		final DecimalFormatSymbols decimalFormatSymbols = formatter.getDecimalFormatSymbols();
		decimalFormatSymbols.setDecimalSeparator('.');
		formatter.setDecimalFormatSymbols(decimalFormatSymbols);
		JTextField field = new JTextField(format(inputVar.getValue(), formatter));
		field.setHorizontalAlignment(SwingConstants.RIGHT);
		return field;
	}

	private String format(Object value, NumberFormat formatter) {
		return value instanceof Double ? formatDouble(value, formatter) : value.toString();
	}

	private String formatDouble(Object value, NumberFormat formatter) {
		if (value.toString().equals("0")) return "0";
		String formattedValue = formatter.format(value);
		return formattedValue.endsWith("E0") ? formattedValue.substring(0, formattedValue.length() - 2) : formattedValue;
	}

	private JComboBox createCombo(String value) {
		ComboBox comboBox = new ComboBox(new String[]{"true", "false"});
		comboBox.setSelectedItem(value);
		return comboBox;
	}

	private void createUIComponents() {
		chartManager = new ChartManager();
		chart = chartManager.getChart();
		inputPanel = new JPanel();
		inputPanel.setLayout(new GridLayout(0, 1));
		outputPanel = new JPanel();
		outputPanel.setLayout(new GridLayout(0, 1));
	}

	public void updateSimulationView(double currentTime, Map<String, Tuple> values) {
		time.setText(format(currentTime, new DecimalFormat("#.####")) + "");
		progressBar.setMinimum((int) (startTime()));
		progressBar.setMaximum((int) (stopTime()));
		progressBar.setValue((int) (currentTime));
		chartManager.updateChart(filterSelectedValues(values));
	}

	private Map<String, Tuple> filterSelectedValues(Map<String, Tuple> values) {
		Map<String, Tuple> filtered = new LinkedHashMap<>();
		if (v1.getSelectedItem() != null) filter(v1.getSelectedItem().toString(), values, filtered);
		if (v2.getSelectedItem() != null) filter(v2.getSelectedItem().toString(), values, filtered);
		if (v3.getSelectedItem() != null) filter(v3.getSelectedItem().toString(), values, filtered);
		return filtered;
	}

	private void filter(String value, Map<String, Tuple> values, Map<String, Tuple> filtered) {
		values.keySet().stream().filter(v -> v.split(":")[0].equals(value.replace("Input: ", "").replace("Output: ", ""))).
				forEach(v -> filtered.put(value, values.get(v)));
	}

	public void showFmu(FmuFile fmuFile) {
		tabPanel.setVisible(true);
		tabPanel.setSelectedIndex(0);
		enableSimulationTabs(false);
		operatingSystems.setText(String.join(", ", Arrays.asList(fmuFile.getOperatingSystems())));
		fmiVersion.setText(fmuFile.getModelDescription().getFmiVersion());
		generationTool.setText(fmuFile.getModelDescription().getGenerationTool());
	}

	private void enableSimulationTabs(boolean enabled) {
		setTabPanelColor(1, enabled ? JBColor.BLACK : JBColor.GRAY);
		setTabPanelColor(2, enabled ? JBColor.BLACK : JBColor.GRAY);
		tabPanel.setEnabledAt(1, enabled);
		tabPanel.setEnabledAt(2, enabled);
	}

	private void setTabPanelColor(int i, Color color) {
		tabPanel.getTabComponentAt(i).setForeground(color);
	}

	public double startTime() {
		return 0;
	}

	public double stopTime() {
		return Double.parseDouble(stopTime.getText());
	}

	public int sleepTime() {
		return slider.getValue();
	}

	public String selectedFMU() {
		final Object selectedItem = fmu.getSelectedItem();
		return selectedItem != null ? selectedItem.toString() : null;
	}

	public JComboBox<String> fmuBox() {
		return fmu;
	}

	public JPanel control() {
		return control;
	}

	public void enableSimulationTabs() {
		enableSimulationTabs(true);
	}

	public JTabbedPane tabPanel() {
		return tabPanel;
	}

	public JPanel messagePanel() {
		return messagePanel;
	}

	public JButton reload() {
		return reload;
	}

	public void playingView() {
		play.setIcon(PAUSE);
		step.setEnabled(false);
		stopTime.setEnabled(false);
		for (Component component : inputPanel.getComponents()) (((JPanel) component).getComponent(1)).setEnabled(false);
	}

	public void pausedView() {
		play.setIcon(PLAY);
		step.setEnabled(true);
		stopTime.setEnabled(true);
		for (Component component : inputPanel.getComponents()) (((JPanel) component).getComponent(1)).setEnabled(true);
	}
}
