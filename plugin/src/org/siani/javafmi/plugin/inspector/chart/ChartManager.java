/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.siani.javafmi.plugin.inspector.chart;

import com.intellij.ui.JBColor;
import org.siani.javafmi.plugin.inspector.chart.aw.gui.chart.Chart2D;
import org.siani.javafmi.plugin.inspector.chart.aw.gui.chart.ITrace2D;
import org.siani.javafmi.plugin.inspector.chart.aw.gui.chart.Trace2DLtd;

import java.awt.*;
import java.util.*;
import java.util.List;


public class ChartManager {

	private Chart2D chart;
	private final int size = 200;
	private Set<Color> currentColors = new HashSet<>();
	private List<Color> colors = new ArrayList<>(Arrays.asList(JBColor.RED, JBColor.GREEN, JBColor.BLUE));
	private Map<String, ITrace2D> traces = new LinkedHashMap<>();

	public ChartManager() {
		chart = new Chart2D();
	}

	public Chart2D getChart() {
		return chart;
	}

	private void addTrace(String name) {
		ITrace2D trace = new Trace2DLtd(size);
		trace.setLabel(name);
		trace.setColor(newColor());
		chart.addTrace(trace);
		traces.put(name, trace);
	}

	private void addValue(String trace, double x, double y) {
		traces.get(trace).addPoint(x, y);
	}

	private Color newColor() {
		for (Color color : colors)
			if (!currentColors.contains(color)) {
				currentColors.add(color);
				return color;
			}
		return JBColor.GRAY;
	}

	public synchronized void updateChart(Map<String, Tuple> values) {
		try {
			List<String> toRemove = new ArrayList<>();
			traces.keySet().stream().filter(trace -> !values.containsKey(trace)).forEach(trace -> {
				traces.get(trace);
				currentColors.remove(traces.get(trace).getColor());
				chart.removeTrace(traces.get(trace));
				toRemove.add(trace);

			});
			toRemove.forEach(traces::remove);
			for (String trace : values.keySet()) {
				if (!traces.containsKey(trace)) addTrace(trace);
				addValue(trace, values.get(trace).x, values.get(trace).y);
			}
		} catch (ConcurrentModificationException ignored) {
		}

	}
}
