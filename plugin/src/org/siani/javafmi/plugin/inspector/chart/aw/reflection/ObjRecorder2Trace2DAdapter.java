/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.siani.javafmi.plugin.inspector.chart.aw.reflection;
import javax.swing.event.*;

import org.siani.javafmi.plugin.inspector.chart.aw.gui.chart.ITrace2D;
import org.siani.javafmi.plugin.inspector.chart.aw.util.TimeStampedValue;


/**
 *  A simple adapter that allows displaying of timestamped values from 
 *  an inspection of the <code>ObjectRecorder</code> on a Chart2D. 
 *
 */
public class ObjRecorder2Trace2DAdapter implements ChangeListener {
    ITrace2D view;
    ObjectRecorder inspector;
    String fieldname;
    long start = System.currentTimeMillis();
    public ObjRecorder2Trace2DAdapter(ITrace2D view, Object toinspect, String fieldname, long interval) {
        this.view = view;
        this.fieldname = fieldname;
        this.view.setLabel(new StringBuffer(toinspect.getClass().getName()).append(toinspect.hashCode()).toString());
        this.inspector = new ObjectRecorder(toinspect,interval);
        this.inspector.addChangeListener(this);
    }
    
    public void stateChanged(ChangeEvent e){
        TimeStampedValue last;
        try{
            last = inspector.getLastValue(fieldname);
        }catch(Exception f){
            f.printStackTrace();
            return;
        }
        if(last!=null){
            double tmpx,tmpy;
            tmpx = last.getTime()-start;
            tmpy = Double.parseDouble(last.getValue().toString());
            this.view.addPoint(tmpx,tmpy);
        }
    }
    
    public void setInterval(long interval){
        this.inspector.setInterval(interval);
    
    }
}
