/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * UnitSystemSI.java
 *
 * Created on 14. September 2001, 10:58
 */

package org.siani.javafmi.plugin.inspector.chart.aw.util.units;

/**
 *  @see aw.util.units.Unit
 *  @see aw.util.units.UnitFactory
 *  @see aw.util.units.UnitSystem
 *
 * @author  <a href='mailto:Achim.Westermann@gmx.de'>Achim Westermann</a>
 * @version 1.0
 */
public class UnitSystemSI implements UnitSystem {
    
    private static Class[]units = new Class[]{
        UnitFemto.class,
        UnitNano.class,
        UnitMicro.class,
        UnitMilli.class,
        UnitUnchanged.class,
        UnitKilo.class,
        UnitMega.class,
        UnitGiga.class,
        UnitTera.class,
        UnitPeta.class
    };
    
    private static UnitSystem instance;
    private UnitSystemSI(){};
    
    public static UnitSystem getInstance(){
        if(instance == null)
            instance = new UnitSystemSI();
        return instance;
    }
    
    public Class[] getUnits(){
        return units;
    }
}
