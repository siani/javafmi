/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.siani.javafmi.plugin.inspector.chart.aw.gui.chart;
import java.util.Iterator;

/**
 *  In addition to the <code> Trace2DLtd</code> this class offers the guarantee 
 *  only to allow a single tracepoint with a certain x- value. If a new 
 *  tracepoint is added whose x- value is already contained, the new tracepoints 
 *  values will get assigned to the certain old tracepoint respecting the fact 
 *  that only an additional changed y- value occurs. 
 *  <br>
 *  The <code>add</code> methods increase complexity to factor n but some event - 
 *  handling may be saved (no change of x and y).
 * <br>
 *  Tracepoints with x- values not contained before will be appended to the end 
 *  of the internal data- structure. <br>
 * @author  Achim Westermann <a href='mailto:Achim.Westermann@gmx.de'>Achim.Westermann@gmx.de</A>
 * @version 
 */
public class Trace2DLtdReplacing extends Trace2DLtd {
    /**
     *  Constructs a <code>Trace2DLtdReplacing</code> with a default buffer size of 
     *  100.
     */  
    public Trace2DLtdReplacing(){
        this(100);
    }
    /**
     *  Constructs a <code>Trace2DLtdReplacing</code> with a buffer size of 
     *  bufsize.
     */  
    public Trace2DLtdReplacing(int bufsize){
        super(bufsize);
    }
        
    public void addPoint(TracePoint2D p)
    {
        TracePoint2D tmp;
        double tmpx,tmpy;
        synchronized(this.renderer){
          synchronized(this){
            Iterator it = this.buffer.iteratorF2L();
            while(it.hasNext()){
                tmp = (TracePoint2D)it.next();
                if((tmpx=tmp.getX())==p.getX()){
                    if((tmpy=p.getY())==tmp.getY()){
                        tmp.setLocation(tmpx,tmpy);
                        this.fireTraceChanged(tmp);
                        return;
                    }
                }
            }
          }
            // using the complicated min/max- routines
            super.addPoint(p);
        }
    }    
}
