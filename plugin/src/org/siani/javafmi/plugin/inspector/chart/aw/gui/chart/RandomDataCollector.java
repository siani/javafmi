/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.siani.javafmi.plugin.inspector.chart.aw.gui.chart;

/**
 * <p>
 * A proof of concept dummy implementation for the supertype. 
 * Only collects random values with timestamp on the x axis. 
 * </p>
 * @author <a href="mailto:Achim.Westermann@gmx.de">Achim Westermann</a>
 *
 */
public class RandomDataCollector extends AbstractDataCollector {

  private double y = 0.0;
  private long starttime = System.currentTimeMillis();
 
  public RandomDataCollector(ITrace2D trace,int latency) {
    super(trace,latency);
  }
  
  /* (non-Javadoc)
   * @see aw.gui.chart.AbstractDataCollector#collectData()
   */
  public TracePoint2D collectData() {
    double rand = Math.random();
    boolean add = (rand>=0.5)?true:false;
    this.y = (add)? this.y+ Math.random():this.y- Math.random();
    return new TracePoint2D(this.getTrace(),((double)System.currentTimeMillis()-this.starttime),this.y);
  }
}
