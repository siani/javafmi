/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.siani.javafmi.plugin.actions;

import com.intellij.ide.SaveAndSyncHandlerImpl;
import com.intellij.notification.Notification;
import com.intellij.notification.NotificationType;
import com.intellij.notification.Notifications;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.compiler.CompileContext;
import com.intellij.openapi.compiler.CompileStatusNotification;
import com.intellij.openapi.compiler.CompilerManager;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleManager;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ex.ProjectManagerEx;
import com.intellij.openapi.roots.ui.configuration.ChooseModulesDialog;
import com.intellij.openapi.vfs.VfsUtil;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.vfs.VirtualFileManager;
import org.jetbrains.annotations.NotNull;
import org.siani.javafmi.plugin.framework.JavaFMIFacet;
import org.siani.javafmi.plugin.model.JavaFMIIcons;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ExportFMUAction extends ExportFMUAbstractAction {

	@Override
	public void actionPerformed(@NotNull AnActionEvent e) {
		final Project project = e.getData(CommonDataKeys.PROJECT);
		if (project == null) return;
		List<Module> fmuFodules = collectFmuModules(project);
		ChooseModulesDialog dialog = createDialog(project, fmuFodules);
		dialog.show();
		if (dialog.isOK()) export(dialog.getChosenElements(), project);
	}

	private void export(List<Module> chosenModules, Project project) {
		final CompilerManager compilerManager = CompilerManager.getInstance(project);
		compilerManager.make(project, chosenModules.toArray(new Module[chosenModules.size()]), export(chosenModules));
	}

	private CompileStatusNotification export(final List<Module> modules) {
		return new CompileStatusNotification() {
			public void finished(final boolean aborted, final int errors, final int warnings, final CompileContext compileContext) {
				if (aborted || errors != 0) return;
				finish();
			}

			private void finish() {
				ApplicationManager.getApplication().invokeLater(() -> {
					saveAll(modules.get(0).getProject());
					exportFMU(modules);
					reloadProject();
				});

			}
		};
	}

	private void exportFMU(List<Module> modules) {
		for (Module module : modules) {
			ProgressManager.getInstance().runProcessWithProgressSynchronously(() -> {
				try {
					final File file = createFMU(module);
					if (file == null) return;
					final Path move = Files.move(file.toPath(), destinyFile(module, file.getName()), StandardCopyOption.REPLACE_EXISTING);
					refresh(move.toFile());
				} catch (IOException e) {
					Notifications.Bus.notify(new Notification("FMU Builder", "FMU Builder", e.getMessage(), NotificationType.ERROR), modules.iterator().next().getProject());
				}
			}, "Creating FMU", false, module.getProject());
		}

	}

	private void refresh(File file) {
		VirtualFile vFile = VfsUtil.findFileByIoFile(file, true);
		if (vFile == null || !vFile.isValid()) return;
		vFile.refresh(true, false);
	}

	@NotNull
	private Path destinyFile(Module module, String name) {
		final File projectDir = new File(module.getProject().getBasePath());
		return new File(projectDir, name).toPath();
	}

	private void reloadProject() {
		SaveAndSyncHandlerImpl.getInstance().refreshOpenFiles();
		VirtualFileManager.getInstance().refreshWithoutFileWatcher(false);
		ProjectManagerEx.getInstanceEx().unblockReloadingProjectOnExternalChanges();
	}


	private void saveAll(Project project) {
		project.save();
		FileDocumentManager.getInstance().saveAllDocuments();
		ProjectManagerEx.getInstanceEx().blockReloadingProjectOnExternalChanges();
	}

	private ChooseModulesDialog createDialog(Project project, List<Module> fmuModules) {
		final ChooseModulesDialog chooseModulesDialog = new ChooseModulesDialog(project, fmuModules, "Select module", "Choose FMU module to export");
		chooseModulesDialog.setSingleSelectionMode();
		chooseModulesDialog.selectElements(Collections.singletonList(fmuModules.get(0)));
		return chooseModulesDialog;
	}

	private List<Module> collectFmuModules(Project project) {
		List<Module> fmuModules = new ArrayList<>();
		for (Module aModule : ModuleManager.getInstance(project).getModules())
			if (JavaFMIFacet.isOfType(aModule)) fmuModules.add(aModule);
		return fmuModules;
	}

	@Override
	public void update(@NotNull AnActionEvent e) {
		final Project project = e.getData(CommonDataKeys.PROJECT);
		boolean enabled = project != null && !collectFmuModules(project).isEmpty();
		e.getPresentation().setIcon(JavaFMIIcons.ICON_16);
		e.getPresentation().setText("Export FMU...");
		e.getPresentation().setEnabled(enabled);
	}
}
