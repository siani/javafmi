/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.siani.javafmi.plugin.actions;

import com.intellij.ide.highlighter.JavaFileType;
import com.intellij.notification.Notification;
import com.intellij.notification.Notifications;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.application.PathManager;
import com.intellij.openapi.fileTypes.FileTypeManager;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.project.DumbAware;
import com.intellij.openapi.roots.CompilerModuleExtension;
import com.intellij.openapi.roots.OrderEnumerator;
import com.intellij.openapi.roots.OrderRootType;
import com.intellij.openapi.roots.libraries.Library;
import com.intellij.openapi.util.io.FileUtil;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiClassType;
import com.intellij.psi.PsiJavaFile;
import com.intellij.psi.PsiManager;
import com.intellij.psi.search.FileTypeIndex;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.util.indexing.FileBasedIndex;
import com.intellij.util.io.ZipUtil;
import org.javafmi.builder.Platform;
import org.javafmi.builder.ShellFmuBuilder;
import org.javafmi.framework.FmiSimulation;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.siani.javafmi.plugin.framework.JavaFMIFacet;
import org.siani.javafmi.plugin.model.PluginConstants;

import java.io.*;
import java.util.*;
import java.util.jar.JarOutputStream;
import java.util.stream.Collectors;
import java.util.zip.ZipOutputStream;

import static com.intellij.notification.NotificationType.ERROR;
import static com.intellij.openapi.module.ModuleUtilCore.getDependencies;
import static java.io.File.separator;

abstract class ExportFMUAbstractAction extends AnAction implements DumbAware {

	private static OrderEnumerator productionRuntimeDependencies(Module module) {
		return OrderEnumerator.orderEntries(module).productionOnly();
	}

	private static final String JAR_EXTENSION = "jar";
	private static final String TEMP_PREFIX = "temp";
	private static final Map<String, Platform> platforms;

	static {
		platforms = new HashMap<>();
		platforms.put("Windows 64", Platform.Win64);
		platforms.put("Windows 32", Platform.Win32);
		platforms.put("Linux 64", Platform.Linux64);
		platforms.put("Linux 32", Platform.Linux32);
	}

	File createFMU(Module module) {
		return buildFMU(module, platforms(JavaFMIFacet.of(module).getConfiguration().platforms()));
	}

	private File buildFMU(Module module, List<Platform> platforms) {
		final String name = findFmuCoSimulationClass(module);
		if (name.isEmpty()) {
			Notifications.Bus.notify(new Notification("FMU Builder", "FMU Builder", "Cosimulation class for module " + module.getName() + " not found", ERROR), module.getProject());
			return null;
		}
		final Set<Module> modules = new HashSet<>();
		final Set<Library> libs = new HashSet<>();
		modules.add(module);
		getDependencies(module, modules);
		modules.forEach(m -> collectLibraries(m, libs));
		updateProgress();
		return build(platforms, name, modules, libs);
	}

	private File build(List<Platform> platforms, String name, Set<Module> modules, Set<Library> libs) {
		String absolutePath = "";
		try {
			absolutePath = jar(modules, libs, name).getAbsolutePath();
			return new ShellFmuBuilder(absolutePath).fmuName(name).platforms(platforms).workingDirectory(FileUtil.getTempDirectory()).build();

		} catch (IOException e) {
			Notifications.Bus.notify(new Notification("FMU Builder", "FMU Builder", e.getMessage(), ERROR), modules.iterator().next().getProject());
			return null;
		} finally {
			if (!absolutePath.isEmpty()) System.out.println(new File(absolutePath).delete());
		}
	}

	private String findFmuCoSimulationClass(Module module) {
		final String[] name = {""};
		ApplicationManager.getApplication().runReadAction(() -> {
			final List<PsiJavaFile> javaFiles = getJavaFiles(module);
			javaFiles.stream().filter(javaFile -> isCoSimulation(javaFile.getClasses()[0])).
				forEach(javaFile -> name[0] = javaFile.getClasses()[0].getName());
		});
		return name[0];
	}

	private boolean isCoSimulation(PsiClass psiClass) {
		if (psiClass.getExtendsList() == null) return false;
		for (PsiClassType aClass : psiClass.getExtendsListTypes())
			if (FmiSimulation.class.getName().equals(aClass.getCanonicalText())) return true;
		return false;
	}

	@NotNull
	private List<PsiJavaFile> getJavaFiles(Module module) {
		List<PsiJavaFile> javaFiles = new ArrayList<>();
		Collection<VirtualFile> files = FileBasedIndex.getInstance().getContainingFiles(FileTypeIndex.NAME, JavaFileType.INSTANCE, GlobalSearchScope.moduleScope(module));
		files.stream().filter(file -> file != null).forEach(file -> {
			PsiJavaFile javaFile = (PsiJavaFile) PsiManager.getInstance(module.getProject()).findFile(file);
			if (javaFile != null) javaFiles.add(javaFile);
		});
		return javaFiles;
	}


	private List<Platform> platforms(String[] platformNames) {
		return Arrays.stream(platformNames).map(platforms::get).collect(Collectors.toList());
	}

	private File jar(@NotNull Set<Module> modules, Set<Library> libs, String name) throws IOException {
		File jarFile = new File(FileUtil.getTempDirectory(), name + "." + JAR_EXTENSION);
		jarFile.createNewFile();
		jarFile.deleteOnExit();
		ZipOutputStream jarStream = null;
		try {
			BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(jarFile));
			jarStream = new JarOutputStream(out);
			final ProgressIndicator progressIndicator = ProgressManager.getInstance().getProgressIndicator();
			final Set<String> writtenItemRelativePaths = new HashSet<>();
			for (Module module : modules) {
				final VirtualFile compilerOutputPath = CompilerModuleExtension.getInstance(module).getCompilerOutputPath();
				if (compilerOutputPath == null) continue;
				ZipUtil.addDirToZipRecursively(jarStream, jarFile, new File(compilerOutputPath.getPath()), "",
					createFilter(progressIndicator, FileTypeManager.getInstance()), writtenItemRelativePaths);
			}
			addLibs(libs, jarStream, jarFile, progressIndicator, writtenItemRelativePaths);
		} finally {
			if (jarStream != null) {
				jarStream.flush();
				jarStream.close();
			}
		}
		return jarFile;
	}

	private void addLibs(Set<Library> libs, ZipOutputStream jarModel, File jarFile, ProgressIndicator progressIndicator, Set<String> writtenItemRelativePaths) throws IOException {
		for (Library lib : libs) {
			for (VirtualFile virtualFile : lib.getFiles(OrderRootType.CLASSES)) {
				if (JAR_EXTENSION.equals(virtualFile.getExtension()) && !isApplicationDependency(virtualFile)) {
					File temp = FileUtil.createTempDirectory(TEMP_PREFIX, virtualFile.getName(), true);
					ZipUtil.extract(new File(virtualFile.getPath().substring(0, virtualFile.getPath().indexOf("!"))), temp, null);
					ZipUtil.addDirToZipRecursively(jarModel, jarFile, new File(temp.getPath()), "",
						createFilter(progressIndicator, FileTypeManager.getInstance()), writtenItemRelativePaths);
				}
			}
		}
	}

	private boolean isApplicationDependency(VirtualFile virtualFile) {
		final File file = new File(PathManager.getPluginsPath(), PluginConstants.PLUGIN_NAME + separator + "lib" + separator);
		final String name = virtualFile.getName();
		return virtualFile.getPath().startsWith(file.getPath().replace("\\", "/")) && !name.equals(PluginConstants.FRAMEWORK);
	}

	private FileFilter createFilter(final ProgressIndicator progressIndicator, @Nullable final FileTypeManager fileTypeManager) {
		return pathName -> {
			if (progressIndicator != null) progressIndicator.setText2("");
			return fileTypeManager == null || !fileTypeManager.isFileIgnored(FileUtil.toSystemIndependentName(pathName.getName()));
		};
	}

	private void updateProgress() {
		final ProgressIndicator progressIndicator = ProgressManager.getInstance().getProgressIndicator();
		if (progressIndicator != null) {
			progressIndicator.setText("Creating FMU artifact");
			progressIndicator.setIndeterminate(true);
		}
	}


	private static void collectLibraries(Module module, final Set<Library> libs) {
		productionRuntimeDependencies(module).compileOnly().forEachLibrary(library -> {
			libs.add(library);
			return true;
		});
	}

}
