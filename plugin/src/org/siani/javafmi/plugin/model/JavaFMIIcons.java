/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.siani.javafmi.plugin.model;

import com.intellij.util.IconUtil;
import com.intellij.util.ui.UIUtil;

import javax.swing.*;

import static com.intellij.openapi.util.IconLoader.getIcon;
import static com.intellij.util.ui.UIUtil.isUnderDarcula;

public class JavaFMIIcons {

	private static final boolean RETINA = UIUtil.isRetina();

	public static final Icon ICON_16 = RETINA ? scale(getIcon("/icons/icon_retina.png")) : getIcon("/icons/icon.png");
	public static final Icon ICON_13 = getIcon("/icons/icon_13.png");
	public static final Icon PLAY = getIcon(isUnderDarcula() ? "/icons/play.png" : "/icons/play_black.png");
	public static final Icon RELOAD = getIcon(isUnderDarcula() ? "/icons/reload.png" : "/icons/reload_black.png");
	public static final Icon PAUSE = getIcon(isUnderDarcula() ? "/icons/pause.png" : "/icons/pause_black.png");
	public static final Icon FORWARD = getIcon(isUnderDarcula() ? "/icons/forward.png" : "/icons/forward_black.png");

	private static Icon scale(Icon icon) {
		return IconUtil.scale(icon, 0.5);
	}

}
