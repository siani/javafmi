/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.siani.javafmi.plugin.model;

import com.intellij.openapi.fileTypes.LanguageFileType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class FMUFileType extends LanguageFileType {
	public static final FMUFileType INSTANCE = new FMUFileType();

	private FMUFileType() {
		super(FMULanguage.INSTANCE);
	}

	@NotNull
	@Override
	public String getName() {
		return "FMU";
	}

	@NotNull
	@Override
	public String getDescription() {
		return "FMU file";
	}

	@NotNull
	@Override
	public String getDefaultExtension() {
		return "fmu";
	}

	@Nullable
	@Override
	public javax.swing.Icon getIcon() {
		return JavaFMIIcons.ICON_16;
	}
}
