/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.siani.javafmi.plugin.framework;

import com.intellij.facet.*;
import com.intellij.openapi.module.Module;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class JavaFMIFacet extends Facet<JavaFMIFacetConfiguration> {
	public static final FacetTypeId<JavaFMIFacet> ID = new FacetTypeId<>("FMU");

	public JavaFMIFacet(@NotNull FacetType facetType,
						@NotNull Module module,
						@NotNull String name,
						@NotNull JavaFMIFacetConfiguration configuration) {
		super(facetType, module, name, configuration, null);
	}

	public static FacetType<JavaFMIFacet, JavaFMIFacetConfiguration> getFacetType() {
		return FacetTypeRegistry.getInstance().findFacetType(ID);
	}

	@Nullable
	public static JavaFMIFacet of(@Nullable Module module) {
		if (module == null) return null;
		return FacetManager.getInstance(module).getFacetByType(ID);
	}


	public static boolean isOfType(Module aModule) {
		return aModule != null && FacetManager.getInstance(aModule).getFacetByType(ID) != null;
	}
}
