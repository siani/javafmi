/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.siani.javafmi.plugin.framework;

import com.intellij.facet.Facet;
import com.intellij.facet.FacetType;
import com.intellij.openapi.module.JavaModuleType;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.siani.javafmi.plugin.model.JavaFMIIcons;

import javax.swing.*;


public class JavaFMIFacetType extends FacetType<JavaFMIFacet, JavaFMIFacetConfiguration> {

	public static final String STRING_ID = "FMU";

	public JavaFMIFacetType() {
		super(JavaFMIFacet.ID, STRING_ID, "FMU");
	}

	public static FacetType<JavaFMIFacet, JavaFMIFacetConfiguration> getInstance() {
		return JavaFMIFacetType.findInstance(JavaFMIFacetType.class);
	}

	public JavaFMIFacetConfiguration createDefaultConfiguration() {
		return new JavaFMIFacetConfiguration();
	}

	public JavaFMIFacet createFacet(@NotNull Module module,
									String name,
									@NotNull JavaFMIFacetConfiguration configuration,
									@Nullable Facet underlyingFacet) {
		return new JavaFMIFacet(this, module, name, configuration);
	}

	public boolean isSuitableModuleType(ModuleType moduleType) {
		return moduleType instanceof JavaModuleType;
	}

	@NotNull
	@Override
	public String getDefaultFacetName() {
		return "FMU";
	}

	@Override
	public Icon getIcon() {
		return JavaFMIIcons.ICON_16;
	}

}
