/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.siani.javafmi.plugin.framework;

import com.intellij.framework.FrameworkTypeEx;
import com.intellij.framework.addSupport.FrameworkSupportInModuleProvider;
import org.jetbrains.annotations.NotNull;
import org.siani.javafmi.plugin.model.JavaFMIIcons;

import javax.swing.*;

public class JavaFMIFrameworkType extends FrameworkTypeEx {
	public static final String ID = "javafmi-java";

	public JavaFMIFrameworkType() {
		super(ID);
	}

	public static JavaFMIFrameworkType getFrameworkType() {
		return EP_NAME.findExtension(JavaFMIFrameworkType.class);
	}

	@NotNull
	@Override
	public FrameworkSupportInModuleProvider createProvider() {
		return new JavaFMISupportProvider();
	}

	@NotNull
	@Override
	public String getPresentableName() {
		return "FMU";
	}

	@NotNull
	@Override
	public Icon getIcon() {
		return JavaFMIIcons.ICON_16;
	}
}
