/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.siani.javafmi.plugin.framework;

import com.intellij.facet.FacetConfiguration;
import com.intellij.facet.ui.FacetEditorContext;
import com.intellij.facet.ui.FacetEditorTab;
import com.intellij.facet.ui.FacetValidatorsManager;
import com.intellij.openapi.components.PersistentStateComponent;
import com.intellij.openapi.util.InvalidDataException;
import com.intellij.openapi.util.WriteExternalException;
import org.jdom.Element;

public class JavaFMIFacetConfiguration implements FacetConfiguration, PersistentStateComponent<JavaFMIFacetConfigurationProperties> {

	private JavaFMIFacetConfigurationProperties properties = new JavaFMIFacetConfigurationProperties();

	public FacetEditorTab[] createEditorTabs(FacetEditorContext editorContext, FacetValidatorsManager validatorsManager) {
		return new FacetEditorTab[]{
//			new FmiFacetEditor(this, editorContext)
		};
	}

	public void readExternal(Element element) throws InvalidDataException {
	}

	public void writeExternal(Element element) throws WriteExternalException {
	}

	public JavaFMIFacetConfigurationProperties getProperties() {
		return properties;
	}

	public JavaFMIFacetConfigurationProperties getState() {
		return properties;
	}

	public void loadState(JavaFMIFacetConfigurationProperties state) {
		properties = state;
	}

	public boolean cosimulation() {
		return properties.cosimulation;
	}

	public void cosimulation(boolean cosimulation) {
		properties.cosimulation = cosimulation;
	}

	public String[] platforms() {
		return properties.platforms;
	}

	public void platforms(String[] platforms) {
		properties.platforms = platforms;
	}

}
