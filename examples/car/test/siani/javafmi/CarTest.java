/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package siani.javafmi;

import org.javafmi.framework.FmiSimulation;
import org.javafmi.framework.ModelDescription;
import org.javafmi.skeleton.SimulationSkeleton;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class CarTest {

	@Test
	public void createsTheTripSimulation() {
		Car car = new Car();
		car.init();
		car.speed = 40;
		car.doStep(1800);
		Double distanceAfterHalfHour = car.distance;
		car.doStep(1800);
		Double distanceAfterAnHour = car.distance;
		FmiSimulation.Status terminateStatus = car.terminate();

		assertEquals(FmiSimulation.Status.OK, terminateStatus);
		assertEquals(20000, distanceAfterHalfHour, 0.1);
		assertEquals(40000, distanceAfterAnHour, 0.1);
	}

	@Test
	public void variablesCanBeSetBeforeInit() {
		FmiSimulation car = new Car();
		List<ModelDescription.ScalarVariable> variables = car.modelDescription().variables();
		int carSpeed = variables.stream().filter(v -> v.name().equals("car.speed")).findFirst().get().valueReference();
		int carDistance = variables.stream().filter(v -> v.name().equals("car.distance")).findFirst().get().valueReference();

		car.setReal(new int[]{carSpeed}, new double[]{40.});
		car.setReal(new int[]{carDistance}, new double[]{10.});
		car.enterInitializationMode();
		car.exitInitializationMode();

		assertThat(car.getReal(carSpeed)[0], is(40.));
		car.terminate();
	}

	@Test
	public void getStateAndSetStateShouldWork() {
		Car car = new Car();
		car.init();
		assertThat(car.distance, is(0.0));
		car.getState("state0");
		car.speed = 40;
		car.doStep(1);
		assertEquals(car.distance, 11.1, 0.1);
		car.setState("state0");
		assertThat(car.distance, is(0.0));
	}

	@Test
	public void shouldGetVariableData() throws Exception {
		Car car = new Car();
		car.init();
		System.out.println(car.getReal(2)[0]);
	}
}
