/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */
package siani.javafmi;

import org.javafmi.framework.FmiSimulation;
import org.javafmi.framework.State;

import static org.javafmi.framework.FmiContainer.Causality.input;
import static org.javafmi.framework.FmiContainer.Causality.output;
import static org.javafmi.framework.FmiContainer.Variability.discrete;


public class Car extends FmiSimulation {
	private static final String ModelName = "Car";
	private static final String SpeedVar = "car.speed";
	private static final String DistanceVar = "car.distance";
	private static final String NumberOfDoorsVar = "car.numberofdoors";
	private static final String MaxPowerVar = "car.maxpower";
	private static final String AbsVar = "car.abs";
	private static final String Mp3Var = "car.mp3";
	private static final String FuelVar = "car.fuel";
	private static final String SongVar = "car.song";
	double speed = 0, distance = 0;
	int numberOfDoors = 5, maxPower = 90;
	boolean abs = true, mp3 = true;
	Fuel fuel = Fuel.Diesel;
	String currentSong = "init";

	@Override
	public Model define() {
		return model(ModelName)
				.canGetAndSetFMUstate(true)
				.add(variable(SpeedVar).asReal().causality(input).variability(discrete).start(0))
				.add(variable(DistanceVar).asReal().causality(output).variability(discrete))
				.add(variable(NumberOfDoorsVar).asInteger().causality(input).variability(discrete))
				.add(variable(MaxPowerVar).asInteger().causality(input).variability(discrete))
				.add(variable(AbsVar).asBoolean().causality(input).variability(discrete))
				.add(variable(Mp3Var).asBoolean().causality(input).variability(discrete))
				.add(variable(FuelVar).asString().causality(input).variability(discrete))
				.add(variable(SongVar).asString().causality(input).variability(discrete))
				.add(modelStructure()
						.addOutput(unknown("3").dependencies("4", "7").dependenciesKind("fixed")));
	}

	@Override
	public Status init() {
		logger().info("doing init");
		registerReal(SpeedVar, () -> speed, value -> speed = value);
		registerReal(DistanceVar, () -> distance);
		registerInteger(NumberOfDoorsVar, () -> numberOfDoors, value -> numberOfDoors = value);
		registerInteger(MaxPowerVar, () -> maxPower, value -> maxPower = value);
		registerBoolean(AbsVar, () -> abs, value -> abs = value);
		registerBoolean(Mp3Var, () -> mp3, value -> mp3 = value);
		registerString(FuelVar, () -> fuel.name(), value -> fuel = Fuel.valueOf(value));
		registerString(SongVar, () -> currentSong, value -> currentSong = value);
		return Status.OK;
	}

	@Override
	public Status doStep(double stepSize) {
		distance += speed * (10. / 36.) * stepSize;
		return Status.OK;
	}

	@Override
	public Status reset() {
		return Status.OK;
	}

	@Override
	public Status terminate() {
		return Status.OK;
	}

	@Override
	public Status getState(String state) {
		logger().info("doing get state");
		return super.getState(state);
	}

	@Override
	protected void setState(State state) {
		super.setState(state);
		distance = (double) state.get(DistanceVar);
	}

	@Override
	public double[] getRealOutputDerivatives(int[] valueReferences, int[] order) {
		logger().info("getting real output derivatives");
		return super.getRealOutputDerivatives(valueReferences, order);
	}

	@Override
	public Status setRealInputDerivatives(int[] valueReferences, int[] order, double[] values) {
		logger().info("setting real input derivatives");
		return super.setRealInputDerivatives(valueReferences, order, values);
	}

	private enum Fuel {Petrol, Diesel}
}
