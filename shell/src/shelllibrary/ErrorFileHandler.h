﻿#pragma once
#include <string>
using std::string;

class ErrorFileHandler {
	string error_directory;
public:
	ErrorFileHandler(string error_directory);
	
	virtual ~ErrorFileHandler();

	virtual void copyErrorFileTo(const string& destination_directory);
};
