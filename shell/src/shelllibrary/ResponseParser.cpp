#include "ResponseParser.h"
#include "Utilities.h"
#include <sstream>
#include <Poco/Path.h>
#include <algorithm>
#include <cctype>

using namespace Poco;

const string ResponseParser::OK_CODE = "ok";
const string ResponseParser::WARNING_CODE = "warning";
const string ResponseParser::DISCARD_CODE = "discard";
const string ResponseParser::ERROR_CODE = "error";
const string ResponseParser::PENDING_CODE = "pending";
const string ResponseParser::FATAL_CODE = "fatal";

void checkError(fmi2Status status, ErrorFileHandler& file_error_handler) {
	if (status == fmi2Warning || status == fmi2Error || status == fmi2Fatal)
		file_error_handler.copyErrorFileTo(Path::current());
};

string fixed(const string& response) {
	string result = response.substr(0, response.find_first_of(' '));
	result.erase(std::remove_if(result.begin(), result.end(), [](const char& letter){ return std::isspace(letter);}), 
		result.end());
	return result;

}

ResponseParser::ResponseParser(shared_ptr<ErrorFileHandler> error_file_handler)
:error_file_handler{error_file_handler} {
	codes.emplace(OK_CODE, fmi2OK);
	codes.emplace(WARNING_CODE, fmi2Warning);
	codes.emplace(DISCARD_CODE, fmi2Discard);
	codes.emplace(ERROR_CODE, fmi2Error);
	codes.emplace(PENDING_CODE, fmi2Pending);
	codes.emplace(FATAL_CODE, fmi2Fatal);
}

fmi2Status ResponseParser::extractStatus(const string& response) {
	auto code = codes.find(fixed(response));
	auto status = fmi2Fatal;
	if (code != codes.end())
		status = code->second;
	checkError(status, *error_file_handler);
	return status;
}

fmi2Status ResponseParser::extractReal(const string& response, fmi2Real doubleValue[]) {
	string responseCopy = response;
	responseCopy.append(" ");
	if (stringStartsWith(responseCopy, "ok")) {
		responseCopy.erase(0, 3);
		std::string delimiter = " ";
		size_t pos = 0;
		size_t index = 0;
		std::string token;
		while ((pos = responseCopy.find(delimiter)) != std::string::npos) {
			token = responseCopy.substr(0, pos);
			doubleValue[index++] = atof(token.c_str());
			responseCopy.erase(0, pos + delimiter.length());
		}
		return fmi2OK;
	}
	return extractStatus(responseCopy);
}

fmi2Status ResponseParser::extractInteger(const string& response, fmi2Integer intValue[]) {
	string responseCopy = response;
	responseCopy.append(" ");
	if (stringStartsWith(responseCopy, "ok")) {
		responseCopy.erase(0, 3);
		std::string delimiter = " ";
		size_t pos = 0;
		size_t index = 0;
		std::string token;
		while ((pos = responseCopy.find(delimiter)) != std::string::npos) {
			token = responseCopy.substr(0, pos);
			intValue[index++] = atoi(token.c_str());
			responseCopy.erase(0, pos + delimiter.length());
		}
		return fmi2OK;
	}
	return extractStatus(responseCopy);
}

fmi2Status ResponseParser::extractBoolean(const string& response, fmi2Boolean booleanValues[]) {
	string responseCopy = response;
	responseCopy.append(" ");
	if (stringStartsWith(responseCopy, "ok")) {
		responseCopy.erase(0, 3);
		std::string delimiter = " ";
		size_t pos = 0;
		size_t index = 0;
		std::string token;
		while ((pos = responseCopy.find(delimiter)) != std::string::npos) {
			token = responseCopy.substr(0, pos);
			booleanValues[index++] = token.find("true") != string::npos ? fmi2True : fmi2False;
			responseCopy.erase(0, pos + delimiter.length());
		}
		return fmi2OK;
	}
	return extractStatus(responseCopy);
}

fmi2Status ResponseParser::extractString(const string& response, char* stringValue) {
	if (stringStartsWith(response, "ok")) {
		string result = response.substr(response.find_first_of(' ') + 1);
		for (int i = 0; i < result.size(); i++) {
			stringValue[i] = result.at(i);
			stringValue[i + 1] = '\0';
		}
		return fmi2OK;
	}
	return extractStatus(response);
}
