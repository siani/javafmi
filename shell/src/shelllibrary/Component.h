#pragma once

#include <fmi/fmi2FunctionTypes.h>
#include "CommunicationHandle.h"
#include "LogListener.h"

class Component {
public:
	CommunicationHandle channel;
	shared_ptr<const fmi2CallbackFunctions> callbacks;
	shared_ptr<LogListener> log_listener;
public:
	Component(CommunicationHandle channel, shared_ptr<const fmi2CallbackFunctions> callbacks);
};
