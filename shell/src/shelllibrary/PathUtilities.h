#pragma once

#ifndef _MSC_VER
#define NOEXCEPT noexcept
#else
#define NOEXCEPT
#endif

#include <string>
#include <functional>
using std::string;

class InvalidResourcesDirectory :public std::exception {
	string _what;
public:
	explicit InvalidResourcesDirectory(const string& bad_directory) : _what(bad_directory) {}

	const char* what() const NOEXCEPT override{
		return _what.c_str();
	}
};

string extractResourcesPath(const string& resource_url, std::function<bool(string& resources_directory)> checker);
