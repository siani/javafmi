#include "Utilities.h"
#include <sstream>
#include <memory>
#include <fmi/fmi2Functions.h>
#include <Poco/File.h>
#include <cstring>
#include "Component.h"
#include "SimulationLauncher.h"
#include "ResponseParser.h"
#include "PathUtilities.h"
#include "PermissionFixer.h"

#if defined(_WIN64)
#define JRE_BIN "bin/java.exe"
#elif defined(__linux)
#define JRE_BIN "bin/java"
#endif

using std::string;
using std::unique_ptr;

long stateIndex = 0;
char* last_shared_string;
unique_ptr<ResponseParser> parser;

const char* fmi2GetTypesPlatform() {
	return "default";
}

const char* fmi2GetVersion() {
	return "2.0";
}


const char* serialize(fmi2Component _component, const fmi2String value) {
	auto component = reinterpret_cast<Component*>(_component);
	component->callbacks->freeMemory(last_shared_string);
	last_shared_string = (char*)component->callbacks->allocateMemory(512, sizeof(char));
	for (size_t i = 0; i < strlen(value); i++) {
		if (value[i] == '\n') last_shared_string[i] = 11;
		else last_shared_string[i] = value[i];
	}
	last_shared_string[strlen(value)] = '\0';
	return last_shared_string;
}


const char* deserialize(char* value) {
	for (size_t i = 0; i < strlen(value); i++) {
		if (value[i] == 11	) value[i] = '\n';
	}
	return last_shared_string;
}


fmi2Status fmi2SetDebugLogging(fmi2Component _component, fmi2Boolean loggingOn, size_t numberOfCategories,
	const fmi2String categories[]) {
	return fmi2Error;
}

fmi2Component fmi2Instantiate(fmi2String instanceName, fmi2Type fmuType, fmi2String fmuGUID,
	fmi2String fmuResourceLocation, const fmi2CallbackFunctions* callbacks,
	fmi2Boolean visible, fmi2Boolean loggingOn) {
	try {
		auto base_directory = extractResourcesPath(fmuResourceLocation, [](string& resources_path) {return Poco::File{ resources_path + "/MANIFEST" }.exists(); });
		fixPermissions(base_directory);
		parser = std::make_unique<ResponseParser>(std::make_shared<ErrorFileHandler>(base_directory));

		char* environment{ getenv("JAVA_HOME") };
		if (environment == NULL) throw std::invalid_argument("JAVA_HOME environment variable is not set");
		char* environmentWithSlash = environment;
		if (!hasEnding(environment, "/") && !hasEnding(environment, "\\"))
			environmentWithSlash = append(environment, "/", callbacks);
		char *command{ append(environmentWithSlash, JRE_BIN, callbacks) };
		if (!doesFileExist(command)) throw std::invalid_argument("JRE not found. Set JAVA_HOME correctly or reboot application");

		auto component = new Component{ launchFmu(command, base_directory, string{instanceName}, callbacks),
			shared_ptr<const fmi2CallbackFunctions>(callbacks) };
		component->log_listener->listen();
		return component;
	} catch (std::exception & e) {
		callbacks->logger(nullptr, instanceName, fmi2Fatal, "ERROR", e.what());
		return nullptr;
	}
}


fmi2Status fmi2SetupExperiment(fmi2Component _component, fmi2Boolean toleranceDefined, fmi2Real tolerance,
	fmi2Real startTime, fmi2Boolean stopTimeDefined, fmi2Real stopTime) {

	auto component = reinterpret_cast<Component*>(_component);
	std::stringstream message_builder;
	message_builder << "setupExperiment" << " "
		<< toleranceDefined << " "
		<< std::scientific << tolerance << " "
		<< std::scientific << startTime << " "
		<< stopTimeDefined << " "
		<< std::scientific << stopTime;

	component->channel.sendMessage(message_builder.str());
	auto response = component->channel.receiveMessage();
	return parser->extractStatus(response);
}

fmi2Status fmi2EnterInitializationMode(fmi2Component _component) {
	auto component = reinterpret_cast<Component*>(_component);
	component->channel.sendMessage("enterInitializationMode");
	return parser->extractStatus(component->channel.receiveMessage());
}

fmi2Status fmi2ExitInitializationMode(fmi2Component _component) {
	auto component = reinterpret_cast<Component*>(_component);
	component->channel.sendMessage("exitInitializationMode");
	return parser->extractStatus(component->channel.receiveMessage());
}

fmi2Status fmi2Terminate(fmi2Component _component) {
	auto component = reinterpret_cast<Component*>(_component);
	component->channel.sendMessage("terminate");
	return parser->extractStatus(component->channel.receiveMessage());
}

void fmi2FreeInstance(fmi2Component _component) {
	auto component = reinterpret_cast<Component*>(_component);
	if (last_shared_string) component->callbacks->freeMemory(last_shared_string);
	component->log_listener->stop();
}

fmi2Status fmi2Reset(fmi2Component _component) {
	auto component = reinterpret_cast<Component*>(_component);
	component->channel.sendMessage("reset");
	return parser->extractStatus(component->channel.receiveMessage());
}

fmi2Status fmi2GetReal(fmi2Component _component, const fmi2ValueReference valueReference[], size_t numberOfValues,
	fmi2Real values[]) {
	auto component = reinterpret_cast<Component*>(_component);
	std::stringstream message_builder;
	message_builder << "getReal";
	for (size_t i = 0; i < numberOfValues; i++)
		message_builder << " " << valueReference[i];
	component->channel.sendMessage(message_builder.str());
	return parser->extractReal(component->channel.receiveMessage(), values);
}

fmi2Status fmi2GetInteger(fmi2Component _component, const fmi2ValueReference valueReference[], size_t numberOfValues,
	fmi2Integer values[]) {
	auto component = reinterpret_cast<Component*>(_component);
	std::stringstream message_builder;
	message_builder << "getInteger";
	for (size_t i = 0; i < numberOfValues; i++)
		message_builder << " " << valueReference[i];
	component->channel.sendMessage(message_builder.str());
	return parser->extractInteger(component->channel.receiveMessage(), values);
}

fmi2Status fmi2GetBoolean(fmi2Component _component, const fmi2ValueReference valueReference[], size_t numberOfValues,
	fmi2Boolean values[]) {
	auto component = reinterpret_cast<Component*>(_component);
	std::stringstream message_builder;
	message_builder << "getBoolean";
	for (size_t i = 0; i < numberOfValues; i++)
		message_builder << " " << valueReference[i];
	component->channel.sendMessage(message_builder.str());
	return parser->extractBoolean(component->channel.receiveMessage(), values);
}

fmi2Status fmi2GetString(fmi2Component _component, const fmi2ValueReference valueReference[], size_t numberOfValues,
	fmi2String values[]) {
	auto component = reinterpret_cast<Component*>(_component);
	component->callbacks->freeMemory(last_shared_string);
	last_shared_string = (char*)component->callbacks->allocateMemory(512, sizeof(char));
	std::stringstream message_builder;
	message_builder << "getString " << valueReference[0];
	component->channel.sendMessage(message_builder.str());
	auto response = component->channel.receiveMessage();
	if (parser->extractString(response, last_shared_string) == 0) {
		if(strlen(last_shared_string) > 0 && last_shared_string[strlen(last_shared_string) - 1] == '\r'){
			last_shared_string[strlen(last_shared_string) - 1] = '\0';
		}
		values[0] = deserialize(last_shared_string);
		return fmi2OK;
	}
	return fmi2Error;
}

fmi2Status fmi2SetReal(fmi2Component _component, const fmi2ValueReference valueReference[], size_t numberOfValues,
	const fmi2Real values[]) {
	auto component = reinterpret_cast<Component*>(_component);
	fmi2Status result = fmi2OK;
	for (size_t i = 0; i < numberOfValues; i++) {
		std::stringstream message_builder;
		message_builder << "setReal " << valueReference[i] << " " << values[i];
		component->channel.sendMessage(message_builder.str());
		if (result == fmi2OK)
			result = parser->extractStatus(component->channel.receiveMessage());
	}
	return result;
}

fmi2Status fmi2SetInteger(fmi2Component _component, const fmi2ValueReference valueReference[], size_t numberOfValues,
	const fmi2Integer values[]) {
	auto component = reinterpret_cast<Component*>(_component);
	fmi2Status result = fmi2OK;
	for (size_t i = 0; i < numberOfValues; i++){
		std::stringstream message_builder;
		message_builder << "setInteger " << valueReference[i] << " " << values[i];
		component->channel.sendMessage(message_builder.str());
		if (result == fmi2OK)
			result = parser->extractStatus(component->channel.receiveMessage());
	}
	return result;
}

fmi2Status fmi2SetBoolean(fmi2Component _component, const fmi2ValueReference valueReference[], size_t numberOfValues,
	const fmi2Boolean values[]) {
	auto component = reinterpret_cast<Component*>(_component);
	fmi2Status result = fmi2OK;
	for (size_t i = 0; i < numberOfValues; i++) {
		std::stringstream message_builder;
		message_builder << "setBoolean " << valueReference[i] << " " << (values[i] == 1 ? "true" : "false");
		component->channel.sendMessage(message_builder.str());
		if (result == fmi2OK)
			result = parser->extractStatus(component->channel.receiveMessage());
	}
	return result;
}

fmi2Status fmi2SetString(fmi2Component _component, const fmi2ValueReference valueReference[], size_t numberOfValues,
	const fmi2String values[]) {
	auto component = reinterpret_cast<Component*>(_component);
	std::stringstream message_builder;
	message_builder << "setString " << valueReference[0] << " " << serialize(_component, values[0]);
	component->channel.sendMessage(message_builder.str());
	return parser->extractStatus(component->channel.receiveMessage());
}

fmi2Status fmi2GetFMUstate(fmi2Component _component, fmi2FMUstate* state) {
 	auto component = reinterpret_cast<Component*>(_component);
	std::stringstream message_builder;
	if(*state == nullptr) *state = (fmi2FMUstate*)stateIndex;
	stateIndex++;
	message_builder << "getState " << *state;
	component->channel.sendMessage(message_builder.str());
	return parser->extractStatus(component->channel.receiveMessage());
}

fmi2Status fmi2SetFMUstate(fmi2Component _component, fmi2FMUstate state) {
	auto component = reinterpret_cast<Component*>(_component);
	std::stringstream message_builder;
	message_builder << "setState " << state;
	component->channel.sendMessage(message_builder.str());
	return parser->extractStatus(component->channel.receiveMessage());
}

fmi2Status fmi2FreeFMUstate(fmi2Component _component, fmi2FMUstate* state) {
	auto component = reinterpret_cast<Component*>(_component);
	std::stringstream message_builder;
	message_builder << "freeState " << *state;
	*state = nullptr;
	component->channel.sendMessage(message_builder.str());
	return parser->extractStatus(component->channel.receiveMessage());
}

fmi2Status fmi2SerializedFMUstateSize(fmi2Component _component, fmi2FMUstate, size_t* stateSize) {
	return fmi2Error;
}

fmi2Status fmi2SerializeFMUstate(fmi2Component _component, fmi2FMUstate state, fmi2Byte serializedState[],
	size_t serializedStateSize) {
	return fmi2Error;
}

fmi2Status fmi2DeSerializeFMUstate(fmi2Component _component, const fmi2Byte serializedState, size_t size,
	fmi2FMUstate* state) {
	return fmi2Error;
}

fmi2Status fmi2GetDirectionalDerivative(fmi2Component _component, const fmi2ValueReference unknownValueReferences[],
	size_t numberOfUnknowns, const fmi2ValueReference knownValueReferences[],
	fmi2Integer numberOfKnowns, fmi2Real knownDifferential[],
	fmi2Real unknownDifferential[]) {
	return fmi2Error;
}

fmi2Status fmi2SetRealInputDerivatives(fmi2Component _component, const fmi2ValueReference valueReferences[],
	size_t numberOfValueReferences, fmi2Integer orders[], const fmi2Real values[]) {
	auto component = reinterpret_cast<Component*>(_component);
	std::stringstream message_builder;
	message_builder << "setRealInputDerivatives " << numberOfValueReferences;
	for (size_t i = 0; i < numberOfValueReferences; i++) message_builder << " " << valueReferences[i];
	for (size_t i = 0; i < numberOfValueReferences; i++) message_builder << " " << orders[i];
	for (size_t i = 0; i < numberOfValueReferences; i++) message_builder << " " << values[i];
	component->channel.sendMessage(message_builder.str());
	return parser->extractStatus(component->channel.receiveMessage());
}

fmi2Status fmi2GetRealOutputDerivatives(fmi2Component _component, const fmi2ValueReference valueReference[],
	size_t numberOfValues, const fmi2Integer order[], fmi2Real values[]) {
	auto component = reinterpret_cast<Component*>(_component);
	std::stringstream message_builder;
	message_builder << "getRealOutputDerivatives " << numberOfValues;
	for (size_t i = 0; i < numberOfValues; i++) message_builder << " " << valueReference[i];
	for (size_t i = 0; i < numberOfValues; i++) message_builder << " " << order[i];
	component->channel.sendMessage(message_builder.str());
	return parser->extractReal(component->channel.receiveMessage(), values);
}

fmi2Status fmi2DoStep(fmi2Component _component, fmi2Real currentComunicationTime, fmi2Real stepSize,
	fmi2Boolean noSetFmuFmuStatePriorToCurrentPoint) {
	auto component = reinterpret_cast<Component*>(_component);
	std::stringstream message_builder;
	message_builder << "doStep " << std::scientific << currentComunicationTime << " " << std::scientific << stepSize;
	component->channel.sendMessage(message_builder.str());
	return parser->extractStatus(component->channel.receiveMessage());
}

fmi2Status fmi2CancelStep(fmi2Component _component) {
	auto component = reinterpret_cast<Component*>(_component);
	component->channel.sendMessage("cancelStep");
	return parser->extractStatus(component->channel.receiveMessage());
}

fmi2Status fmi2GetStatus(fmi2Component _component, const fmi2StatusKind kind, fmi2Status* status) {
	return fmi2Error;
}

fmi2Status fmi2GetRealStatus(fmi2Component _component, const fmi2StatusKind kind, fmi2Real* value) {
	return fmi2Error;
}

fmi2Status fmi2GetIntegerStatus(fmi2Component _component, const fmi2StatusKind kind, fmi2Integer* value) {
	return fmi2Error;
}

fmi2Status fmi2GetBooleanStatus(fmi2Component _component, const fmi2StatusKind kind, fmi2Boolean* value) {
	return fmi2Error;
}

fmi2Status fmi2GetStringStatus(fmi2Component _component, const fmi2StatusKind kind, fmi2String* value) {
	return fmi2Error;
}
