#include <iostream>
#include "CommunicationHandle.h"
using namespace Poco;

CommunicationHandle::
CommunicationHandle(shared_ptr<ProcessHandle> process, shared_ptr<PipeOutputStream> process_in, shared_ptr<PipeInputStream> process_out,
                    shared_ptr<PipeInputStream> process_error)
	: process(process),
	  process_in{process_in},
	  process_out{process_out},
	  process_error{process_error} { }

void CommunicationHandle::sendMessage(const string& message) {
	if (Process::isRunning(*process)) *process_in << message << std::endl;
}

string CommunicationHandle::receiveMessage() {
	if (!Process::isRunning(*process)) return "fatal";
	process_error->getline(buffer, BUFFER_SIZE);
	string result{buffer};
	while (process_error->readsome(buffer, BUFFER_SIZE)) {
		result += string{buffer};
	}
	return result;
}

shared_ptr<std::istream> CommunicationHandle::outChannel() {
	return process_out;
}
