#include "catch.hpp"
#include "../src/shelllibrary/LogParser.h"
#include <string>
using std::string;

TEST_CASE("LogParserTest") {
	string input{ "anyName:anyCategory:any text" };

	SECTION("extract instanceName from input") {
		REQUIRE(LogParser{ input }.instanceName() == "anyName");
	}

	SECTION("extract category from input") {
		REQUIRE(LogParser{ input }.category() == "anyCategory");
	}

	SECTION("extract text from input") {
		REQUIRE(LogParser{ input }.text() == "any text");
	}
}