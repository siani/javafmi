//#define CATCH_CONFIG_MAIN (uncomment for linux)
#include "fmi/fmi2Functions.h"
#include <iostream>
#include <cstring>
#include "catch.hpp"

using std::cout;
using std::endl;
using std::string;

#if defined(_WIN64)
string resourcesPath{ "D:/Users/jevora/repositories/javafmi/shell/resources/car/resources" };
#else
string resourcesPath{ "/home/oroncal/workspace/javafmi2/javafmi/shell/resources/car/resources" };
#endif

void loggerCar(fmi2ComponentEnvironment, fmi2String, fmi2Status, fmi2String, fmi2String message, ...) {
    std::cout << message << std::endl;
};

void* allocateMemory(size_t size, size_t block) {
	return malloc(size * block);
};

void freeMemory(void* element) {
	free(element);
};

TEST_CASE("CarFmuExample") {
	fmi2ValueReference valueReferences[2];
	fmi2Real realValues[2];
	fmi2Integer intValues[2];
	fmi2Boolean booleanValues[2];
	fmi2String stringValues[1];
    fmi2CallbackFunctions callbacks{loggerCar, allocateMemory, freeMemory, nullptr, nullptr};
	int speedReference = 1;
	int distanceReference = 2;
	int numberOfDoorsReference = 3;
	int maxPowerReference = 4;
	int absReference = 5;
	int mp3Reference = 6;
	int gasolineReference = 7;
	int songReference = 8;
	auto component = fmi2Instantiate("Car",
	                                 fmi2CoSimulation,
	                                 "guid",
	                                 string{"file://" + resourcesPath }.c_str(),
									 &callbacks,
	                                 fmi2False,
	                                 fmi2False);

	CHECK(fmi2SetupExperiment(component, fmi2False, 0.001, 0.0, fmi2False, 1000.) == fmi2OK) ;
	CHECK(fmi2EnterInitializationMode(component) == fmi2OK) ;
	CHECK(fmi2ExitInitializationMode(component) ==fmi2OK) ;

	/* Stepping */
	valueReferences[0] = speedReference;
	realValues[0] = 40.;
	CHECK(fmi2SetReal(component, valueReferences, 1, realValues) == fmi2OK) ;

	CHECK(fmi2DoStep(component, 0., 1800., 0) == fmi2OK) ;

	valueReferences[0] = distanceReference;
	CHECK(fmi2GetReal(component, valueReferences, 1, realValues) == fmi2OK) ;
	CHECK(realValues[0] == 20000) ;

	CHECK(fmi2DoStep(component, 0., 1800., 0) == fmi2OK) ;

	valueReferences[0] = distanceReference;
	CHECK(fmi2GetReal(component, valueReferences, 1, realValues) == fmi2OK) ;
	CHECK(realValues[0] == 40000) ;

	/* States */
	fmi2FMUstate state = nullptr;
	CHECK(fmi2GetFMUstate(component, &state) == fmi2OK);
	CHECK((int)state == 0);

	fmi2FMUstate state2 = nullptr;
	CHECK(fmi2GetFMUstate(component, &state2) == fmi2OK);
	CHECK((int)state2 == 1);
	CHECK((int)state == 0);

	CHECK(fmi2DoStep(component, 0., 1800., 0) == fmi2OK);

	valueReferences[0] = distanceReference;
	CHECK(fmi2GetReal(component, valueReferences, 1, realValues) == fmi2OK);
	CHECK(realValues[0] == 60000);

	CHECK(fmi2GetFMUstate(component, &state2) == fmi2OK);
	CHECK((int)state == 0);

	CHECK(fmi2SetFMUstate(component, state) == fmi2OK);
	valueReferences[0] = distanceReference;
	CHECK(fmi2GetReal(component, valueReferences, 1, realValues) == fmi2OK);
	CHECK(realValues[0] == 40000);

	CHECK(fmi2SetFMUstate(component, state2) == fmi2OK);
	valueReferences[0] = distanceReference;
	CHECK(fmi2GetReal(component, valueReferences, 1, realValues) == fmi2OK);
	CHECK(realValues[0] == 60000);
	
	/* Multiple setting and getting*/

	/* Real */
	valueReferences[0] = speedReference;
	valueReferences[1] = distanceReference;
	CHECK(fmi2GetReal(component, valueReferences, 2, realValues) == fmi2OK);
	CHECK(realValues[0] == 40);
	CHECK(realValues[1] == 60000);

	realValues[0] = 80;
	CHECK(fmi2SetReal(component, valueReferences, 2, realValues) == fmi2Error);

	CHECK(fmi2GetReal(component, valueReferences, 2, realValues) == fmi2OK);
	CHECK(realValues[0] == 80);
	CHECK(realValues[1] == 60000);

	realValues[0] = std::numeric_limits<double>::quiet_NaN();
	CHECK(fmi2SetReal(component, valueReferences, 1, realValues) == fmi2OK);

	CHECK(fmi2GetReal(component, valueReferences, 1, realValues) == fmi2OK);
	CHECK(std::isnan(realValues[0]));

	/* Integer */
	valueReferences[0] = numberOfDoorsReference;
	valueReferences[1] = maxPowerReference;
	CHECK(fmi2GetInteger(component, valueReferences, 2, intValues) == fmi2OK);
	CHECK(intValues[0] == 5);
	CHECK(intValues[1] == 90);

	intValues[0] = 3;
	intValues[1] = 100;
	CHECK(fmi2SetInteger(component, valueReferences, 2, intValues) == fmi2OK);

	CHECK(fmi2GetInteger(component, valueReferences, 2, intValues) == fmi2OK);
	CHECK(intValues[0] == 3);
	CHECK(intValues[1] == 100);

	/* Boolean */
	valueReferences[0] = absReference;
	valueReferences[1] = mp3Reference;
	CHECK(fmi2GetBoolean(component, valueReferences, 2, booleanValues) == fmi2OK);
	CHECK(booleanValues[0] == fmi2True);
	CHECK(booleanValues[1] == fmi2True);

	booleanValues[0] = fmi2True;
	booleanValues[1] = fmi2False;
	CHECK(fmi2SetBoolean(component, valueReferences, 2, booleanValues) == fmi2OK);

	CHECK(fmi2GetBoolean(component, valueReferences, 2, booleanValues) == fmi2OK);
	CHECK(booleanValues[0] == fmi2True);
	CHECK(booleanValues[1] == fmi2False);

	/* Strings -> multiple not implemented */

	valueReferences[0] = gasolineReference;
	CHECK(fmi2GetString(component, valueReferences, 1, stringValues) == fmi2OK);
	CHECK(strcmp(stringValues[0], "Diesel") == 0);

	stringValues[0] = "Petrol";
	CHECK(fmi2SetString(component, valueReferences, 1, stringValues) == fmi2OK);

	CHECK(fmi2GetString(component, valueReferences, 1, stringValues) == fmi2OK);
	CHECK(strcmp(stringValues[0], "Petrol") == 0);

	valueReferences[0] = songReference;
	CHECK(fmi2GetString(component, valueReferences, 1, stringValues) == fmi2OK);
	CHECK(strcmp(stringValues[0], "init") == 0);
	cout << stringValues[0];

	stringValues[0] = "this song is ok\nI love it";
	CHECK(fmi2SetString(component, valueReferences, 1, stringValues) == fmi2OK);

	CHECK(fmi2GetString(component, valueReferences, 1, stringValues) == fmi2OK);
	CHECK(strcmp(stringValues[0], "this song is ok\nI love it") == 0);

    /** DECIMALS **/
    fmi2DoStep(component, 0, 2.21324, 0);
	valueReferences[0] = distanceReference;
	CHECK(fmi2GetReal(component, valueReferences, 1, realValues) == fmi2OK);
	CHECK(realValues[0] == 60049.18311111111);

	/* Getting / setting derivatives */
	CHECK(fmi2SetRealInputDerivatives(component, valueReferences, 2, intValues, realValues) == fmi2OK);
	CHECK(fmi2GetRealOutputDerivatives(component, valueReferences, 2, intValues, realValues) == fmi2OK);

	/* Terminating*/
	CHECK(fmi2Terminate(component) == fmi2OK);

	fmi2FreeInstance(component);
}
