#include "fmi/fmi2Functions.h"
#include <iostream>
#include <cstring>
#include "catch.hpp"

using std::cout;
using std::endl;
using std::string;

#if defined(_WIN64)
string resourcesPathBarrel{ "D:/Users/jevora/repositories/javafmi/shell/resources/tankbarrel/resources" };
#else
string resourcesPath{ "/home/oroncal/workspace/javafmi2/javafmi/shell/resources/tankbarrel/resources" };
#endif

void loggerBarrel(fmi2ComponentEnvironment, fmi2String, fmi2Status, fmi2String, fmi2String message, ...) {
	std::cout << message << std::endl;
};

void* allocateMemoryBarrel(size_t size, size_t block) {
	return malloc(size * block);
};

void freeMemoryBarrel(void* element) {
	free(element);
};

TEST_CASE("TankBarrelExample") {
	fmi2CallbackFunctions callbacks{ loggerBarrel, allocateMemoryBarrel, freeMemoryBarrel, nullptr, nullptr };
	fmi2Real realValues[2];
	fmi2ValueReference references[] = {9, 10};
	auto component = fmi2Instantiate("Matryoshka",
		fmi2CoSimulation,
		"guid",
		string{ "file://" + resourcesPathBarrel}.c_str(),
		&callbacks,
		fmi2False,
		fmi2False);

	CHECK(fmi2SetupExperiment(component, fmi2False, 0.001, 0.0, fmi2False, 1000.) == fmi2OK);
	CHECK(fmi2EnterInitializationMode(component) == fmi2OK);
	CHECK(fmi2ExitInitializationMode(component) == fmi2OK);

	/* Stepping */
	double time = 0;
	while (time < 18) {
		fmi2DoStep(component, time, 0.001, 0);
		fmi2GetReal(component, references, 2, realValues);
		time += 0.001;
	}

	fmi2FreeInstance(component);
}
