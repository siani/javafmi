#include "fmi/fmi2Functions.h"
#include "catch.hpp"
#include <Poco/File.h>
#include <Poco/Path.h>
#include <string>

using namespace Poco;
using std::string;

TEST_CASE("Energy9Test") {
	Path path = Path{Path::current()}
	            .pushDirectory("runtime")
	            .pushDirectory("energy9")
	            .pushDirectory("resources");

	auto energy9_resources = File(path);
	REQUIRE(energy9_resources.exists()) ;


	auto component = fmi2Instantiate("Energy9",
	                                 fmi2CoSimulation,
	                                 "guid",
	                                 string{"file://" + path.toString(Path::PATH_UNIX)}.c_str(),
	                                 nullptr,
	                                 fmi2False,
	                                 fmi2False);

	auto setup_status = fmi2SetupExperiment(component, fmi2False, 1e-5, 0., fmi2False, 1.);
	auto enter_init_status = fmi2EnterInitializationMode(component);
	auto exit_init_status = fmi2ExitInitializationMode(component);
	auto step_status = fmi2DoStep(component, 0.0, 0.02, false);
	auto terminate_status = fmi2Terminate(component);

	CHECK(setup_status == fmi2OK) ;
	CHECK(enter_init_status == fmi2OK) ;
	CHECK(exit_init_status == fmi2OK) ;
	CHECK(step_status == fmi2OK) ;
	CHECK(terminate_status == fmi2OK) ;
}
