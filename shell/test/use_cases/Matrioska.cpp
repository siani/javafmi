#include <fmi/fmi2Functions.h>
#include "catch.hpp"
#include <string>
#include <iostream>

using std::string;

#if defined(_WIN64)
string matrioskaResourcesPath{ "D:/Users/jevora/repositories/javafmi/shell/resources/matryoshka/resources" };
#else
string resourcesPath{ "/media/sf_jcortes/runtime/matryoshka/resources" };
#endif

void loggerMatrioska(fmi2ComponentEnvironment, fmi2String, fmi2Status, fmi2String, fmi2String message, ...) {
	std::cout << message << std::endl;
};

void* allocateMemoryMatrioska(size_t size, size_t block) {
	return malloc(size * block);
};

TEST_CASE("MatrioskaTest") {
	fmi2CallbackFunctions callbacks{ loggerMatrioska, allocateMemoryMatrioska, nullptr, nullptr, nullptr };
	auto component = fmi2Instantiate("Matrioska",
	                                 fmi2CoSimulation,
	                                 "guid",
									 string{ "file://" + matrioskaResourcesPath }.c_str(),
	                                 &callbacks,
	                                 fmi2False,
	                                 fmi2False);

	CHECK(fmi2SetupExperiment(component, fmi2False, 0.001, 0.0, fmi2False, 1000.) == fmi2OK) ;
	CHECK(fmi2EnterInitializationMode(component) == fmi2OK) ;
	CHECK(fmi2ExitInitializationMode(component) == fmi2OK) ;
	std::vector<unsigned int> valueReferences{ 10000, 10001 };
	std::vector<double> values{10., 10.};
	
	CHECK(fmi2SetReal(component, valueReferences.data(), valueReferences.size(), values.data()) == fmi2OK);
	CHECK(fmi2DoStep(component, 0., 3600., 0) == fmi2OK);
	CHECK(fmi2Terminate(component) == fmi2OK);

	//fmi2FreeInstance(component);
}
