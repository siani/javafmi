/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.builder;

import java.util.HashMap;
import java.util.Map;

public abstract class Library implements Resource {
	private static final Map<String, String> extensions;
	protected final Platform platform;
	private final String undecoratedName;

	static {
		extensions = new HashMap<>();
		extensions.put(Platform.Win32.os(), ".dll");
		extensions.put(Platform.Linux32.os(), ".so");
	}

	public Library(Platform platform, String undecoratedName) {
		this.platform = platform;
		this.undecoratedName = undecoratedName;
	}
	
	public String os() {
		return platform.os();
	}

	public String arch() {
		return platform.arch();
	}

	public String extension() {
		return extensions.get(platform.os());
	}

	public String undecoratedName() {
		return undecoratedName;
	}
}
