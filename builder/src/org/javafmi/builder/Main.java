/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.builder;

import org.javafmi.builder.commandline.*;
import org.javafmi.builder.view.FmuBuilderWindow;
import org.javafmi.builder.view.configuration.ConfigurationsRepository;
import org.javafmi.builder.view.configuration.WindowConfigurationDeSerializer;
import org.javafmi.builder.view.configuration.WindowConfigurationSerializer;

import java.io.File;

public class Main {
    public static void main(String[] args) {
        if (args.length == 0) openGUI();
        else openCLI(args);
    }

    private static FmuBuilderWindow openGUI() {
        return new FmuBuilderWindow(new ConfigurationsRepository(Platform.Win64, new WindowConfigurationDeSerializer(), new WindowConfigurationSerializer()));
    }

    private static void openCLI(String[] args) {
        CommandLineArgumentsParser parser = new CommandLineArgumentsParser("java -jar simulation user.jar [-modelName fmu_name] [-i path/to/dependency.jar ...] [-p platform]");
        parser.operands(new String[]{"user.jar"}, new UserJarChecker());
        parser.addOptionParser("[-p]", "supported platforms: win64 lin64", new PlatformParser());
        parser.addOptionParser("[-i]", "include: any kind of resource to be included in the FMU", new IncludesParser());
        parser.addOptionParser("[-n]", "fmu modelName", new FmuNameParser());
        BuilderOptions options = new BuilderOptions(parser.parse(args));
        File resultFmu = new ShellFmuBuilder(options).build();
        System.out.println("Created " + resultFmu.getName());
    }

}
