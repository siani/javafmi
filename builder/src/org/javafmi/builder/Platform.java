/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.builder;

import org.javafmi.builder.platforms.Linux;
import org.javafmi.builder.platforms.Windows;

import java.util.HashMap;
import java.util.Map;

public abstract class Platform {
    public static final Platform Win64 = new Windows("64");
    public static final Platform Win32 = new Windows("32");
    public static final Platform Linux64 = new Linux("64");
    public static final Platform Linux32 = new Linux("32");

    private static final Map<String, Platform> platforms;
    protected String os;
    protected String arch;

    static {
        platforms = new HashMap<>();
        platforms.put("win64", Win64);
        platforms.put("win32", Win32);
        platforms.put("lin64", Linux64);
        platforms.put("lin32", Linux32);
    }

    public static Platform fromID(String platformID) {
        return platforms.get(normalize(platformID));
    }

    private static String normalize(String platformID) {
        return platformID.toLowerCase()
                .replace("linux", "lin")
                .replace("windows", "win")
                .replace("mac", "darwin");
    }

    public String os() {
        return os;
    }

    public String arch() {
        return arch;
    }

    @Override
    public String toString() {
        return os + arch;
    }

    public abstract String localAppDataDirectory();
}
