/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.builder;

import org.javafmi.framework.ModelDescription;
import org.javafmi.framework.ModelDescription.*;
import org.siani.itrules.Template;
import org.siani.itrules.model.Frame;
import org.siani.itrules.model.PrimitiveFrame;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

public class ModelDescriptionSerializer {

    public static String serialize(ModelDescription modelDescription) {
        Template template = ModelDescriptionTemplate.create();
        return template.format(frameOf(modelDescription));
    }

    private static Frame frameOf(Object object) {
        Frame frame = new Frame(typesOf(object));
        Method[] methods = object.getClass().getDeclaredMethods();
        for (Method method : methods)
            fill(frame, method, value(object, method));
        return frame;
    }

    private static String typesOf(Object object) {
        if (object.getClass().getSimpleName().isEmpty())
            return object.getClass().getInterfaces()[0].getSimpleName();
        return object.getClass().getSimpleName();
    }

    @SuppressWarnings("unchecked")
    private static void fill(Frame frame, Method method, Object object) {
        if (isList(method)) ((List) object).forEach(o -> frame.addSlot(method.getName(), createFrameWith(o)));
        else if (isComposite(method)) frame.addSlot(method.getName(), createFrameWith(object));
        else fill(frame, method.getName(), object);
    }

    private static Frame createFrameWith(Object object) {
        return frameOf(object).addTypes(typeOf(object));
    }

    private static String typeOf(Object object) {
        return object.getClass().getInterfaces()[0].getSimpleName();
    }

    private static void fill(Frame frame, String name, Object value) {
        if (value == null) return;
        frame.addSlot(name, new PrimitiveFrame(name + "=\"" + value.toString() + "\""));
    }

    private static Object value(Object object, Method method) {
        try {
            method.setAccessible(true);
            return method.invoke(object);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static boolean isList(Method method) {
        return method.getReturnType().isAssignableFrom(List.class);
    }

    private static boolean isComposite(Method method) {
        Class<?> returnType = method.getReturnType();
        return returnType.isAssignableFrom(CoSimulation.class) ||
                returnType.isAssignableFrom(DefaultExperiment.class) ||
                returnType.isAssignableFrom(ScalarVariable.class) ||
                returnType.isAssignableFrom(ModelStructure.class) ||
                returnType.isAssignableFrom(Outputs.class) ||
                returnType.isAssignableFrom(Derivatives.class) ||
                returnType.isAssignableFrom(InitialUnknowns.class) ||
                returnType.isAssignableFrom(Unknown.class);
    }


}
