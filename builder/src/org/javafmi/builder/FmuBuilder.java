/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.builder;

import org.javafmi.kernel.Zipper;

import java.io.*;

import static org.javafmi.kernel.Files.*;

public class FmuBuilder {

    protected File workingDirectory;
    protected final String fmuName;
    private final String modelName;

    public FmuBuilder(String fmuName, String workingDirectory) {
        this.fmuName = normalizeFmuName(fmuName);
        this.modelName = new File(fmuName).getName().replace(".fmu", "");
        this.workingDirectory = new File(workingDirectory, "fmu_build_folder").getAbsoluteFile();
        this.workingDirectory.mkdirs();
        this.workingDirectory.deleteOnExit();
    }

    private String normalizeFmuName(String fmuName) {
        String fixed = fmuName;
        if (!fixed.endsWith(".fmu")) fixed += ".fmu";
        return fixed;
    }

    public FmuBuilder addModelDescription(InputStream modelDescription) {
        BufferedOutputStream target = bufferedOutputStream(fileOutputStream(createFile(workingDirectory, "modelDescription.xml")));
        copyFromTo(modelDescription, target);
        close(modelDescription);
        close(target);
        return this;
    }

    public FmuBuilder addLibrary(Library library) {
        BufferedInputStream source = bufferedInputStream(library.getInputStream());
        BufferedOutputStream target = bufferedOutputStream(fileOutputStream(createFile(createDirectory(workingDirectory, "binaries/" + library.os() + library.arch()), modelName + library.extension())));
        copyFromTo(source, target);
        close(source);
        close(target);
        return this;
    }

    public FmuBuilder addResource(InputStream resource, String resourceName) {
        createDirectory(workingDirectory, "resources");
        FileOutputStream target = fileOutputStream(createFile(createDirectory(workingDirectory, "resources"), resourceName));
        copyFromTo(resource, target);
        close(resource);
        close(target);
        return this;
    }

    public File buildFmu() {
        File fmu = new Zipper().zipDirectory(workingDirectory, fmuName);
        deleteAll(workingDirectory);
        return fmu;
    }

    public String fmuName() {
        return fmuName;
    }

    public String modelName() {
        return modelName;
    }
}
