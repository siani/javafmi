/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.builder;

import org.javafmi.builder.commandline.*;
import org.javafmi.framework.FmiSimulation;
import org.javafmi.framework.ModelDescription;
import org.javafmi.kernel.Convention;
import org.javafmi.kernel.JarClassLoader;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.lang.reflect.Constructor;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.javafmi.builder.ModelDescriptionSerializer.serialize;
import static org.javafmi.kernel.Convention.*;
import static org.javafmi.kernel.Convention.FmuClassPackage;
import static org.javafmi.kernel.Files.fileInputStream;

public class ShellFmuBuilder {
    private FmuBuilder builder;
    private String jarFile;
    private String fmuName;
    private String workingDirectory;
    private List<String> includes;
    private List<Platform> platforms;

    public ShellFmuBuilder(String jarFile) {
        HashMap<String, String> map = new HashMap<>();
        map.put(BuilderOptions.USER_JAR, jarFile);
        setOptions(new BuilderOptions(map));
    }

    public ShellFmuBuilder(BuilderOptions options) {
        setOptions(options);
    }

    private void setOptions(BuilderOptions options) {
        jarFile = options.userJar();
        fmuName = options.fmuName();
        includes = options.includes();
        workingDirectory = options.workingDirectory();
        platforms = options.platforms()
                .stream()
                .map(Platform::fromID)
                .collect(toList());
    }

    public File build() {
        builder = new FmuBuilder(fmuName, workingDirectory);
        File userJarFile = new File(jarFile).getAbsoluteFile();
        addUserJar(userJarFile);
        addModelDescription(userJarFile);
        platforms.stream().forEach(this::addLibrary);
        addDependencies();
        addManifest();
        return builder.buildFmu();
    }

    private void addUserJar(File userJarFile) {
        if (!userJarFile.exists())
            throw new RuntimeException(userJarFile.getAbsolutePath() + " does not exists or it is not a valid path");
        builder.addResource(fileInputStream(userJarFile), userJarFile.getName());
    }

    private void addModelDescription(File userJarFile) {
        builder.addModelDescription(new ByteArrayInputStream(serialize(modelDescriptionFrom(userJarFile)).getBytes()));
    }

    private void addLibrary(Platform platform) {
        builder.addLibrary(new ShellLibrary(platform));
    }

    private void addDependencies() {
        includes.stream()
                .filter(resource -> !resource.isEmpty())
                .map(File::new)
                .filter(File::exists)
                .forEach(resource -> builder.addResource(fileInputStream(resource), resource.getName()));
    }

    private void addManifest() {
        ByteArrayInputStream manifest = new ByteArrayInputStream((FmuClassPackage + builder.modelName()).getBytes());
        builder.addResource(manifest, "MANIFEST");
    }

    private ModelDescription modelDescriptionFrom(File userJar) {
        FmiSimulation fmiSimulation = instantiateFmiSimulationClass(userJar.getAbsoluteFile());
        fmiSimulation.generationTool(FrameworkManufacturer + " FMU Builder v" + FrameworkVersion);
        return fmiSimulation.modelDescription();
    }

    private FmiSimulation instantiateFmiSimulationClass(File userJar) {
        try {
            String className = FmuClassPackage + builder.modelName();
            Class fmiAccessClass = new JarClassLoader(userJar).loadClass(className);
            Constructor fmiSimulationConstructor = fmiAccessClass.getConstructor();
            return (FmiSimulation) fmiSimulationConstructor.newInstance();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public ShellFmuBuilder fmuName(String fmuName) {
        this.fmuName = fmuName;
        return this;
    }

    public ShellFmuBuilder workingDirectory(String directory) {
        this.workingDirectory = directory;
        return this;
    }

    public ShellFmuBuilder includes(String... filesToInclude) {
        Collections.addAll(includes, filesToInclude);
        return this;
    }

    public ShellFmuBuilder platforms(List<Platform> platforms) {
        this.platforms = platforms;
        return this;
    }
}
