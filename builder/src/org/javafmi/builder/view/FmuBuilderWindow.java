/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.builder.view;

import org.javafmi.builder.Platform;
import org.javafmi.builder.ShellFmuBuilder;
import org.javafmi.builder.view.configuration.ConfigurationsRepository;
import org.javafmi.builder.view.configuration.WindowConfiguration;
import org.javafmi.builder.view.configuration.WindowConfigurationBuilder;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class FmuBuilderWindow {

	private final JFrame window;
	private JTextField outputDirectoryTextField = new JTextField();
	private JTextField jarFileTextField = new JTextField();
	private JCheckBox win64CheckBox = new JCheckBox("Windows 64");
	private JCheckBox lin64CheckBox = new JCheckBox("Linux 64");
	private JTextArea resourcesTextArea = new JTextArea();
	private JCheckBox autoBuildCheckBox = new JCheckBox("Auto build");
	private DefaultListModel<String> listModel = new DefaultListModel<>();
	private JList<String> jlist = new JList<>(listModel);
	private Dimension labelSize = new Dimension(100, 30);
	private Dimension textFieldSize = new Dimension(300, 30);
	private Dimension windowSize = new Dimension(750, 400);
	private List<WindowConfiguration> configurations;

	public FmuBuilderWindow(ConfigurationsRepository repository) {
		window = new JFrame("SIANI Fmu Builder");
		window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		window.addWindowListener(new SaveOnExit(repository));
		window.setMinimumSize(windowSize);
		window.setPreferredSize(windowSize);
		window.setMaximumSize(windowSize);
		Container pane = window.getContentPane();
		pane.setLayout(new BoxLayout(pane, BoxLayout.X_AXIS));
		pane.add(fmuListPanel(), BorderLayout.WEST);
		pane.add(configPanel(), BorderLayout.CENTER);
		window.setVisible(true);
		configurations = repository.loadWindowConfigurations();
		configurations.forEach(this::addConfiguration);
		jlist.addListSelectionListener(listener -> {
			if (nothingSelected()) return;
			configure(configurations.get(jlist.getSelectedIndex()));
		});
		jlist.setSelectedIndex(0);
	}

	private JPanel fmuListPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		jlist.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		JScrollPane scrollPane = new JScrollPane(jlist);
		panel.setPreferredSize(new Dimension(150, 400));
		panel.setMaximumSize(new Dimension(150, 400));
		JPanel scrollPaneModifiers = new JPanel(new FlowLayout());
		panel.add(scrollPane, BorderLayout.CENTER);
		JButton addFmu = new JButton("+");
		JButton removeFmu = new JButton("-");
		addFmu.addActionListener(e -> {
			String jarPath = openJarFileDialog(panel);
			WindowConfiguration configuration = defaultConfiguration(filenameOf(jarPath), jarPath);
			configurations.add(listModel.size(), configuration);
			listModel.addElement(configuration.outputName());
			jlist.setSelectedIndex(listModel.getSize() - 1);
			removeFmu.setEnabled(listModel.getSize() > 0);
			panel.repaint();
		});
		scrollPaneModifiers.add(addFmu);
		removeFmu.addActionListener(e -> {
			int index = jlist.getSelectedIndex();
			configurations.remove(index);
			listModel.removeElementAt(index);
			if (index > 0) jlist.setSelectedIndex(index - 1);
			if (index == 0 && !listModel.isEmpty()) jlist.setSelectedIndex(0);
			else {
				jarFileTextField.setText("");
				outputDirectoryTextField.setText("");
			}
			removeFmu.setEnabled(listModel.getSize() > 0);

		});
		scrollPaneModifiers.add(removeFmu);
		panel.add(scrollPaneModifiers, BorderLayout.PAGE_START);
		panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		return panel;
	}

	private String filenameOf(String path) {
		return new File(path).getName().replace(".jar", ".fmu");
	}

	private JPanel configPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.add(panelForJarFile());
		panel.add(panelForOutputDirectory());
		panel.add(panelForFmuConfiguration());
		panel.add(panelForResources());
		panel.add(panelForBuild());
		panel.setPreferredSize(new Dimension(550, 400));
		panel.setMaximumSize(new Dimension(550, 400));
		panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		return panel;
	}

	private boolean nothingSelected() {
		return jlist.getSelectedIndex() < 0;
	}

	private void configure(WindowConfiguration configuration) {
		outputDirectoryTextField.setText(configuration.outputDirectory());
		jarFileTextField.setText(configuration.jarFile());
		win64CheckBox.setSelected(configuration.win64());
		lin64CheckBox.setSelected(configuration.lin64());
		resourcesTextArea.setText(configuration.resources());
		autoBuildCheckBox.setSelected(configuration.autoBuild());
	}

	private WindowConfiguration defaultConfiguration(String fmuName, String jarPath) {
		return new WindowConfigurationBuilder(fmuName)
				.jarFile(jarPath)
				.outputDirectory(new File(jarPath).getParent())
				.win64(true)
				.lin64(true)
				.standalone(true)
				.lightweight(false)
				.release(true)
				.debug(false)
				.resources("")
				.autoBuild(false)
				.build();
	}

	private JPanel panelForJarFile() {
		JPanel pane = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel label = new JLabel("Jar file:");
		label.setPreferredSize(labelSize);
		pane.add(label);
		jarFileTextField.setPreferredSize(textFieldSize);
		pane.add(jarFileTextField);
		JButton changeJarButton = new JButton("Change");
		changeJarButton.addActionListener(e -> openJarFileDialog(pane));
		pane.add(changeJarButton);
		return pane;
	}

	private String openJarFileDialog(JPanel pane) {
		String workingDirectory = jarFileTextField.getText();
		if (workingDirectory.isEmpty()) workingDirectory = "./";
		JFileChooser fileChooser = new JFileChooser(workingDirectory);
		fileChooser.setFileFilter(new FileFilter() {
			@Override
			public boolean accept(File f) {
				return f != null && (f.isDirectory() || f.getName().endsWith(".jar"));
			}

			@Override
			public String getDescription() {
				return "select a valid jar file";
			}
		});
		int choice = fileChooser.showOpenDialog(pane);
		if (choice == JFileChooser.APPROVE_OPTION) return fileChooser.getSelectedFile().toString();
		return "";
	}

	private JPanel panelForOutputDirectory() {
		JPanel pane = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel label = new JLabel("Output directory:");
		label.setPreferredSize(labelSize);
		pane.add(label);
		outputDirectoryTextField.setPreferredSize(textFieldSize);
		pane.add(outputDirectoryTextField);
		JButton outputDirectoryButton = new JButton("Change");
		outputDirectoryButton.addActionListener(e -> {
			String workingDirectory = outputDirectoryTextField.getText();
			if (workingDirectory.isEmpty()) workingDirectory = "./";
			JFileChooser fileChooser = new JFileChooser(workingDirectory);
			fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			if (fileChooser.showOpenDialog(pane) == JFileChooser.APPROVE_OPTION)
				outputDirectoryTextField.setText(fileChooser.getSelectedFile().toString());
		});

		pane.add(outputDirectoryButton);
		return pane;
	}

	private JPanel panelForFmuConfiguration() {
		JPanel pane = new JPanel();
		pane.setLayout(new GridLayout(2, 3));
		pane.add(win64CheckBox);
		pane.add(lin64CheckBox);
		return pane;
	}

	private JPanel panelForResources() {
		JPanel pane = new JPanel(new BorderLayout());
		JPanel paneForButton = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		JButton addResourceButton = new JButton("Add resource");
		addResourceButton.addActionListener(e -> {
			JFileChooser fileChooser = new JFileChooser("./");
			int choice = fileChooser.showOpenDialog(pane);
			if (choice == JFileChooser.APPROVE_OPTION) {
				resourcesTextArea.append(fileChooser.getSelectedFile().getAbsolutePath() + "\n");
			}
		});
		paneForButton.add(addResourceButton);
		pane.add(resourcesTextArea, BorderLayout.CENTER);
		pane.add(paneForButton, BorderLayout.PAGE_END);
		return pane;
	}

	private JPanel panelForBuild() {
		JPanel pane = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		JProgressBar bar = new JProgressBar();
		bar.setIndeterminate(true);
		bar.setVisible(false);
		pane.add(bar);
		pane.add(autoBuildCheckBox);
		JButton buildButton = new JButton("Build");
		buildButton.addActionListener(e -> {
			bar.setVisible(true);
			buildButton.setEnabled(false);
			new SwingWorker() {
				@Override
				protected Object doInBackground() throws Exception {
					try {
						if (!win64CheckBox.isSelected() && !lin64CheckBox.isSelected()) errorDialog("Select a platform");
						else build();
					} catch (Exception e) {
						errorDialog(e.getCause().getMessage());
					}
					return null;
				}

				@Override
				protected void done() {
					buildButton.setEnabled(true);
					bar.setVisible(false);
				}
			}.execute();
		});
		pane.add(buildButton);
		return pane;
	}

	private void errorDialog(String message) {
		JDialog frame = new JDialog(window, "FMU Generation Failed", true);
		Dimension parentSize = window.getSize();
		Point p = window.getLocation();
		frame.setLocation(p.x + parentSize.width / 4, p.y + parentSize.height / 4);
		frame.setMinimumSize(new Dimension(parentSize.width / 2, parentSize.height / 4));
		JPanel messagePanel = new JPanel();
		messagePanel.add(new JLabel(message));
		JPanel buttonPanel = new JPanel();
		JButton button = new JButton("OK");
		button.addActionListener(e1 -> frame.dispose());
		buttonPanel.add(button);
		frame.getContentPane().add(messagePanel, BorderLayout.CENTER);
		frame.getContentPane().add(buttonPanel, BorderLayout.SOUTH);
		frame.pack();
		frame.setVisible(true);
	}

	private void build() {
		WindowConfiguration configuration = currentConfiguration();
		ArrayList<Platform> platforms = new ArrayList<>();
		if (configuration.win64()) platforms.add(Platform.Win64);
		if (configuration.lin64()) platforms.add(Platform.Linux64);
		new ShellFmuBuilder(configuration.jarFile())
				.fmuName(Paths.get(outputDirectoryTextField.getText(), configuration.outputName()).toAbsolutePath().toString())
				.includes(configuration.resources().split("\n"))
				.platforms(platforms)
				.build();
	}

	private WindowConfiguration getConfiguration(int index) {
		return new WindowConfigurationBuilder(listModel.elementAt(index))
				.outputDirectory(outputDirectoryTextField.getText())
				.jarFile(jarFileTextField.getText())
				.win64(win64CheckBox.isSelected())
				.lin64(lin64CheckBox.isSelected())
				.standalone(true)
				.lightweight(false)
				.release(true)
				.debug(false)
				.resources(resourcesTextArea.getText())
				.autoBuild(autoBuildCheckBox.isSelected())
				.build();
	}

	private WindowConfiguration currentConfiguration() {
		return getConfiguration(jlist.getSelectedIndex());
	}

	private void addConfiguration(WindowConfiguration configuration) {
		listModel.addElement(configuration.outputName());
		configure(configuration);
	}

	private class SaveOnExit extends WindowAdapter {

		private final ConfigurationsRepository repository;

		public SaveOnExit(ConfigurationsRepository repository) {
			this.repository = repository;
		}

		@Override
		public void windowClosing(WindowEvent e) {
			if (jlist.getSelectedIndex() >= 0)
				configurations.set(jlist.getSelectedIndex(), getConfiguration(jlist.getSelectedIndex()));
			repository.save(configurations.stream().collect(toList()));
		}
	}

}
