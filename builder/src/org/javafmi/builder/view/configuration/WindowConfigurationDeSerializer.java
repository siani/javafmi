/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.builder.view.configuration;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.javafmi.kernel.Files.copyFromTo;
import static org.javafmi.kernel.Files.fileInputStream;

public class WindowConfigurationDeSerializer {

    public List<WindowConfiguration> deSerialize(Path path) {
        if (path.toFile().exists()) return deSerialize(fileInputStream(path.toFile()));
        return new ArrayList<>();
    }

    public List<WindowConfiguration> deSerialize(InputStream stream) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        copyFromTo(stream, outputStream);
        return new Gson().fromJson(new String(outputStream.toByteArray()), getType());
    }

    private Type getType() {
        return new TypeToken<Collection<WindowConfiguration>>() {
        }.getType();
    }
}
