/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.builder.view.configuration;

import java.io.File;

public class WindowConfigurationBuilder {
    private String outputName;
    private String outputDirectory = new File(".").toString();
    private String jarFile = "";
    private boolean win64 = true;
    private boolean lin64 = false;
    private boolean standalone = true;
    private boolean lightweight = false;
    private boolean debug = true;
    private boolean release = false;
    private String resources = "";
    private boolean autoBuild = false;

    public WindowConfigurationBuilder(String outputName) {
        this.outputName = outputName;
    }

    public WindowConfigurationBuilder outputDirectory(String outputDirectory) {
        this.outputDirectory = outputDirectory;
        return this;
    }

    public WindowConfigurationBuilder jarFile(String jarFile) {
        this.jarFile = jarFile;
        return this;
    }

    public WindowConfigurationBuilder win64(boolean value) {
        win64 = value;
        return this;
    }

    public WindowConfigurationBuilder lin64(boolean value) {
        lin64 = value;
        return this;
    }

    public WindowConfigurationBuilder standalone(boolean value) {
        standalone = value;
        return this;
    }

    public WindowConfigurationBuilder lightweight(boolean value) {
        lightweight = value;
        return this;
    }

    public WindowConfigurationBuilder debug(boolean value) {
        debug = value;
        return this;
    }

    public WindowConfigurationBuilder release(boolean value) {
        this.release = value;
        return this;
    }

    public WindowConfigurationBuilder resources(String resources) {
        this.resources = resources;
        return this;
    }

    public WindowConfigurationBuilder autoBuild(boolean value) {
        autoBuild = value;
        return this;
    }

    public WindowConfiguration build() {
        return new WindowConfiguration(outputName, outputDirectory, jarFile, win64, lin64, standalone, lightweight, debug, release, resources, autoBuild);
    }
}
