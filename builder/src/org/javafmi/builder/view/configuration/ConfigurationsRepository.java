/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.builder.view.configuration;

import org.javafmi.builder.Platform;

import java.nio.file.Path;
import java.util.List;

import static java.nio.file.Paths.get;

public class ConfigurationsRepository {
    private final WindowConfigurationDeSerializer reader;
    private final WindowConfigurationSerializer writer;
    private Platform platform;

    public ConfigurationsRepository(Platform platform, WindowConfigurationDeSerializer reader, WindowConfigurationSerializer writer) {
        this.platform = platform;
        this.reader = reader;
        this.writer = writer;
    }

    public List<WindowConfiguration> loadWindowConfigurations() {
        return reader.deSerialize(path(platform.localAppDataDirectory(), "FmuBuilder.conf"));
    }

    private Path path(String localAppDataDirectory, String configurationFile) {
        return get(localAppDataDirectory, configurationFile);
    }

    public void save(List<WindowConfiguration> configurations) {
        writer.serialize(configurations, path(platform.localAppDataDirectory(), "FmuBuilder.conf"));
    }
}
