/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.builder.view.configuration;

import com.google.gson.Gson;

public class WindowConfiguration {
    private String outputName;
    private String outputDirectory;
    private String jarFile;
    private boolean win64;
    private boolean lin64;
    private boolean standalone;
    private boolean lightweight;
    private boolean debug;
    private boolean release;
    private String resources;
    private boolean autoBuild;

    WindowConfiguration(String outputName, String outputDirectory, String jarFile, boolean win64, boolean lin64, boolean standalone, boolean lightweight, boolean debug, boolean release, String resources, boolean autoBuild) {
        this.outputName = outputName;
        this.outputDirectory = outputDirectory;
        this.jarFile = jarFile;
        this.win64 = win64;
        this.lin64 = lin64;
        this.standalone = standalone;
        this.lightweight = lightweight;
        this.debug = debug;
        this.release = release;
        this.resources = resources;
        this.autoBuild = autoBuild;
    }

    public String outputName() {
        return outputName;
    }

    public String outputDirectory() {
        return outputDirectory;
    }

    public String jarFile() {
        return jarFile;
    }

    public boolean win64() {
        return win64;
    }

    public boolean lin64() {
        return lin64;
    }

    public boolean standalone() {
        return standalone;
    }

    public boolean lightweight() {
        return lightweight;
    }

    public boolean debug() {
        return debug;
    }

    public boolean release() {
        return release;
    }

    public String resources() {
        return resources;
    }

    public boolean autoBuild() {
        return autoBuild;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
