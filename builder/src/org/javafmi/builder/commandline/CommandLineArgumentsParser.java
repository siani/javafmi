/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.builder.commandline;

import java.util.*;
import java.util.function.Function;

public class CommandLineArgumentsParser {
    private final Map<String, Function<String, String>> parsers;
    private String command;
    private List<String> mandatoryOperands;
    private List<String> helpMessages;

    public CommandLineArgumentsParser(String command) {
        this.command = command;
        this.helpMessages = new ArrayList<>();
        this.parsers = new HashMap<>();
    }

    public void operands(String[] operands, Function<String, String> operandsChecker) {
        mandatoryOperands = Arrays.asList(operands);
        parsers.put("", operandsChecker);
    }

    public void addOptionParser(String flag, String help, Function<String, String> parser) {
        helpMessages.add(flag + " " + help);
        parsers.put(flag.replaceAll("[\\[\\]]", ""), parser);
    }

    public Map<String, String> parse(String... args) {
        if (args.length < mandatoryOperands.size())
            throw new BadUsageException(errorMessage("wrong number of arguments"));
        Map<String, String> parsedArguments = new HashMap<>();
        List<String> arguments = new ArrayList<>(Arrays.asList(args));
        process("", parsedArguments, arguments);
        while (arguments.size() > 0)
            process(arguments.remove(0), parsedArguments, arguments);
        return parsedArguments;
    }

    private String errorMessage(String reason) {
        StringBuilder message = new StringBuilder();
        message.append("bad usage: ").append(reason).append("\n");
        message.append(command).append('\n');
        for (String helpMessage : helpMessages) {
            message.append(helpMessage).append('\n');
        }
        return message.toString().trim();
    }

    private void process(String option, Map<String, String> options, List<String> input) {
        if (parsers.get(option) == null) throw new BadUsageException(errorMessage("unknown option " + option));
        StringBuilder optionsWithoutFlags = new StringBuilder();
        while (input.size() > 0 && !input.get(0).startsWith("-"))
            optionsWithoutFlags.append(input.remove(0)).append(" ");
        if (optionsWithoutFlags.toString().isEmpty()) return;
        options.put(option, parsers.get(option).apply(optionsWithoutFlags.toString().trim()));
    }

    public class BadUsageException extends RuntimeException {
        public BadUsageException(String reason) {
            super(reason);
        }
    }
}
