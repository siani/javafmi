/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.builder.commandline;

import org.javafmi.builder.Platform;

import java.io.File;
import java.util.*;

public class BuilderOptions {
    public static final String FMU_NAME = "-n";
    public static final String INCLUDE = "-i";
    public static final String PLATFORM = "-p";
    public static final String WORKING_DIRECTORY = "-w";
    public static final String USER_JAR = "";
    private Map<String, String> options;

    public BuilderOptions(Map<String, String> options) {
        this.options = options;
    }

    public String userJar() {
        return options.get(USER_JAR);
    }

    public String fmuName() {
        String fmuName = options.get(BuilderOptions.FMU_NAME);
        if (fmuName != null) return fmuName;
        String userJar = userJar().replace('\\', '/');
        return userJar.substring(userJar.lastIndexOf('/') + 1, userJar.lastIndexOf(".jar")) + ".fmu";
    }

    public List<String> platforms() {
        String platforms = options.get(PLATFORM);
        if (platforms != null) return Arrays.asList(platforms.split(";"));
        return Collections.singletonList(Platform.Win64.toString());
    }

    public List<String> includes() {
        String includes = options.get(INCLUDE);
        if (includes != null) return Arrays.asList(includes.split(";"));
        return new ArrayList<>();
    }

    public String workingDirectory() {
        String workingDirectory = options.get(BuilderOptions.WORKING_DIRECTORY);
        if (workingDirectory != null) return workingDirectory;
        else return new File(".").getAbsolutePath();
    }
}

