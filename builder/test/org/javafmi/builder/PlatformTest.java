/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.builder;

import org.junit.Ignore;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.javafmi.kernel.OS.isLinux64;
import static org.javafmi.kernel.OS.isWin64;
import static org.junit.Assert.assertThat;
import static org.junit.Assume.assumeTrue;

public class PlatformTest {

    private String userName = System.getProperty("user.name");

    @Test
    public void creates_a_windows_64_platform() {
        assertThat(Platform.Win64.os(), is("win"));
        assertThat(Platform.Win64.arch(), is("64"));
    }

    @Test
    public void creates_a_linux_64_platform() {
        assertThat(Platform.Linux64.os(), is("linux"));
        assertThat(Platform.Linux64.arch(), is("64"));
    }

    @Test
    @Ignore
    public void provides_windows_app_data_local_directory_for_javafmi() {
        assumeTrue(isWin64());
        assertThat(Platform.Win64.localAppDataDirectory(), containsString("Users\\" + userName + "\\AppData\\Local\\javafmi"));
    }

    @Test
    public void provides_linux_app_data_directory_for_javafmi() {
        assumeTrue(isLinux64());
        assertThat(Platform.Linux64.localAppDataDirectory(), containsString("/home/" + userName + "/.javafmi"));
    }
}
