/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.builder;

import org.javafmi.kernel.Unzipper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.javafmi.kernel.Files.deleteAll;

public class ShellFmuBuilderTest {

    private File resources;

    @Before
    public void setUp() {
        resources = new File("extractedResources");
    }

    @After
    public void cleanUp() {
        deleteAll(resources);
    }

    @Test
    public void createsAnFmuReadyForWindows() {
        File fmu = new ShellFmuBuilder("test-res/Car.jar").build();
        File extractedResources = new Unzipper(fmu).unzipAllFilesInDirectory("resources", resources);
        List<String> files = Arrays.asList(extractedResources.listFiles()).stream()
                .map(File::getName)
                .collect(Collectors.toList());
        assertThat(files.size(), is(2));
        assertThat(files, containsInAnyOrder("Car.jar", "MANIFEST"));
    }
}
