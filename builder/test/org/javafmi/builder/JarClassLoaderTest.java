/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.builder;


import org.javafmi.kernel.JarClassLoader;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;

public class JarClassLoaderTest {
    public static final String JAR_PATH = "test-res/JarClassLoaderTest/bin/JarClassLoaderTest.jar";
    public static final String SIMPLE_CLASS = "SimpleClass";
    public static final String SUB_CLASS = "SubClass";
    public static final String VARIABLE_NAME = "Variable";
    private final static String MODEL_DESCRIPTION_CLASS = "MyModelDescription";
    private JarClassLoader loader;

    @Before
    public void setUp() {
        loader = new JarClassLoader(new File(JAR_PATH));
    }

    @Test
    public void gettingASimpleClassFromJar() throws IllegalAccessException, InstantiationException {
        Object object = loader.loadClass(SIMPLE_CLASS).newInstance();
        assertEquals("SimpleClassForTest", object.toString());
    }

    @Test
    public void gettingASubClass() throws IllegalAccessException, InstantiationException {
        Object object = loader.loadClass(SUB_CLASS).newInstance();
        assertEquals("Subclass of class SimpleClass", object.toString());
    }
}
