/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.builder;

import org.javafmi.modeldescription.ScalarVariable;
import org.javafmi.proxy.v2.State;
import org.javafmi.proxy.Status;
import org.javafmi.wrapper.Simulation;
import org.javafmi.wrapper.v2.Access;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.util.Arrays;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.core.Is.is;
import static org.javafmi.kernel.Files.deleteAll;
import static org.javafmi.kernel.OS.isLinux64;
import static org.javafmi.kernel.OS.isWin64;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeTrue;

public class TestCarFmu {

	private static final String CAR_FMU = "Car.fmu";

	@BeforeClass
	public static void buildFmu() {
		new ShellFmuBuilder("test-res/Car.jar")
				.platforms(Arrays.asList(Platform.Win64, Platform.Linux64))
				.build();
	}

//	@AfterClass
//	public static void cleanUp() {
//		deleteAll(new File(CAR_FMU));
//	}
//
//	@Before
//	public void setUp() {
//		deleteAll(new File("fmu_build_folder"));
//	}

	@Test
	public void create_car() {
		assumeTrue(isWin64() || isLinux64());
		// TODO: this can be NOW executed everywhere as POCO is bridged using JAR
		// TODO: force wrapper to take binaries instead of java for testing POCO
		// TODO: To modify its execution and use POCO, change version in proxyfactory
		Simulation simulation = new Simulation(CAR_FMU);
		assertThat(modelVariables(simulation).length, is(8));
		assertThat(modelVariables(simulation)[0].getName(), is("car.speed"));
		assertThat(modelVariables(simulation)[1].getName(), is("car.distance"));
		assertThat(modelVariables(simulation)[2].getName(), is("car.numberofdoors"));
		assertThat(modelVariables(simulation)[3].getName(), is("car.maxpower"));
		assertThat(modelVariables(simulation)[4].getName(), is("car.abs"));
		assertThat(modelVariables(simulation)[5].getName(), is("car.mp3"));
		assertThat(modelVariables(simulation)[6].getName(), is("car.fuel"));
		assertThat(modelVariables(simulation)[7].getName(), is("car.song"));

        /* Stepping */

		simulation.init(0.);
		simulation.write("car.speed").with(40.);
		simulation.doStep(1800.);
		assertThat(simulation.read("car.distance").asDouble(), is(closeTo(20000, 0.1)));
		simulation.doStep(1800.);
		assertThat(simulation.read("car.distance").asDouble(), is(closeTo(40000, 0.1)));

        /* States */
		Access access = new Access(simulation);
		State state = State.newEmptyState();
		access.getState(state);
		simulation.doStep(1800.);
		assertThat(simulation.read("car.distance").asDouble(), is(closeTo(60000, 0.1)));

		access.setState(state);
		assertThat(simulation.read("car.distance").asDouble(), is(closeTo(40000, 0.1)));

		access.freeState(state);
		assertThat(access.setState(state), is(Status.ERROR));

        /* Accessing variables */

        /* Doubles */
		assertArrayEquals(new double[]{40.0, 40000.0}, access.getReal(1, 2), 0.1);
		assertThat(access.setReal(new int[]{1, 2}, new double[]{80.0, -15.0}), is(Status.ERROR));
		assertArrayEquals(new double[]{80.0, 40000.0}, access.getReal(1, 2), 0.1);

	    /* Integer */
		assertArrayEquals(new int[]{5, 90}, access.getInteger(3, 4));
		assertThat(access.setInteger(new int[]{3, 4}, new int[]{3, 100}), is(Status.OK));
		assertArrayEquals(new int[]{3, 100}, access.getInteger(3, 4));

	    /* Boolean */
		assertTrue(Arrays.equals(new boolean[]{true, true}, access.getBoolean(5, 6)));
		assertThat(access.setBoolean(new int[]{5, 6}, new boolean[]{true, false}), is(Status.OK));
		assertTrue(Arrays.equals(new boolean[]{true, false}, access.getBoolean(5, 6)));

	    /* Strings -> multiple not implemented */
		assertThat(simulation.read("car.fuel").asString(), is("Diesel"));
		assertThat(simulation.write("car.fuel").with("Petrol"), is(Status.OK));
		assertThat(simulation.read("car.fuel").asString(), is("Petrol"));

		assertThat(simulation.read("car.song").asString(), is("init"));
		simulation.write("car.song").with("This\nis\na\ntest");
		assertThat(simulation.read("car.song").asString(), is("This\nis\na\ntest"));

        /* testing decimal values */
		simulation.doStep(2.21324);
		assertThat(simulation.read("car.distance").asDouble(), is(closeTo(40049.18311111111, 0.00001)));

		assertThat(simulation.terminate(), is(Status.OK));
	}

	private ScalarVariable[] modelVariables(Simulation simulation) {
		return simulation.getModelDescription().getModelVariables();
	}

	@Test
	public void testLoadingSeveralFmus() {
		assumeTrue(isWin64() || isLinux64());
		int numberOfCars = 3;
		Simulation simulations[] = new Simulation[numberOfCars];
		for (int i = 0; i < numberOfCars; i++) {
			simulations[i] = new Simulation("Car.fmu");
			simulations[i].init(0.);
		}
		for (int i = 0; i < numberOfCars; i++) simulations[i].terminate();
	}

}
