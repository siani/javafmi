/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.builder;

import org.javafmi.builder.commandline.*;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.Assert.assertThat;

public class CommandLineArgumentsParserTest {

    private CommandLineArgumentsParser parser;
    private final String emptyArgumentsError =
            "bad usage: wrong number of arguments\n" +
                    "java -jar simulation user.jar [-modelName fmu_name] [-i path/to/dependency.jar ...] [-p platform]\n" +
                    "[-p] supported platforms: win64 lin64\n" +
                    "[-i] include: any kind of resource to be included in the FMU\n" +
                    "[-n] fmu modelName";

    private final String unknownOptionError =
            "bad usage: unknown option -a\n" +
                    "java -jar simulation user.jar [-modelName fmu_name] [-i path/to/dependency.jar ...] [-p platform]\n" +
                    "[-p] supported platforms: win64 lin64\n" +
                    "[-i] include: any kind of resource to be included in the FMU\n" +
                    "[-n] fmu modelName";

    @Before
    public void setUp() {
        parser = new CommandLineArgumentsParser("java -jar simulation user.jar [-modelName fmu_name] [-i path/to/dependency.jar ...] [-p platform]");
        parser.operands(new String[]{"user.jar"}, new UserJarChecker());
        parser.addOptionParser("[-p]", "supported platforms: win64 lin64", new PlatformParser());
        parser.addOptionParser("[-i]", "include: any kind of resource to be included in the FMU", new IncludesParser());
        parser.addOptionParser("[-n]", "fmu modelName", new FmuNameParser());
    }

    @Test
    public void when_no_arguments_throws_bad_usage_exception() {
        try {
            parser.parse().size();
        } catch (CommandLineArgumentsParser.BadUsageException e) {
            assertThat(e.getMessage(), is(emptyArgumentsError));
        }
    }

    @Test
    public void parse_the_user_jar() {
        assertThat(new BuilderOptions(parser.parse("user.jar")).userJar(), is("user.jar"));
    }

    @Test
    public void default_fmu_name_is_user_jar_with_fmu_extension() {
        assertThat(new BuilderOptions(parser.parse("user.jar")).fmuName(), is("user.fmu"));
    }

    @Test
    public void when_path_to_jar_is_relative_fmu_name_is_only_the_jar_name() {
        String pathToJar = "../relative/path/to/user.jar";
        assertThat(new BuilderOptions(parser.parse(pathToJar)).userJar(), is(pathToJar));
        assertThat(new BuilderOptions(parser.parse(pathToJar)).fmuName(), is("user.fmu"));
    }

    @Test
    public void a_different_fmu_can_be_specified() {
        assertThat(new BuilderOptions(parser.parse("user.jar -n different_name".split(" "))).fmuName(), is("different_name.fmu"));
    }

    @Test
    public void unknown_command_throws_an_exception_with_usage_as_description() {
        try {
            parser.parse("-a", "invalid argument");
        } catch (CommandLineArgumentsParser.BadUsageException e) {
            assertThat(e.getMessage(), is(unknownOptionError));
        }
    }

    @Test
    public void fmu_name_always_ends_with_fmu() {
        assertThat(new BuilderOptions(parser.parse("user.jar -n given_name.fmu".split(" "))).fmuName(), is("given_name.fmu"));
        assertThat(new BuilderOptions(parser.parse("user.jar -n given_name".split(" "))).fmuName(), is("given_name.fmu"));
    }

    @Test
    public void dependencies_could_be_a_list_of_jar_files() {
        String[] args = "user.jar -i foo.jar bar.jar relative/path.jar".split(" ");
        assertThat(new BuilderOptions(parser.parse(args)).includes(),
                containsInAnyOrder("foo.jar", "bar.jar", "relative/path.jar"));
    }

    @Test
    public void dependencies_could_be_a_single_jar() {
        assertThat(new BuilderOptions(parser.parse("user.jar -i single.jar".split(" "))).includes(),
                containsInAnyOrder("single.jar"));
    }

    @Test
    public void dependencies_could_be_before_name() {
        String[] args = "user.jar -i a.jar another.jar -n modelName".split(" ");
        assertThat(new BuilderOptions(parser.parse(args)).fmuName(), is("modelName.fmu"));
        assertThat(new BuilderOptions(parser.parse(args)).includes(), containsInAnyOrder("a.jar", "another.jar"));
    }

    @Test
    public void default_platform_is_win_64() {
        assertThat(new BuilderOptions(parser.parse("user.jar -n foo".split(" "))).platforms().get(0), is(Platform.Win64.toString()));
    }

    @Test
    public void platform_is_specified() {
        assertThat(new BuilderOptions(parser.parse("user.jar -n foo -p win64".split(" "))).platforms().get(0), is(Platform.Win64.toString()));
        assertThat(new BuilderOptions(parser.parse("user.jar -n foo -p lin64".split(" "))).platforms().get(0), is(Platform.Linux64.toString()));
        assertThat(new BuilderOptions(parser.parse("user.jar -n foo -p lin64 win64".split(" "))).platforms(),
                containsInAnyOrder(Platform.Linux64.toString(), Platform.Win64.toString()));
        assertThat(new BuilderOptions(parser.parse("user.jar -n foo -p win64 lin64".split(" "))).platforms(),
                containsInAnyOrder(Platform.Win64.toString(), Platform.Linux64.toString()));
    }
}
