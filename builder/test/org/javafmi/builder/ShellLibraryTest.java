/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.builder;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class ShellLibraryTest {

	@Test
	public void createsAWin64Library() {
		Library windows64Library = new ShellLibrary(Platform.Win64);
		assertThat(windows64Library.os(), is("win"));
		assertThat(windows64Library.arch(), is("64"));
		assertThat(windows64Library.extension(), is(".dll"));
		assertThat(windows64Library.undecoratedName(), is("ShellLibrary"));
		assertThat(windows64Library.name(), is("libwin64ShellLibrary.dll"));
	}

	@Test
	public void createsAWin32Library() {
		Library windows64Library = new ShellLibrary(Platform.Win32);
		assertThat(windows64Library.os(), is("win"));
		assertThat(windows64Library.arch(), is("32"));
		assertThat(windows64Library.extension(), is(".dll"));
		assertThat(windows64Library.undecoratedName(), is("ShellLibrary"));
		assertThat(windows64Library.name(), is("libwin32ShellLibrary.dll"));
	}

	@Test
	public void createsALin64Library() {
		Library windows64Library = new ShellLibrary(Platform.Linux64);
		assertThat(windows64Library.os(), is("linux"));
		assertThat(windows64Library.arch(), is("64"));
		assertThat(windows64Library.extension(), is(".so"));
		assertThat(windows64Library.undecoratedName(), is("ShellLibrary"));
		assertThat(windows64Library.name(), is("liblinux64ShellLibrary.so"));
	}

	@Test
	public void createsALin32Library() {
		Library windows64Library = new ShellLibrary(Platform.Linux32);
		assertThat(windows64Library.os(), is("linux"));
		assertThat(windows64Library.arch(), is("32"));
		assertThat(windows64Library.extension(), is(".so"));
		assertThat(windows64Library.undecoratedName(), is("ShellLibrary"));
		assertThat(windows64Library.name(), is("liblinux32ShellLibrary.so"));
	}
}
