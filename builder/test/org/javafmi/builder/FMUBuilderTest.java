/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.builder;

import org.javafmi.kernel.Unzipper;
import org.junit.After;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import static org.javafmi.kernel.Files.deleteAll;
import static org.javafmi.kernel.Files.fileInputStream;
import static org.junit.Assert.assertEquals;

public class FMUBuilderTest {
    public static final String TEST_RESOURCES_FMU_BUILDER_DIRECTORY = "test-res/";
    public static final String EMPTY_WIN32_LIBRARY = "empty_win32.dll";
    private static final String EMPTY_WIN64_LIBRARY = "empty_win64.dll";
    public static final String EMPTY_LIN32_LIBRARY = "empty_linux32.so";
    private static final String EMPTY_LIN64_LIBRARY = "empty_linux64.so";
    public static final String EMPTY_MAC_LIBRARY = "empty_darwin64.dylib";
    private static final String MODEL_DESCRIPTION = "empty_modelDescription.xml";
    private String modelName;

    @After
    public void cleanUp() {
        deleteAll(new File(modelName));
        deleteAll(new File(modelName + ".fmu"));
    }

    @Test
    public void buildingAnEmptyFMU() {
        modelName = "emptyModel";
        File emptyFmu = new FmuBuilder(modelName, new File(".").getAbsolutePath()).buildFmu();
        Unzipper unzipper = new Unzipper(emptyFmu);
        assertEquals(modelName + ".fmu", emptyFmu.getName());
        assertEquals(0, unzipper.unzipAll(new File(modelName)).listFiles().length);
    }


    @Test
    public void buildFmuWithLibrariesForAllOperatingSystems() {
        modelName = "modelWithAllLibraries";
        Map<String, InputStream> librariesStreams = configureLibrariesStreams(new HashMap<>());
        File fmu = new FmuBuilder(modelName, new File(".").getAbsolutePath())
                .addLibrary(new TestLibrary(Platform.Win64, librariesStreams))
                .addLibrary(new TestLibrary(Platform.Win32, librariesStreams))
                .addLibrary(new TestLibrary(Platform.Linux64, librariesStreams))
                .addLibrary(new TestLibrary(Platform.Linux32, librariesStreams))
                .buildFmu();
        Unzipper unzipper = new Unzipper(fmu);
        assertEquals(4, unzipper.unzipAllFilesInDirectory("binaries", new File(modelName)).listFiles().length);
    }

    @Test
    public void buildsAnFmuWithAnXMLasModelDescription() {
        modelName = "withModelDescription";
        FileInputStream modelDescription = fileInputStream(new File(TEST_RESOURCES_FMU_BUILDER_DIRECTORY + MODEL_DESCRIPTION).getAbsoluteFile());
        File fmu = new FmuBuilder(modelName, new File(".").getAbsolutePath()).addModelDescription(modelDescription).buildFmu();
        File[] filesInsideFmu = new Unzipper(fmu).unzipAll(new File(modelName)).listFiles();
        assertEquals(1, filesInsideFmu.length);
        assertEquals("modelDescription.xml", filesInsideFmu[0].getName());
    }

    @Test
    public void buildFmuWithResources() {
        modelName = "fmuWithResources";
        File fmu = new FmuBuilder(modelName, new File(".").getAbsolutePath())
                .addResource(fileInputStream(new File(TEST_RESOURCES_FMU_BUILDER_DIRECTORY + "subDir/" + "subResource.data").getAbsoluteFile()), "subDir/subResource.data")
                .addResource(fileInputStream(new File(TEST_RESOURCES_FMU_BUILDER_DIRECTORY + "resource.data").getAbsoluteFile()), "resource.data")
                .buildFmu();
        Unzipper unzipper = new Unzipper(fmu);
        File[] files = unzipper.unzipAllFilesInDirectory("resources", new File(modelName)).listFiles();
        assertEquals(2, files.length);
    }

    private Map<String, InputStream> configureLibrariesStreams(Map<String, InputStream> map) {
        map.put(EMPTY_WIN32_LIBRARY, fileInputStream(new File(TEST_RESOURCES_FMU_BUILDER_DIRECTORY + EMPTY_WIN32_LIBRARY).getAbsoluteFile()));
        map.put(EMPTY_WIN64_LIBRARY, fileInputStream(new File(TEST_RESOURCES_FMU_BUILDER_DIRECTORY + EMPTY_WIN64_LIBRARY).getAbsoluteFile()));
        map.put(EMPTY_LIN32_LIBRARY, fileInputStream(new File(TEST_RESOURCES_FMU_BUILDER_DIRECTORY + EMPTY_LIN32_LIBRARY).getAbsoluteFile()));
        map.put(EMPTY_LIN64_LIBRARY, fileInputStream(new File(TEST_RESOURCES_FMU_BUILDER_DIRECTORY + EMPTY_LIN64_LIBRARY).getAbsoluteFile()));
        map.put(EMPTY_MAC_LIBRARY, fileInputStream(new File(TEST_RESOURCES_FMU_BUILDER_DIRECTORY + EMPTY_MAC_LIBRARY).getAbsoluteFile()));
        return map;
    }

    private static class TestLibrary extends Library {
        private final Map<String, InputStream> librariesStreams;

        public TestLibrary(Platform platform, Map<String, InputStream> librariesStreams) {
            super(platform, "");
            this.librariesStreams = librariesStreams;
        }

        @Override
        public InputStream getInputStream() {
            return librariesStreams.get("empty_" + os() + arch() + extension());
        }

        @Override
        public String name() {
            return "";
        }
    }
}