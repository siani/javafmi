/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.builder.view;

import org.javafmi.builder.Platform;
import org.javafmi.builder.view.configuration.ConfigurationsRepository;
import org.javafmi.builder.view.configuration.WindowConfiguration;
import org.javafmi.builder.view.configuration.WindowConfigurationDeSerializer;
import org.javafmi.builder.view.configuration.WindowConfigurationSerializer;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class ConfigurationRepositoryTest {

    private WindowConfigurationSerializer writer;
    private Platform platform;
    private WindowConfigurationDeSerializer reader;
    private ConfigurationsRepository repository;

    @Before
    public void setUp() throws Exception {
        writer = mock(WindowConfigurationSerializer.class);
        platform = mock(Platform.class);
        reader = mock(WindowConfigurationDeSerializer.class);
        repository = new ConfigurationsRepository(platform, reader, writer);
    }

    @Test
    public void repositoryLoadsConfigurationsUsingADeserializer() {

        repository.loadWindowConfigurations();

        verify(platform).localAppDataDirectory();
        verify(reader).deSerialize(Matchers.<java.nio.file.Path>anyObject());
    }

    @Test
    public void repositorySavesConfigurationUsingASerializer() {
        List<WindowConfiguration> configurations = new ArrayList<>();

        repository.save(configurations);

        verify(platform).localAppDataDirectory();
        verify(writer).serialize(anyListOf(WindowConfiguration.class), Matchers.<java.nio.file.Path>anyObject());
    }
}
