/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.builder.view;

import org.javafmi.builder.view.configuration.WindowConfiguration;
import org.javafmi.builder.view.configuration.WindowConfigurationBuilder;
import org.javafmi.builder.view.configuration.WindowConfigurationDeSerializer;
import org.javafmi.builder.view.configuration.WindowConfigurationSerializer;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class WindowConfigurationSerializerAndDeserializerTest {

    @Test
    public void serializeAndDeserializeBuildConfiguration() throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        List<WindowConfiguration> configurations = new ArrayList<>();
        configurations.add(aConfiguration("aName"));
        configurations.add(aConfiguration("anotherName"));

        new WindowConfigurationSerializer().serialize(configurations, outputStream);
        List<WindowConfiguration> deserializedConfigurations = new WindowConfigurationDeSerializer().deSerialize(new ByteArrayInputStream(outputStream.toByteArray()));

        assertThat(configurations.size(), is(deserializedConfigurations.size()));
        assertThat(configurations.get(0).toString(), is(deserializedConfigurations.get(0).toString()));
        assertThat(configurations.get(1).toString(), is(deserializedConfigurations.get(1).toString()));
    }

    private WindowConfiguration aConfiguration(String outputName) {
        String outputDirectory = "any/directory";
        String jarFile = "any.jar";
        String resources = "any\nstring";

        return new WindowConfigurationBuilder(outputName)
                .outputDirectory(outputDirectory)
                .jarFile(jarFile)
                .win64(true)
                .lin64(true)
                .standalone(true)
                .lightweight(false)
                .debug(true)
                .release(false)
                .resources(resources)
                .autoBuild(false)
                .build();
    }
}
