/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.kernel;

public class OS {

    public static final String Mac = "Mac";
    public static final String Windows = "Windows";
    public static final String Linux = "Linux";
    public static final String Java = "Java 8";

    public static String name() {
        return normalizeName(System.getProperty("os.name"));
    }

    public static boolean isWin32() {
        return (name().equals(Windows) && (architecture().contains("86") || architecture().contains("32")));
    }

    public static boolean isWin64() {
        return (name().equals(Windows) && architecture().contains("64"));
    }

    public static boolean isLinux32() {
        return (name().equals(Linux) && architecture().contains("86"));
    }

    public static boolean isLinux64() {
        return (name().contains(Linux) && architecture().contains("64"));
    }

    public static boolean isMac() {
        return (name().equals(Mac));
    }

    public static String architecture() {
        return normalizeArchitecture(System.getProperty("os.arch"));
    }

    private static String normalizeArchitecture(String arch) {
        return arch.contains("64") ? "64" : "32";
    }

    private static String normalizeName(String osName) {
        if (osName.contains(Mac)) return Mac;
        if (osName.contains(Windows)) return Windows;
        if (osName.contains(Linux)) return Linux;
        return "Unknown System";
    }
}
