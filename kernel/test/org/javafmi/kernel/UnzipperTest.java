/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.kernel;

import org.hamcrest.CoreMatchers;
import org.junit.AfterClass;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.javafmi.kernel.Files.deleteAll;

public class UnzipperTest {
	public static final String WIN_32_BOUNCING_BALL_FMU = "win_32_bouncingBall.fmu";

	private static String outPath = "tmp";

	@AfterClass
	public static void clean() {
		deleteAll(outPath);
	}

	@Test
	public void testUnzipModelDescription() {
		File fmuFile = getFmuFile(WIN_32_BOUNCING_BALL_FMU);
		Unzipper unzipper = new Unzipper(fmuFile);
		String modelDescription = "modelDescription.xml";
		File modelDescriptionFile = unzipper.unzip("modelDescription.xml", new File(outPath));
		assertThat(calculateChecksum(modelDescriptionFile), CoreMatchers.is("08441861ad7298c191209b764f15f2d2"));
		assertThat(modelDescriptionFile.getPath().replace("\\", "/"), containsString(outPath + "/" + modelDescription));
	}

	private File getFmuFile(String fmuFile) {
		return new File("../wrapper/test-res/fmu/cosimulation/", fmuFile);
	}

	private String calculateChecksum(File file) {
		FileInputStream inputStream = null;
		try {
			MessageDigest messageDigest = MessageDigest.getInstance("MD5");
			inputStream = new FileInputStream(file);
			byte[] dataBytes = new byte[1024];
			int bytesRead;
			while (true) {
				if ((bytesRead = inputStream.read(dataBytes)) < 0)
					break;
				messageDigest.update(dataBytes, 0, bytesRead);
			}

			byte[] digestBytes = messageDigest.digest();
			StringBuilder stringBuffer = new StringBuilder("");
			for (byte digestByte : digestBytes)
				stringBuffer.append(Integer.toString((digestByte & 0xff) + 0x100, 16).substring(1));
			inputStream.close();
			return stringBuffer.toString();


		} catch (IOException | NoSuchAlgorithmException ex) {
			Logger.getLogger(UnzipperTest.class.getName()).log(Level.SEVERE, null, ex);
		} finally {
			try {
				inputStream.close();
			} catch (IOException ex) {
				Logger.getLogger(UnzipperTest.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		return null;
	}
}
