/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.skeleton;

import org.javafmi.framework.FmiSimulation;
import org.javafmi.framework.Logger;
import org.javafmi.skeleton.actions.Terminate;

import java.io.*;
import java.util.Arrays;
import java.util.Scanner;

import static org.javafmi.skeleton.ActionFactory.get;

public class SimulationSkeleton {
    private static final boolean AutoFlush = true;

    public static void main(String[] args) throws FileNotFoundException {
        try {
            new Proxy(instantiate(className())).listen();
        } catch (Throwable e) {
            show(e);
        }
    }

    public static FmiSimulation instantiate(String className) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Class<?> aClass = Class.forName(className);
        FmiSimulation simulation = (FmiSimulation) aClass.newInstance();
        simulation.logger(new Logger(className, System.out));
        return simulation;
    }

    private static String className() throws FileNotFoundException {
        return new Scanner(new File("MANIFEST")).useDelimiter("\\Z").next();
    }

    private static void show(Throwable e) throws FileNotFoundException {
        PrintWriter printWriter = new PrintWriter(new FileOutputStream(className() + "_error.log"));
        e.printStackTrace(printWriter);
        printWriter.close();
    }


    static class Proxy {
        private final FmiSimulation simulation;
        private boolean continueListening;
        private BufferedReader reader;
        private PrintWriter writer;


        Proxy(FmiSimulation simulation) {
            this.simulation = simulation;
            this.reader = new BufferedReader(new InputStreamReader(System.in));
            this.writer = new PrintWriter(System.err, AutoFlush);
        }

        void listen() {
            continueListening = true;
            String message;
            while (continueListening) {
                if ((message = receiveMessage()) == null) break;
                processRequest(message);
            }

        }

        String receiveMessage() {
            try {
                return reader.readLine();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        void processRequest(String command) {
            Request request = new Request(command);
            Action action = get(request.action());
            writer.println(action.executeOn(simulation, request.arguments()));
            if (action instanceof Terminate) close();
        }

        void close() {
            continueListening = false;
        }

        private class Request {
            private final String[] arguments;
            private final String commandName;

            Request(String request) {
                String[] dividedRequest = request.split(" ");
                this.commandName = dividedRequest[0];
                this.arguments = Arrays.copyOfRange(dividedRequest, 1, dividedRequest.length);
            }

            String[] arguments() {
                return arguments;
            }

            String action() {
                return commandName;
            }
        }
    }
}
