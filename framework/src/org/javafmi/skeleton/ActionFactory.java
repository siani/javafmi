/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.skeleton;

import org.javafmi.skeleton.actions.*;

import java.util.HashMap;

public class ActionFactory {

	private static final HashMap<String, Action> commands;

	private static final String EnterInitializationMode = "enterInitializationMode";
	private static final String ExitInitializationMode = "exitInitializationMode";
	private static final String SetupExperiment = "setupExperiment";
	private static final String Terminate = "terminate";
	private static final String Reset = "reset";

	private static final String GetReal = "getReal";
	private static final String GetInteger = "getInteger";
	private static final String GetBoolean = "getBoolean";
	private static final String GetString = "getString";
	private static final String SetReal = "setReal";
	private static final String SetInteger = "setInteger";
	private static final String SetBoolean = "setBoolean";
	private static final String SetString = "setString";

	private static final String DoStep = "doStep";
	private static final String CancelStep = "cancelStep";

	private static final String GetState = "getState";
	private static final String SetState = "setState";
	private static final String FreeState = "freeState";

	private static final String SetRealInputDerivatives = "setRealInputDerivatives";
	private static final String GetRealOutputDerivatives = "getRealOutputDerivatives";

	static {
		commands = new HashMap<>();
		commands.put(EnterInitializationMode, new EnterInitializationMode());
		commands.put(SetupExperiment, new SetupExperiment());
		commands.put(ExitInitializationMode, new ExitInitializationMode());
		commands.put(Terminate, new Terminate());
		commands.put(Reset, new Reset());

		commands.put(GetReal, new GetReal());
		commands.put(GetInteger, new GetInteger());
		commands.put(GetBoolean, new GetBoolean());
		commands.put(GetString, new GetString());
		commands.put(SetReal, new SetReal());
		commands.put(SetInteger, new SetInteger());
		commands.put(SetBoolean, new SetBoolean());
		commands.put(SetString, new SetString());

		commands.put(DoStep, new DoStep());
		commands.put(CancelStep, new CancelStep());

		commands.put(GetState, new GetState());
		commands.put(SetState, new SetState());
		commands.put(FreeState, new FreeState());

		commands.put(SetRealInputDerivatives, new SetRealInputDerivatives());
		commands.put(GetRealOutputDerivatives, new GetRealOutputDerivatives());
	}

	static Action get(String command) {
		if (commands.get(command) == null) throw new RuntimeException("Command not found: " + command);
		return commands.get(command);
	}

}
