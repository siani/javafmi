/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.skeleton.actions;

import org.javafmi.framework.FmiSimulation;
import org.javafmi.skeleton.Action;

import static java.lang.Integer.parseInt;
import static java.util.stream.IntStream.range;

public class GetRealOutputDerivatives implements Action {
	@Override
	public String executeOn(FmiSimulation simulation, String... args) {
		int size = parseInt(args[0]);
		int[] valueReferences = toPrimitiveArray(range(1, size + 1).boxed().map(i -> parseInt(args[i])).toArray(Integer[]::new));
		int[] orders = toPrimitiveArray(range(size + 1, 2 * size + 1).boxed().map(i -> parseInt(args[i])).toArray(Integer[]::new));
		return buildResponse(simulation.getRealOutputDerivatives(valueReferences, orders));
	}

	public int[] toPrimitiveArray(Integer[] array) {
		int[] result = new int[array.length];
		for (int i = 0; i < array.length; i++) result[i] = array[i];
		return result;
	}

	public String buildResponse(double[] realValues) {
		StringBuilder responseBuilder = new StringBuilder(FmiSimulation.Status.OK.toString() + " ");
		for (double real : realValues) responseBuilder.append(real).append(" ");
		return responseBuilder.toString().trim();
	}
}
