/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.skeleton.actions;

import org.javafmi.framework.FmiSimulation;
import org.javafmi.skeleton.Action;

public class GetInteger implements Action {

    @Override
    public String executeOn(FmiSimulation simulation, String... args) {
        try {
            return buildResponse(simulation.getInteger(extractValueReferencesFromArgs(args)));
        } catch (Exception e) {
            return FmiSimulation.Status.ERROR.toString();
        }
    }

    public int[] extractValueReferencesFromArgs(String[] args) {
        int[] valueReferences = new int[args.length];
        for (int i = 0; i < args.length; i++) {
            valueReferences[i] = Integer.valueOf(args[i]);
        }
        return valueReferences;
    }

    public String buildResponse(int[] intValues) {
        StringBuilder responseBuilder = new StringBuilder(FmiSimulation.Status.OK.toString() + " ");
        for (int integer : intValues) {
            responseBuilder.append(integer).append(" ");
        }
        return responseBuilder.toString().trim();
    }
}
