/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.framework;

import java.util.List;

public interface ModelDescription {
    String fmiVersion();

    String modelName();

    String author();

    String guid();

    String description();

    String version();

    String copyright();

    String license();

    String generationTool();

    String generationDateAndTime();

    String variableNamingConvention();

    Integer numberOfEventIndicators();

    CoSimulation coSimulation();

    DefaultExperiment defaultExperiment();

    List<ScalarVariable> variables();

    ModelStructure modelStructure();

    enum VariableNamingConvention {structured, flat;}

    interface DefaultExperiment {
        Double startTime();

        Double stopTime();

        Double tolerance();

        Double stepSize();
    }


    interface CoSimulation {
        String modelIdentifier();

        Boolean needsExecutionTool();

        Boolean canHandleVariableCommunicationStepSize();

        Boolean canInterpolateInputs();

        Integer maxOutputDerivativeOrder();

        Boolean canRunAsynchronously();

        Boolean canBeInstantiatedOnlyOncePerProcess();

        Boolean canNotUseMemoryManagementFunctions();

        Boolean canGetAndSetFMUstate();

        Boolean canSerializeFMUstate();

        Boolean providesDirectionalDerivative();
    }

    interface ScalarVariable {

        String name();

        int valueReference();

        String description();

        String causality();

        String variability();

        String initial();

        Integer previous();

        boolean canHandleMultipleSetPerTimeInstant();
    }

    interface IntegerVariable extends ScalarVariable {

        String quantity();

        Integer min();

        Integer max();

        Integer start();
    }

    interface RealVariable extends ScalarVariable {

        String quantity();

        String unit();

        String displayUnit();

        Boolean relativeQuantity();

        Double min();

        Double max();

        Double nominal();

        Boolean unbounded();

        Double start();

        Integer derivative();

        Boolean reinit();
    }

    interface StringVariable extends ScalarVariable {

        String start();
    }

    interface BooleanVariable extends ScalarVariable {

        Boolean start();
    }

    interface ModelStructure {

        Outputs outputs();

        Derivatives derivatives();

        InitialUnknowns initialUnknowns();
    }

    interface Outputs {

        List<Unknown> unknowns();
    }

    interface Derivatives {
        List<Unknown> unknowns();
    }

    interface InitialUnknowns {
        List<Unknown> unknowns();
    }

    interface Unknown {
        String index();

        String dependencies();

        String dependenciesKind();
    }

}
