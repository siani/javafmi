/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.skeleton;

import org.javafmi.skeleton.actions.*;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ActionFactoryTest {

    @Test
    public void identifiesCreationAndDestructionCommands() {
        assertEquals(EnterInitializationMode.class, ActionFactory.get("enterInitializationMode").getClass());
        assertEquals(ExitInitializationMode.class, ActionFactory.get("exitInitializationMode").getClass());
        assertEquals(SetupExperiment.class, ActionFactory.get("setupExperiment").getClass());
        assertEquals(Terminate.class, ActionFactory.get("terminate").getClass());
        assertEquals(Reset.class, ActionFactory.get("reset").getClass());
    }

    @Test
    public void identifiesGettingAndSettingCommands() {
        assertEquals(GetReal.class, ActionFactory.get("getReal").getClass());
        assertEquals(GetInteger.class, ActionFactory.get("getInteger").getClass());
        assertEquals(GetBoolean.class, ActionFactory.get("getBoolean").getClass());
        assertEquals(GetString.class, ActionFactory.get("getString").getClass());
        assertEquals(SetReal.class, ActionFactory.get("setReal").getClass());
        assertEquals(SetInteger.class, ActionFactory.get("setInteger").getClass());
        assertEquals(SetBoolean.class, ActionFactory.get("setBoolean").getClass());
        assertEquals(SetString.class, ActionFactory.get("setString").getClass());
    }

    @Test
    public void identifiesComputationCommands() {
        assertEquals(DoStep.class, ActionFactory.get("doStep").getClass());
        assertEquals(CancelStep.class, ActionFactory.get("cancelStep").getClass());
    }
}
