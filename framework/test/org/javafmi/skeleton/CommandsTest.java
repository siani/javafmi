/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.skeleton;

import org.javafmi.framework.FmiSimulation;
import org.javafmi.skeleton.actions.*;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class CommandsTest {


    private FmiSimulation simulation;

    @Before
    public void setUpMock() {
        simulation = mock(FmiSimulation.class);
    }

    @Test
    public void enterInitializationCommandShouldCallEnterInitializationModeInProxy() {
        when(simulation.enterInitializationMode()).thenReturn(FmiSimulation.Status.OK);
        Action enterInitializationMode = new EnterInitializationMode();
        enterInitializationMode.executeOn(simulation);
        verify(simulation).enterInitializationMode();
    }

    @Test
    public void exitInitializationCommandShouldCallExitInitializationModeInProxy() {
        when(simulation.exitInitializationMode()).thenReturn(FmiSimulation.Status.OK);
        Action exitInitializationMode = new ExitInitializationMode();
        exitInitializationMode.executeOn(simulation);
        verify(simulation).exitInitializationMode();
    }

    @Test
    public void terminateCommandShouldCallTerminateInProxy() {
        when(simulation.terminate()).thenReturn(FmiSimulation.Status.OK);
        Action terminate = new Terminate();
        terminate.executeOn(simulation);
        verify(simulation).terminate();
    }

    @Test
    public void resetCommandShouldCallResetInProxy() {
        when(simulation.reset()).thenReturn(FmiSimulation.Status.OK);
        Action reset = new Reset();
        reset.executeOn(simulation);
        verify(simulation).reset();
    }

    @Test
    public void getRealCallsGetRealInProxy() {
        String valueReference = "1331";
        Action getReal = new GetReal();
        getReal.executeOn(simulation, valueReference);
        verify(simulation).getReal(any());
    }

    @Test
    public void getRealWithSeveralValueReferences() {
        when(simulation.getReal(new int[]{1, 2, 3})).thenReturn(new double[]{3.14, 4.15, 5.16});
        Action getReal = new GetReal();
        String result = getReal.executeOn(simulation, "1", "2", "3");
        assertEquals(4, result.split(" ").length);
        assertEquals("ok", result.split(" ")[0]);
        assertEquals("3.14", result.split(" ")[1]);
        assertEquals("4.15", result.split(" ")[2]);
        assertEquals("5.16", result.split(" ")[3]);
        verify(simulation).getReal(new int[]{1, 2, 3});
    }

    @Test
    public void getIntegerCallsGetIntegerInProxy() {
        Action getInteger = new GetInteger();
        String valueReference = "1331";
        getInteger.executeOn(simulation, valueReference);
        verify(simulation).getInteger(1331);

    }

    @Test
    public void getBooleanCallsGetBooleanInProxy() {
        Action getBoolean = new GetBoolean();
        String valueReference = "1331";
        getBoolean.executeOn(simulation, valueReference);
        verify(simulation).getBoolean(1331);
    }

    @Test
    public void getStringCallsGetStringInProxy() {
        when(simulation.getString(1331)).thenReturn(new String[]{"foo"});
        Action getString = new GetString();
        String valueReference = "1331";
        getString.executeOn(simulation, valueReference);
        verify(simulation).getString(1331);

    }

    @Test
    public void setRealCallsSetRealInProxy() {
        when(simulation.setReal(new int[]{1331}, new double[]{3.1})).thenReturn(FmiSimulation.Status.OK);
        Action setReal = new SetReal();
        String valueReference = "1331";
        String value = "3.1";
        setReal.executeOn(simulation, valueReference, value);
        verify(simulation).setReal(new int[]{1331}, new double[]{3.1});
    }

    @Test
    public void setIntegerCallsSetIntegerInProxy() {
        when(simulation.setInteger(new int[]{1331}, new int[]{31})).thenReturn(FmiSimulation.Status.OK);
        Action setInteger = new SetInteger();
        String valueReference = "1331";
        String value = "31";
        setInteger.executeOn(simulation, valueReference, value);
        verify(simulation).setInteger(new int[]{1331}, new int[]{31});
    }

    @Test
    public void setBooleanCallsSetBooleanInProxy() {
        when(simulation.setBoolean(new int[]{1331}, new boolean[]{false})).thenReturn(FmiSimulation.Status.OK);
        Action setBoolean = new SetBoolean();
        String valueReference = "1331";
        String value = "false";
        setBoolean.executeOn(simulation, valueReference, value);
        verify(simulation).setBoolean(new int[]{1331}, new boolean[]{false});
    }

    @Test
    public void setStringCallsSetStringInProxy() {
        when(simulation.setString(new int[]{1331}, new String[]{"value"})).thenReturn(FmiSimulation.Status.OK);
        Action setString = new SetString();
        String valueReference = "1331";
        String value = "value";
        setString.executeOn(simulation, valueReference, value);
        verify(simulation).setString(new int[]{1331}, new String[]{"value"});
    }

    @Test
    public void doStepCallsDoStepInProxy() {
        when(simulation.doStep(0.1, 0.002, true)).thenReturn(FmiSimulation.Status.OK);
        Action doStep = new DoStep();
        String communicationPoint = "0.1";
        String stepSize = "0.002";
        doStep.executeOn(simulation, communicationPoint, stepSize);
        verify(simulation).doStep(0.1, 0.002, true);
    }

    @Test
    public void cancelStepCallsCancelStepInProxy() {
        when(simulation.cancelStep()).thenReturn(FmiSimulation.Status.OK);
        Action cancelStep = new CancelStep();
        cancelStep.executeOn(simulation);
        verify(simulation).cancelStep();
    }
}
