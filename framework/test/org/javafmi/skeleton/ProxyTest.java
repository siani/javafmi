/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.skeleton;

import org.javafmi.framework.FmiSimulation;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

public class ProxyTest {

    private FmiSimulation simulation;
    private SimulationSkeleton.Proxy proxy;

    @Before
    public void setUp() {
        simulation = mock(FmiSimulation.class);
        proxy = new SimulationSkeleton.Proxy(simulation);
    }

    @Test
    public void skeletonExecutesTheProxyMethodsOfCreationAndDestruction() {
        when(simulation.enterInitializationMode()).thenReturn(FmiSimulation.Status.OK);
        when(simulation.exitInitializationMode()).thenReturn(FmiSimulation.Status.OK);
        when(simulation.terminate()).thenReturn(FmiSimulation.Status.OK);
        proxy.processRequest("enterInitializationMode");
        proxy.processRequest("exitInitializationMode");
        proxy.processRequest("terminate");
        verify(simulation).enterInitializationMode();
        verify(simulation).exitInitializationMode();
        verify(simulation).terminate();
    }

    @Test
    public void skeletonExecutesTheFmuProxyMethodsOfGettingAndSetting() {
        when(simulation.setReal(new int[]{1331}, new double[]{3.141592})).thenReturn(FmiSimulation.Status.OK);
        when(simulation.setInteger(new int[]{1331}, new int[]{3141592})).thenReturn(FmiSimulation.Status.OK);
        when(simulation.setBoolean(new int[]{1331}, new boolean[]{false})).thenReturn(FmiSimulation.Status.OK);
        when(simulation.setString(new int[]{1331}, new String[]{"value"})).thenReturn(FmiSimulation.Status.OK);
        proxy.processRequest("getReal 1331");
        proxy.processRequest("getInteger 1331");
        proxy.processRequest("getBoolean 1331");
        proxy.processRequest("getString 1331");
        proxy.processRequest("setReal 1331 3.141592");
        proxy.processRequest("setInteger 1331 3141592");
        proxy.processRequest("setBoolean 1331 false");
        proxy.processRequest("setString 1331 value");
        verify(simulation).getReal(1331);
        verify(simulation).getInteger(1331);
        verify(simulation).getBoolean(1331);
        verify(simulation).getString(1331);
        verify(simulation).setReal(new int[]{1331}, new double[]{3.141592});
        verify(simulation).setInteger(new int[]{1331}, new int[]{3141592});
        verify(simulation).setBoolean(new int[]{1331}, new boolean[]{false});
        verify(simulation).setString(new int[]{1331}, new String[]{"value"});
    }

    @Test
    public void skeletonExecutesTheFmuProxyMethodsOfComputation() {
        when(simulation.doStep(0.001, 0.002, true)).thenReturn(FmiSimulation.Status.OK);
        when(simulation.cancelStep()).thenReturn(FmiSimulation.Status.OK);
        proxy.processRequest("doStep 0.001 0.002");
        proxy.processRequest("cancelStep");
        verify(simulation).doStep(0.001, 0.002, true);
        verify(simulation).cancelStep();
    }

    @Test
    public void skeletonExecutesTheFmuProxyForDerivatives() {
        when(simulation.setRealInputDerivatives(new int[]{1,2}, new int[2], new double[]{0.2, 0.5})).thenReturn(FmiSimulation.Status.OK);
        when(simulation.getRealOutputDerivatives(new int[]{1,2}, new int[2])).thenReturn(new double[]{1.0, 1.0});
        proxy.processRequest("setRealInputDerivatives 2 1 2 0 0 0.2 0.5");
        proxy.processRequest("getRealOutputDerivatives 2 1 2 0 0");
    }
}
