/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.framework;

import org.junit.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.Matchers.closeTo;
import static org.javafmi.framework.FmiContainer.Causality.input;
import static org.javafmi.framework.FmiContainer.Initial.calculated;
import static org.javafmi.framework.FmiContainer.Variability.tunable;
import static org.javafmi.framework.ModelDescription.VariableNamingConvention.*;
import static org.junit.Assert.*;

public class FmiContainer_ {


    public static FmiContainer setup() {
        return new FmiContainer();
    }

    @Test
    public void should_create_model_description_with_empty_attributes() {
        ModelDescription modelDescription = setup().model("model").terminate();

        assertThat(modelDescription.modelName(), is("model"));
        assertThat(modelDescription.fmiVersion(), is("2.0"));
        assertThat(modelDescription.guid(), is(notNullValue()));
        assertThat(modelDescription.author(), is(nullValue()));
        assertThat(modelDescription.version(), is(nullValue()));
        assertThat(modelDescription.copyright(), is(nullValue()));
        assertThat(modelDescription.license(), is(nullValue()));
		assertThat(modelDescription.generationDateAndTime(), is(notNullValue()));
		assertThat(modelDescription.variableNamingConvention(), is(nullValue()));
        assertThat(modelDescription.numberOfEventIndicators(), is(nullValue()));
    }

    @Test
    public void should_create_model_description_with_custom_attributes() {
        ModelDescription modelDescription = setup().model("model")
                .author("author")
                .guid("guid12345")
                .modelVersion("model version 3.1")
                .description("description")
                .variableNamingConvention(flat)
                .numberOfEventIndicators(101)
                .copyright("(c) 2016")
                .license("license")
                .variableNamingConvention(structured)
                .numberOfEventIndicators(4)
                .canHandleVariableCommunicationStepSize(true)
                .canInterpolateInputs(true)
                .maxOutputDerivativeOrder(10)
                .canRunAsynchronuously(true)
                .canBeInstantiatedOnlyOncePerProcess(true)
                .canNotUseMemoryManagementFunctions(true)
                .canGetAndSetFMUstate(true)
                .canSerializeFMUstate(false)
                .providesDirectionalDerivative(true)
                .terminate();

        assertThat(modelDescription.fmiVersion(), is("2.0"));
        assertThat(modelDescription.modelName(), is("model"));
        assertThat(modelDescription.author(), is("author"));
        assertThat(modelDescription.guid(), is("guid12345"));
        assertThat(modelDescription.version(), is("model version 3.1"));
        assertThat(modelDescription.copyright(), is("(c) 2016"));
        assertThat(modelDescription.license(), is("license"));
        assertThat(modelDescription.variableNamingConvention(), is(structured.name()));
        assertThat(modelDescription.numberOfEventIndicators(), is(4));
        assertThat(modelDescription.description(), is("description"));
        assertThat(modelDescription.generationDateAndTime(), is(notNullValue()));

    }

    @Test
    public void should_create_model_description_with_empty_cosimulation() {
        ModelDescription.CoSimulation coSimulation = setup().model("model").terminate().coSimulation();

        assertThat(coSimulation.modelIdentifier(), is("model"));
        assertThat(coSimulation.needsExecutionTool(), is(false));
        assertThat(coSimulation.canHandleVariableCommunicationStepSize(), is(true));
        assertThat(coSimulation.canInterpolateInputs(), is(nullValue()));
        assertThat(coSimulation.maxOutputDerivativeOrder(), is(nullValue()));
        assertThat(coSimulation.canRunAsynchronously(), is(nullValue()));
        assertThat(coSimulation.canBeInstantiatedOnlyOncePerProcess(), is(nullValue()));
        assertThat(coSimulation.canNotUseMemoryManagementFunctions(), is(nullValue()));
        assertThat(coSimulation.canGetAndSetFMUstate(), is(nullValue()));
        assertThat(coSimulation.canSerializeFMUstate(), is(nullValue()));
        assertThat(coSimulation.providesDirectionalDerivative(), is(nullValue()));
    }

    @Test
    public void should_create_model_description_with_empty_default_experiment() {
        ModelDescription.DefaultExperiment experiment = setup().model("model").terminate().defaultExperiment();
        assertThat(experiment.startTime(), is(nullValue()));
        assertThat(experiment.stopTime(), is(nullValue()));
        assertThat(experiment.tolerance(), is(nullValue()));
        assertThat(experiment.stepSize(), is(nullValue()));
    }

    @Test
    public void should_create_model_description_with_custom_default_experiment() {
        ModelDescription.DefaultExperiment experiment = setup().model("model")
                .startTime(0.)
                .stopTime(2.)
                .tolerance(0.0001)
                .stepSize(0.1)
                .terminate().defaultExperiment();

        assertThat(experiment.startTime(), is(closeTo(0.,0.01)));
        assertThat(experiment.stopTime(), is(closeTo(2.,0.01)));
        assertThat(experiment.tolerance(), is(closeTo(0.0001,0.01)));
        assertThat(experiment.stepSize(), is(closeTo(0.1,0.01)));
    }

    @Test
    public void should_create_model_description_without_variables() {
        List<ModelDescription.ScalarVariable> variables = setup().model("model").terminate().variables();
        assertThat(variables.size(), is(0));
    }

    @Test
    public void should_create_model_description_with_empty_structure() {
        ModelDescription.ModelStructure modelStructure = setup().model("model").terminate().modelStructure();
        assertThat(modelStructure.outputs().unknowns().size(), is(0));
        assertThat(modelStructure.derivatives().unknowns().size(), is(0));
        assertThat(modelStructure.initialUnknowns().unknowns().size(), is(0));
    }

    @Test
    public void should_create_real_variable_with_empty_attributes() {
        ModelDescription.RealVariable variable = setup().variable("real-variable").asReal().terminate();

        assertThat(variable.name(), is("real-variable"));
        assertThat(variable.valueReference(), is(0));
        assertThat(variable.description(), is(nullValue()));
        assertThat(variable.causality(), is(nullValue()));
        assertThat(variable.variability(), is(nullValue()));
        assertThat(variable.initial(), is(nullValue()));
        assertThat(variable.previous(), is(nullValue()));
        assertThat(variable.canHandleMultipleSetPerTimeInstant(), is(true));
        assertThat(variable.quantity(), is(nullValue()));
        assertThat(variable.unit(), is(nullValue()));
        assertThat(variable.displayUnit(), is(nullValue()));
        assertThat(variable.relativeQuantity(), is(nullValue()));
        assertThat(variable.min(), is(nullValue()));
        assertThat(variable.max(), is(nullValue()));
        assertThat(variable.nominal(), is(nullValue()));
        assertThat(variable.unbounded(), is(nullValue()));
        assertThat(variable.start(), is(nullValue()));
        assertThat(variable.derivative(), is(nullValue()));
        assertThat(variable.reinit(), is(nullValue()));
    }


    @Test
    public void should_create_real_variable_with_custom_attributes() {
        ModelDescription.RealVariable variable = createRealVariable().terminate();

        assertThat(variable.name(), is("real-variable"));
        assertThat(variable.valueReference(), is(0));
        assertThat(variable.description(), is("this is a test variable"));
        assertThat(variable.causality(), is("input"));
        assertThat(variable.variability(), is("tunable"));
        assertThat(variable.initial(), is("calculated"));
        assertThat(variable.previous(), is(nullValue()));
        assertThat(variable.canHandleMultipleSetPerTimeInstant(), is(false));
        assertThat(variable.quantity(), is("quantity"));
        assertThat(variable.unit(), is("unit"));
        assertThat(variable.displayUnit(), is("displayUnit"));
        assertThat(variable.relativeQuantity(), is(false));
        assertThat(variable.derivative(), is(5));
        assertThat(variable.min(), is(closeTo(0.,0.01)));
        assertThat(variable.max(), is(closeTo(1.,0.01)));
        assertThat(variable.nominal(), is(closeTo(.5,0.01)));
        assertThat(variable.unbounded(), is(false));
        assertThat(variable.start(), is(closeTo(.5,0.01)));
        assertThat(variable.derivative(), is(5));
        assertThat(variable.reinit(), is(false));
    }


    @Test
    public void should_create_integer_variable_with_empty_attributes() {
        ModelDescription.IntegerVariable variable = setup().variable("integer-variable").asInteger().terminate();

        assertThat(variable.name(), is("integer-variable"));
        assertThat(variable.valueReference(), is(0));
        assertThat(variable.description(), is(nullValue()));
        assertThat(variable.causality(), is(nullValue()));
        assertThat(variable.variability(), is(nullValue()));
        assertThat(variable.initial(), is(nullValue()));
        assertThat(variable.previous(), is(nullValue()));
        assertThat(variable.canHandleMultipleSetPerTimeInstant(), is(true));
        assertThat(variable.quantity(), is(nullValue()));
        assertThat(variable.min(), is(nullValue()));
        assertThat(variable.max(), is(nullValue()));
        assertThat(variable.start(), is(nullValue()));
    }

    @Test
    public void should_create_integer_variable_with_custom_attributes() {
        ModelDescription.IntegerVariable variable = createIntegerVariable().terminate();

        assertThat(variable.name(), is("integer-variable"));
        assertThat(variable.description(), is("this is a test variable"));
        assertThat(variable.causality(), is("input"));
        assertThat(variable.variability(), is("tunable"));
        assertThat(variable.initial(), is("calculated"));
        assertThat(variable.previous(), is(nullValue()));
        assertThat(variable.canHandleMultipleSetPerTimeInstant(), is(false));
        assertThat(variable.quantity(), is("quantity"));
        assertThat(variable.min(), is(0));
        assertThat(variable.max(), is(10));
        assertThat(variable.start(), is(5));
    }

    @Test
    public void should_create_boolean_variable_with_empty_attributes() {
        ModelDescription.BooleanVariable variable = setup().variable("boolean-variable").asBoolean().terminate();

        assertThat(variable.name(), is("boolean-variable"));
        assertThat(variable.valueReference(), is(0));
        assertThat(variable.description(), is(nullValue()));
        assertThat(variable.causality(), is(nullValue()));
        assertThat(variable.variability(), is(nullValue()));
        assertThat(variable.initial(), is(nullValue()));
        assertThat(variable.previous(), is(nullValue()));
        assertThat(variable.canHandleMultipleSetPerTimeInstant(), is(true));
        assertThat(variable.start(), is(nullValue()));
    }

    @Test
    public void should_create_boolean_variable_with_custom_attributes() {
        ModelDescription.BooleanVariable variable = createBooleanVariable().terminate();

        assertThat(variable.name(), is("boolean-variable"));
        assertThat(variable.description(), is("this is a test variable"));
        assertThat(variable.causality(), is("input"));
        assertThat(variable.variability(), is("tunable"));
        assertThat(variable.initial(), is("calculated"));
        assertThat(variable.previous(), is(nullValue()));
        assertThat(variable.canHandleMultipleSetPerTimeInstant(), is(false));
        assertThat(variable.start(), is(true));
    }

    @Test
    public void should_create_string_variable_with_empty_attributes() {
        ModelDescription.StringVariable variable = setup().variable("string-variable").asString().terminate();
        assertThat(variable.name(), is("string-variable"));
        assertThat(variable.valueReference(), is(0));
        assertThat(variable.description(), is(nullValue()));
        assertThat(variable.causality(), is(nullValue()));
        assertThat(variable.variability(), is(nullValue()));
        assertThat(variable.initial(), is(nullValue()));
        assertThat(variable.previous(), is(nullValue()));
        assertThat(variable.canHandleMultipleSetPerTimeInstant(), is(true));
        assertThat(variable.start(), is(nullValue()));
    }

    @Test
    public void should_create_string_variable_with_custom_attributes() {
        ModelDescription.StringVariable variable = createStringVariable().terminate();

        assertThat(variable.name(), is("string-variable"));
        assertThat(variable.description(), is("this is a test variable"));
        assertThat(variable.causality(), is("input"));
        assertThat(variable.variability(), is("tunable"));
        assertThat(variable.initial(), is("calculated"));
        assertThat(variable.previous(), is(nullValue()));
        assertThat(variable.canHandleMultipleSetPerTimeInstant(), is(false));
        assertThat(variable.start(), is("start value"));
    }

    @Test
    public void should_create_a_full_model_description() {
        ModelDescription modelDescription = setup().model("modelName")
                .guid("guid12345")
                .description("description")
                .author("author")
                .modelVersion("model version 3.1")
                .copyright("copyright")
                .license("license")
                .variableNamingConvention(structured)
                .numberOfEventIndicators(101)

                .startTime(0.)
                .stopTime(1.)
                .tolerance(0.0001)
                .stepSize(0.02)

                .canHandleVariableCommunicationStepSize(true)
                .canInterpolateInputs(true)
                .maxOutputDerivativeOrder(3)
                .canRunAsynchronuously(true)
                .canBeInstantiatedOnlyOncePerProcess(false)
                .canNotUseMemoryManagementFunctions(false)
                .canGetAndSetFMUstate(true)
                .canSerializeFMUstate(true)
                .providesDirectionalDerivative(true)
                .add(createRealVariable())
                .add(createIntegerVariable())
                .add(createBooleanVariable())
                .add(createStringVariable())
                .terminate();

        assertThat(modelDescription.modelName(), is("modelName"));
        assertThat(modelDescription.author(), is("author"));
        assertThat(modelDescription.guid(), is("guid12345"));
        assertThat(modelDescription.description(), is("description"));
        assertThat(modelDescription.author(), is("author"));
        assertThat(modelDescription.fmiVersion(), is("2.0"));
        assertThat(modelDescription.copyright(), is("copyright"));
        assertThat(modelDescription.license(), is("license"));
        assertThat(modelDescription.variableNamingConvention(), is("structured"));
        assertThat(modelDescription.numberOfEventIndicators(), is(101));

        assertThat(modelDescription.coSimulation().modelIdentifier(), is("modelName"));
        assertThat(modelDescription.coSimulation().needsExecutionTool(), is(false));
        assertThat(modelDescription.coSimulation().canHandleVariableCommunicationStepSize(), is(true));
        assertThat(modelDescription.coSimulation().canInterpolateInputs(), is(true));
        assertThat(modelDescription.coSimulation().maxOutputDerivativeOrder(), is(3));
        assertThat(modelDescription.coSimulation().canRunAsynchronously(), is(true));
        assertThat(modelDescription.coSimulation().canBeInstantiatedOnlyOncePerProcess(), is(false));
        assertThat(modelDescription.coSimulation().canNotUseMemoryManagementFunctions(), is(false));
        assertThat(modelDescription.coSimulation().canGetAndSetFMUstate(), is(true));
        assertThat(modelDescription.coSimulation().canSerializeFMUstate(), is(true));
        assertThat(modelDescription.coSimulation().providesDirectionalDerivative(), is(true));

        assertThat(modelDescription.defaultExperiment().startTime(), is(closeTo(0.,0.01)));
        assertThat(modelDescription.defaultExperiment().stopTime(), is(closeTo(1.,0.01)));
        assertThat(modelDescription.defaultExperiment().tolerance(), is(closeTo(0.0001,0.01)));
        assertThat(modelDescription.defaultExperiment().stepSize(), is(closeTo(0.02,0.01)));

        List<ModelDescription.ScalarVariable> variables = modelDescription.variables();
        assertThat(variables.size(), is(4));
        assertThat(variables.get(0).valueReference(), is(1));
        assertThat(variables.get(1).valueReference(), is(2));
        assertThat(variables.get(2).valueReference(), is(3));
        assertThat(variables.get(3).valueReference(), is(4));
    }

    private FmiContainer.RealVariable createRealVariable() {
        return setup().variable("real-variable").asReal()
                .description("this is a test variable")
                .causality(input)
                .variability(tunable)
                .initial(calculated)
                .canHandleMultipleSetPerTime(false)
                .quantity("quantity")
                .unit("unit")
                .displayUnit("displayUnit")
                .relativeQuantity(false)
                .derivative(5)
                .min(0.)
                .max(1.)
                .nominal(.5)
                .start(.5)
                .isUnbounded(false)
                .reinit(false);
    }

    private FmiContainer.IntegerVariable createIntegerVariable() {
        return setup().variable("integer-variable").asInteger()
                .description("this is a test variable")
                .causality(input)
                .variability(tunable)
                .initial(calculated)
                .canHandleMultipleSetPerTime(false)
                .quantity("quantity")
                .min(0)
                .max(10)
                .start(5);
    }

    private FmiContainer.BooleanVariable createBooleanVariable() {
        return setup().variable("boolean-variable").asBoolean()
                .description("this is a test variable")
                .causality(input)
                .variability(tunable)
                .initial(calculated)
                .canHandleMultipleSetPerTime(false)
                .start(true);
    }


    private FmiContainer.StringVariable createStringVariable() {
        return setup().variable("string-variable").asString()
                .description("this is a test variable")
                .causality(input)
                .variability(tunable)
                .initial(calculated)
                .canHandleMultipleSetPerTime(false)
                .start("start value");
    }


}
