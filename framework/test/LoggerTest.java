/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

import org.javafmi.framework.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.PrintStream;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class LoggerTest {


    @Mock
    private PrintStream outStream;
    private String anyName;
    private String anyMessage;
    private Logger logger;

    @Before
    public void setUp() throws Exception {
        anyName = "anyName";
        anyMessage = "any message";
        logger = new LoggerBuilder().outStream(outStream).instanceName(anyName).build();
    }

    @Test
    public void printInfoMessages() {
        logger.info(anyMessage);
        verify(outStream).println(anyName + ":" + "INFO" + ":" + anyMessage);
    }

    @Test
    public void printWarningMessages(){
        logger.warning(anyMessage);
        verify(outStream).println(anyName + ":" + "WARNING" + ":" + anyMessage);
    }

    @Test
    public void printErrorMessages(){
        logger.error(anyMessage);
        verify(outStream).println(anyName + ":" + "ERROR" + ":" + anyMessage);
    }

    private class LoggerBuilder {
        private PrintStream outStream;
        private String instanceName;

        public LoggerBuilder outStream(PrintStream outStream) {
            this.outStream = outStream;
            return this;
        }

        public LoggerBuilder instanceName(String instanceName) {
            this.instanceName = instanceName;
            return this;
        }

        public Logger build() {
            return new Logger(instanceName, outStream);
        }
    }
}
