/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.wrapper.variables;


import org.javafmi.modeldescription.ModelDescription;
import org.javafmi.modeldescription.ScalarVariable;
import org.javafmi.proxy.FmiProxy;
import org.javafmi.wrapper.Variable;

public class VariableReader {

    private final ModelDescription modelDescription;
    private FmiProxy fmiProxy;

    public VariableReader(FmiProxy fmiProxy, ModelDescription modelDescription) {
        this.fmiProxy = fmiProxy;
        this.modelDescription = modelDescription;
    }

    public Variable read(String variableName) {
        ScalarVariable modelVariable = modelDescription.getModelVariable(variableName);
        if (modelVariable.isEnumeration())
            return buildEnumerationVariable(modelVariable);
        else
            return buildVariable(modelVariable);
    }

    public Variable[] read(String[] variableNames) {
        double[] realValues = fmiProxy.getReal(modelDescription.getValueReferences(variableNames));
        Variable[] valuesAsVariables = new Variable[realValues.length];
        for (int i = 0; i < valuesAsVariables.length; i++) {
            valuesAsVariables[i] = new Variable<>(variableNames[i], realValues[i]);
        }
        return valuesAsVariables;
    }

    private Variable buildEnumerationVariable(ScalarVariable modelVariable) {
        return new Variable(modelVariable.getName(), fmiProxy.getEnumeration(modelVariable));
    }

    private Variable buildVariable(ScalarVariable modelVariable) {
        String name = modelVariable.getName();
        int valueReference = modelVariable.getValueReference();
        switch (modelVariable.getTypeName()) {
            case "Real":
                return new Variable<>(name, fmiProxy.getReal(valueReference)[0]);
            case "Integer":
                return new Variable<>(name, fmiProxy.getInteger(valueReference)[0]);
            case "Boolean":
                return new Variable<>(name, fmiProxy.getBoolean(valueReference)[0]);
            case "String":
                return new Variable<>(name, fmiProxy.getString(valueReference)[0]);
            default:
                break;
        }
        throw new RuntimeException("Unknown variable type: " + modelVariable.getTypeName());
    }
}
