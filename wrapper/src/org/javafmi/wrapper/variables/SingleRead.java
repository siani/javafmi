/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.wrapper.variables;

import org.javafmi.proxy.FmiProxy;

public class SingleRead {
	private final FmiProxy proxy;
	private final int valueReference;

	public SingleRead(FmiProxy proxy, int valueReference) {
		this.proxy = proxy;
		this.valueReference = valueReference;
	}

	public double asDouble() {
		return proxy.getReal(valueReference)[0];
	}

	public int asInteger() {
		return proxy.getInteger(valueReference)[0];
	}

	public boolean asBoolean() {
		return proxy.getBoolean(valueReference)[0];
	}

	public String asString() {
		return proxy.getString(valueReference)[0];
	}

	public Object asEnumeration() {
		return proxy.getEnumeration(valueReference)[0];
	}
}
