/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.proxy;

import org.javafmi.modeldescription.FmiVersion;

import java.io.*;
import java.util.LinkedHashMap;
import java.util.Map;

public class ModelDescriptionPeeker {

    private static final String Version = "fmiVersion";
    private static final String V1 = "1.0";
    private static final String V2 = "2.0";
    private static final Map<String, String> versions;

    static {
        versions = new LinkedHashMap<>();
        versions.put(V1, FmiVersion.One);
        versions.put(V2, FmiVersion.Two);
    }

    public static String peekVersion(File modelDescriptionFile) {
        return versions.get(readVersionFromFile(modelDescriptionFile));
    }

    private static String readVersionFromFile(File modelDescriptionFile) {
        BufferedReader reader = getBufferedReader(modelDescriptionFile);
        String line;
        while (!(line = readLine(reader)).contains(Version)) ;
        closeReader(reader);
        return isV1(line) ? V1 : V2;
    }

    private static boolean isV1(String line) {
        int beginIndex = line.indexOf(Version);
        line = line.substring(beginIndex);
        return line.length() >= 18 ? line.substring(0, 18).contains(V1) : line.contains(V1);
    }

    private static void closeReader(BufferedReader reader) {
        try {
            reader.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static String readLine(BufferedReader reader) {
        try {
            return reader.readLine();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static BufferedReader getBufferedReader(File modelDescriptionFile) {
        try {
            return new BufferedReader(new FileReader(modelDescriptionFile));
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Can not read the version from file " + modelDescriptionFile.getPath(), e);
        }
    }
}
