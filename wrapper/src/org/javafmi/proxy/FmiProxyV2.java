/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.proxy;

import org.javafmi.modeldescription.v2.Capabilities;
import org.javafmi.modeldescription.v2.ModelDescription;
import org.javafmi.proxy.v2.State;

public interface FmiProxyV2 extends FmiProxy {

    Status setDebugLogging(boolean loggingOn, String[] categories);

    Status setUpExperiment(boolean toleranceDefined, double tolerance, double startTime, boolean stopTimeDefined, double stopTime);

    Status enterInitializationMode();

    Status exitInitializationMode();

    ModelDescription getModelDescription();

    Status setRealInputDerivatives(int[] valueReferences, int[] orders, double[] values);

    double[] getRealOutputDerivatives(int[] valueReferences, int[] orders);

    State getState(State state);

    Status setState(State state);

    Status freeState(State state);

    int getStateSize(State state);

    byte[] serializeState(State state);

    State deserializeState(byte[] serializedState);

    void checkCapability(String method, Capabilities.Capability... capabilitiesToCheck);

    double[] getDirectionalDerivative(int[] unknownVariablesValueReferences, int[] knownVariablesValueReferences, double[] knownDifferential);
}
