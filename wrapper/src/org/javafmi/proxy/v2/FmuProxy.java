/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.proxy.v2;

import org.javafmi.modeldescription.FmiVersion;
import org.javafmi.modeldescription.ScalarVariable;
import org.javafmi.modeldescription.v2.Capabilities;
import org.javafmi.modeldescription.v2.EnumerationType;
import org.javafmi.modeldescription.v2.ModelDescription;
import org.javafmi.proxy.*;

import static java.lang.System.currentTimeMillis;
import static java.util.Arrays.stream;
import static org.javafmi.modeldescription.v2.Capabilities.Capability;
import static org.javafmi.proxy.Status.*;

public class FmuProxy implements FmiProxyV2 {

    public static boolean LOGGING_ON = false;
    public static boolean VISIBLE = false;
    private final Fmu library;
    private final ModelDescription modelDescription;
    private final CallbackFunctions callbacks;
    private FmuProxyState fmuProxyState;

    public FmuProxy(Fmu library, ModelDescription modelDescription) {
        if (!hasCoSimulationSupport(modelDescription))
            throw new RuntimeException("This Fmu does not support co-simulation execution");
        this.library = library;
        this.modelDescription = modelDescription;
        this.callbacks = createCallbacks();
    }

    private boolean hasCoSimulationSupport(ModelDescription modelDescription) {
        return modelDescription.getCoSimulation() != null;
    }

    private CallbackFunctions createCallbacks() {
        return checkCanRunAsynchronously(new CallbackFunctions());
    }

    private CallbackFunctions checkCanRunAsynchronously(CallbackFunctions callbackFunctions) {
        if (!modelDescription.check(Capabilities.CanRunAsynchronuously))
            callbackFunctions.stepFinished = null;
        return callbackFunctions;
    }

    @Override
    public String getVersion() {
        return FmiVersion.Two;
    }

    @Override
    public void instantiate(String resourcesPath) {
        this.library.instantiate(
                uniqueInstanceName(),
                Callbacks.FmiType.fmiCoSimulation,
                modelDescription.getGuid(),
                "file:///" + resourcesPath.replace("\\","/"),
                callbacks,
                VISIBLE,
                LOGGING_ON);
        this.fmuProxyState = FmuProxyState.INSTANTIATED;
    }

    private String uniqueInstanceName() {
        return currentTimeMillis() + "_" + modelDescription.getModelName();
    }

    @Override
    public void freeInstance() {
        library.free();
    }

    @Override
    public Status setDebugLogging(boolean loggingOn, String[] categories) {
        return Status.translateStatus(library.setDebugLogging(loggingOn, categories));
    }

    @Override
    public Status initialize(double startTime, double stopTime) {
        if (fmuProxyState.equals(FmuProxyState.INITED)) return WARNING;
		if (ERROR == setUpExperiment(true, modelDescription.getDefaultExperiment().getTolerance(), startTime, !Double.isNaN(stopTime), Double.isNaN(stopTime) ? Double.MAX_VALUE : stopTime))
            throw new MethodExecutionException("fmi2SetupExperiment: " + modelDescription.getModelIdentifier());
        if (ERROR == enterInitializationMode())
            throw new MethodExecutionException("fmi2SetupExperiment: " + modelDescription.getModelIdentifier());
        if (ERROR == exitInitializationMode())
            throw new MethodExecutionException("fmi2SetupExperiment: " + modelDescription.getModelIdentifier());
        return OK;
    }

    @Override
    public Status setUpExperiment(boolean toleranceDefined, double tolerance, double startTime, boolean stopTimeDefined, double stopTime) {
        return Status.translateStatus(library.setUpExperiment(toleranceDefined, tolerance, startTime, stopTimeDefined, stopTime));
    }

    @Override
    public Status enterInitializationMode() {
        Status enterInitStatus = Status.translateStatus(library.enterInitializationMode());
        if (enterInitStatus.equals(OK)) fmuProxyState = FmuProxyState.INITTING;
        return enterInitStatus;
    }

    @Override
    public Status exitInitializationMode() {
        Status exitInitStatus = Status.translateStatus(library.exitInitializationMode());
        if (exitInitStatus == OK) fmuProxyState = FmuProxyState.INITED;
        return exitInitStatus;
    }

    @Override
    public Status terminate() {
        Status terminateStatus = Status.translateStatus(library.terminate());
        if (terminateStatus.equals(OK)) fmuProxyState = FmuProxyState.TERMINATED;
        else fmuProxyState = FmuProxyState.INITED;
        return terminateStatus;
    }

    @Override
    public Status reset() {
        Status resetStatus = Status.translateStatus(library.reset());
        if (resetStatus.equals(Status.OK)) fmuProxyState = FmuProxyState.INSTANTIATED;
        return resetStatus;
    }

    @Override
    public ModelDescription getModelDescription() {
        return modelDescription;
    }

    @Override
    public Status setRealInputDerivatives(int[] valueReferences, int[] orders, double[] values) {
        return Status.translateStatus(library.setRealInputDerivatives(valueReferences, orders, values));
    }

    @Override
    public double[] getRealOutputDerivatives(int[] valueReferences, int[] orders) {
        return library.getRealOutputDerivatives(valueReferences, orders);
    }

    @Override
    public double[] getReal(int... valueReference) {
        return library.getReal(valueReference);
    }

    @Override
    public int[] getInteger(int... valueReference) {
        return library.getInteger(valueReference);
    }

    @Override
    public boolean[] getBoolean(int... valueReference) {
        return library.getBoolean(valueReference);
    }

    @Override
    public String[] getString(int... valueReference) {
        return library.getString(valueReference);
    }

	@Override
	public Object[] getEnumeration(int... valueReference) {
		return stream(valueReference).boxed().map(v -> getEnumeration(modelDescription.getModelVariable(v))).toArray(Object[]::new);
	}

	@Override
    public Object getEnumeration(ScalarVariable modelVariable) {
        EnumerationType enumeration = (EnumerationType) modelVariable.getType();
        Integer enumerationIndex;
        EnumerationType enumerationType = (EnumerationType) modelDescription.getTypeFromTypeDefinition(enumeration.getDeclaredType());
        if (modelVariable.getVariability().equalsIgnoreCase("constant")) {
            enumerationIndex = enumeration.getStart();
            return enumerationType.getItems().get(enumerationIndex - 1).getName();
        }
        enumerationIndex = getInteger(modelVariable.getValueReference())[0];
        return enumerationType.getItems().get(enumerationIndex - 1).getName();
    }

    @Override
    public Status setReal(int[] valueReferences, double[] doubleValues) {
        return Status.translateStatus(library.setReal(valueReferences, doubleValues));
    }

    @Override
    public Status setInteger(int[] valueReference, int[] intValue) {
        return Status.translateStatus(library.setInteger(valueReference, intValue));
    }

    @Override
    public Status setBoolean(int[] valueReference, boolean[] booleanValue) {
        return Status.translateStatus(library.setBoolean(valueReference, booleanValue));
    }

    @Override
    public Status setString(int[] valueReference, String[] stringValue) {
        return Status.translateStatus(library.setString(valueReference, stringValue));
    }

    @Override
    public State getState(State state) {
        checkCapability("getState", Capabilities.CanGetAndSetFMUState);
        return state.SetPointer(library.getFmuState(state.GetPointer()));
    }

    @Override
    public Status setState(State state) {
        checkCapability("setState", Capabilities.CanGetAndSetFMUState);
        return Status.translateStatus(library.setFmuState(state.GetPointer()));
    }

    @Override
    public Status freeState(State state) {
        checkCapability("freeState", Capabilities.CanGetAndSetFMUState);
        return Status.translateStatus(library.freeFmuState(state.GetPointer()));
    }

    @Override
    public int getStateSize(State state) {
        checkCapability("getStateSize", Capabilities.CanGetAndSetFMUState, Capabilities.CanSerializeFMUState);
        return library.serializedFmuStateSize(state.GetPointer());
    }

    @Override
    public byte[] serializeState(State state) {
        checkCapability("serializeState", Capabilities.CanGetAndSetFMUState, Capabilities.CanSerializeFMUState);
        return library.serializeFmuState(state.GetPointer());
    }

    @Override
    public State deserializeState(byte[] serializedState) {
        checkCapability("deserializeState", Capabilities.CanGetAndSetFMUState, Capabilities.CanSerializeFMUState);
        return State.newStateFrom(library.deSerializeFMUState(serializedState));
    }

    @Override
    public void checkCapability(String method, Capability... capabilitiesToCheck) {
        for (Capability capability : capabilitiesToCheck)
            if (!modelDescription.check(capability))
                throw new UnsupportedOperationException(method + " " + modelDescription.getModelIdentifier());
    }

    @Override
    public double[] getDirectionalDerivative(int[] unknownVariablesValueReferences, int[] knownVariablesValueReferences, double[] knownDifferential) {
        if (!modelDescription.check(Capabilities.ProvidesDirectionalDerivative))
            throw new UnsupportedOperationException("getDirectionalDerivative: " + modelDescription.getModelIdentifier());
        return library.getDirectionalDerivative(unknownVariablesValueReferences, knownVariablesValueReferences, knownDifferential);
    }

    @Override
    public Status doStep(double communicationPoint, double stepSize) {
        return Status.translateStatus(library.doStep(communicationPoint, stepSize, false));
    }

    @Override
    public Status cancelStep() {
        return Status.translateStatus(library.cancelStep());
    }

    @Override
    public boolean isTerminated() {
        return fmuProxyState.equals(FmuProxyState.TERMINATED);
    }
}
