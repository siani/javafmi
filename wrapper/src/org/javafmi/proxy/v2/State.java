/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.proxy.v2;

import com.sun.jna.Pointer;

public class State {

    private Pointer state;

    private State(Pointer state) {
        this.state = state;
    }

    public static State newEmptyState() {
        return new State(Pointer.NULL);
    }

    public static State newStateFrom(Pointer state) {
        return new State(state);
    }

    public Pointer GetPointer() {
        return state;
    }

    public State SetPointer(Pointer fmuState) {
        this.state = fmuState;
        return this;
    }
}
