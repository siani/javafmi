/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.proxy.v2;

import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class CallbackFunctions extends Structure {

    public Callbacks.Logger logger;
    public Callbacks.MemoryAllocator memoryAllocator;
    public Callbacks.MemoryLiberator memoryLiberator;
    public Callbacks.StepFinished stepFinished;
    public Pointer componentEnvironment;

    public CallbackFunctions() {
        super();
        this.logger = new Logger();
        MemoryHandler memoryHandler = new MemoryHandler();
        this.memoryAllocator = memoryHandler::allocateMemory;
        this.memoryLiberator = memoryHandler::freeMemory;
        this.stepFinished = new StepFinished();
        this.componentEnvironment = null;
    }

    @Override
    protected List getFieldOrder() {
        return Arrays.asList("logger", "memoryAllocator", "memoryLiberator", "stepFinished", "componentEnvironment");
    }

    public static class StepFinished implements Callbacks.StepFinished {
		@Override
		public void stepFinished(Pointer componentEnvironment, int status) {
		}
	}

    public static class MemoryHandler {

		private List<Pointer> pointers = new LinkedList<>();

		public Pointer allocateMemory(int numberOfObjects, int size) {
			pointers.add(0, createPointer((numberOfObjects * size)));
			return pointers.get(0);
		}

		public void freeMemory(Pointer pointer) {
			if (pointers.remove(pointer))
				freePointer(pointer);
		}

		private Pointer createPointer(int size) {
			size++;
			long peer = Native.malloc(size);
			Pointer pointer = new Pointer(peer);
			pointer.setMemory(0, size, (byte) 0);
			return pointer;
		}

		public void terminate() {
			pointers.forEach(this::freePointer);
			pointers.clear();
		}

		private void freePointer(Pointer pointer) {
			Native.free(Pointer.nativeValue(pointer));
		}
	}

    public static class Logger implements Callbacks.Logger {
		@Override
		public void logMessage(Pointer componentEnvironment, String instanceName, int status, String category, String message, Pointer args) {
			System.out.println("[" + instanceName + getCategory(category) + "] " + message);
		}

		private String getCategory(String category) {
			return category.isEmpty() ? "" : ":" + category;
		}
	}
}
