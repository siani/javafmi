/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.proxy.v2;

import com.sun.jna.Pointer;
import org.javafmi.framework.FmiSimulation;
import org.javafmi.framework.Logger;
import org.javafmi.kernel.JarClassLoader;
import org.javafmi.modeldescription.FmiVersion;
import org.javafmi.modeldescription.ScalarVariable;
import org.javafmi.modeldescription.v2.Capabilities;
import org.javafmi.modeldescription.v2.EnumerationType;
import org.javafmi.modeldescription.v2.ModelDescription;
import org.javafmi.proxy.FmiProxyV2;
import org.javafmi.proxy.FmuProxyState;
import org.javafmi.proxy.MethodExecutionException;
import org.javafmi.proxy.Status;

import java.io.File;
import java.lang.reflect.Field;

import static org.javafmi.kernel.Convention.FmuClassPackage;
import static org.javafmi.proxy.Status.*;

public class JavaFmuProxy implements FmiProxyV2 {

    private ModelDescription modelDescription;
    private FmiSimulation simulation;
    private long stateIndex = 1l;

    public JavaFmuProxy(ModelDescription modelDescription) {
        this.modelDescription = modelDescription;
    }

    @Override
    public void instantiate(String resourcesPath) {
        try {
            Class<?> aClass = new JarClassLoader(new File(resourcesPath + "/" + modelDescription.getModelName() + ".jar"))
                    .loadClass(FmuClassPackage + modelDescription.getModelName());
            simulation = (FmiSimulation) aClass.newInstance();
            simulation.logger(new Logger(modelDescription.getModelName(), System.out));
        } catch (IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Status initialize(double startTime, double stopTime) {
        if (ERROR == setUpExperiment(true, modelDescription.getDefaultExperiment().getTolerance(), startTime, !Double.isNaN(stopTime), Double.isNaN(stopTime) ? Double.MAX_VALUE : stopTime))
            throw new MethodExecutionException("fmi2SetupExperiment: " + modelDescription.getModelIdentifier());
        if (ERROR == enterInitializationMode())
            throw new MethodExecutionException("fmi2SetupExperiment: " + modelDescription.getModelIdentifier());
        if (ERROR == exitInitializationMode())
            throw new MethodExecutionException("fmi2SetupExperiment: " + modelDescription.getModelIdentifier());
        return OK;
    }

    @Override
    public Status doStep(double communicationPoint, double stepSize) {
        return statusFrom(simulation.doStep(stepSize));
    }

    @Override
    public Status cancelStep() {
        return statusFrom(simulation.cancelStep());
    }

    @Override
    public Status terminate() {
        return statusFrom(simulation.terminate());
    }

    private Status statusFrom(FmiSimulation.Status status) {
        return Status.translateStatus(status.code());
    }

    @Override
    public void freeInstance() {
    }

    @Override
    public Status reset() {
        return statusFrom(simulation.reset());
    }

    @Override
    public String getVersion() {
        return FmiVersion.Two;
    }

    @Override
    public double[] getReal(int... valueReference) {
        return simulation.getReal(valueReference);
    }

    @Override
    public int[] getInteger(int... valueReference) {
        return simulation.getInteger(valueReference);
    }

    @Override
    public boolean[] getBoolean(int... valueReference) {
        return simulation.getBoolean(valueReference);
    }

    @Override
    public String[] getString(int... valueReference) {
        return simulation.getString(valueReference);
    }

    @Override
    public Object[] getEnumeration(int... valueReference) {
        throw new UnsupportedOperationException("Enumerations are not available in FMUs built by the JavaFMI Builder");
    }

    @Override
    public Object getEnumeration(ScalarVariable modelVariable) {
        EnumerationType enumeration = (EnumerationType) modelVariable.getType();
        Integer enumerationIndex;
        EnumerationType enumerationType = (EnumerationType) modelDescription.getTypeFromTypeDefinition(enumeration.getDeclaredType());
        if (modelVariable.getVariability().equalsIgnoreCase("constant")) {
            enumerationIndex = enumeration.getStart();
            return enumerationType.getItems().get(enumerationIndex - 1).getName();
        }
        enumerationIndex = getInteger(modelVariable.getValueReference())[0];
        return enumerationType.getItems().get(enumerationIndex - 1).getName();
    }

    @Override
    public Status setReal(int[] valueReference, double[] doubleValue) {
        return statusFrom(simulation.setReal(valueReference, doubleValue));
    }

    @Override
    public Status setInteger(int[] valueReference, int[] intValue) {
        return statusFrom(simulation.setInteger(valueReference, intValue));
    }

    @Override
    public Status setBoolean(int[] valueReference, boolean[] booleanValue) {
        return statusFrom(simulation.setBoolean(valueReference, booleanValue));
    }

    @Override
    public Status setString(int[] valueReference, String[] stringValue) {
        return statusFrom(simulation.setString(valueReference, stringValue));
    }

    @Override
    public boolean isTerminated() {
        return false;
    }

    @Override
    public Status setDebugLogging(boolean loggingOn, String[] categories) {
        throw new UnsupportedOperationException("not implemented");
    }

    @Override
    public Status setUpExperiment(boolean toleranceDefined, double tolerance, double startTime, boolean stopTimeDefined, double stopTime) {
        return statusFrom(simulation.setupExperiment(toleranceDefined, tolerance, startTime, stopTimeDefined, stopTime));
    }

    @Override
    public Status enterInitializationMode() {
        return statusFrom(simulation.enterInitializationMode());
    }

    @Override
    public Status exitInitializationMode() {
        return statusFrom(simulation.exitInitializationMode());
    }

    @Override
    public ModelDescription getModelDescription() {
        return modelDescription;
    }

    @Override
    public Status setRealInputDerivatives(int[] valueReferences, int[] orders, double[] values) {
        throw new UnsupportedOperationException("not implemented");
    }

    @Override
    public double[] getRealOutputDerivatives(int[] valueReferences, int[] orders) {
        throw new UnsupportedOperationException("not implemented");
    }

    @Override
    public State getState(State state) {
        checkCapability("getState", Capabilities.CanGetAndSetFMUState);
        long stateIndex = this.stateIndex++;
        simulation.getState(stateId(stateIndex));
        return state.SetPointer(new Pointer(stateIndex));
    }

    @Override
    public Status setState(State state) {
        checkCapability("setState", Capabilities.CanGetAndSetFMUState);
        return statusFrom(simulation.setState(stateId(state)));
    }

    @Override
    public Status freeState(State state) {
        checkCapability("freeState", Capabilities.CanGetAndSetFMUState);
        return statusFrom(simulation.freeState(stateId(state)));
    }

    @Override
    public int getStateSize(State state) {
        throw new UnsupportedOperationException("not implemented");
    }

    @Override
    public byte[] serializeState(State state) {
        throw new UnsupportedOperationException("not implemented");
    }

    @Override
    public State deserializeState(byte[] serializedState) {
        throw new UnsupportedOperationException("not implemented");
    }

    @Override
    public void checkCapability(String method, Capabilities.Capability... capabilitiesToCheck) {
        for (Capabilities.Capability capability : capabilitiesToCheck)
            if (!modelDescription.check(capability))
                throw new UnsupportedOperationException(method + " " + modelDescription.getModelIdentifier());
    }

    @Override
    public double[] getDirectionalDerivative(int[] unknownVariablesValueReferences, int[] knownVariablesValueReferences, double[] knownDifferential) {
        throw new UnsupportedOperationException("not implemented");
    }

    private String stateId(long state) {
        return "s_" + state;
    }

    private String stateId(State state) {
        try {
            Field peerField = Pointer.class.getDeclaredField("peer");
            peerField.setAccessible(true);
            long peer = (long) peerField.get(state.GetPointer());
            peerField.setAccessible(true);
            return stateId(peer);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
            return null;
        }
    }
}
