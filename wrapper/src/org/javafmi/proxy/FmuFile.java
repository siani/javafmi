/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.proxy;

import org.javafmi.kernel.Files;
import org.javafmi.kernel.OS;
import org.javafmi.kernel.Unzipper;
import org.javafmi.modeldescription.FmiVersion;
import org.javafmi.modeldescription.ModelDescription;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.io.File.separator;
import static java.lang.System.getProperty;
import static java.util.Arrays.asList;
import static org.javafmi.kernel.Convention.FrameworkManufacturer;
import static org.javafmi.modeldescription.ModelDescriptionDeserializer.deserialize;
import static org.javafmi.proxy.ModelDescriptionPeeker.peekVersion;

public class FmuFile {

	public static final String BINARIES_FOLDER = "binaries";
	public static final String MAC_OS_FOLDER = "darwin";
	public static final String WINDOWS_FOLDER = "win";
	public static final String LINUX_FOLDER = "linux";
	public static final String ARCH_64 = "64";
	public static final String ARCH_32 = "32";
	public static final String MAC_OS_LIBRARY_EXTENSION = ".dylib";
	public static final String WINDOWS_LIBRARY_EXTENSION = ".dll";
	public static final String LINUX_LIBRARY_EXTENSION = ".so";
	public static final String MISSING_SYSTEM_MESSAGE = "JavaFMI is not prepared to your system";
	private static final String MODEL_DESCRIPTION_FILENAME = "modelDescription.xml";
	private final File temporalFolder;
	private final ModelDescription modelDescription;
	private File libraryFile;
	private File resourcesDirectory;

	public FmuFile(String filePath) {
		this(new File(filePath));
	}

	public FmuFile(File fmuFile) {
		this.temporalFolder = createTemporalFolder(fmuFile);
		new Unzipper(fmuFile).unzipAll(temporalFolder);
		this.modelDescription = loadModelDescription();
		this.libraryFile = getLibrary();
	}

	private File createTemporalFolder(File fmuFile) {
		File folder = new File(System.getProperty("java.io.tmpdir") + "/fmu_" + desambiguate() + fmuFile.getName().replace(".fmu", "").replace(".", "_"));
		folder.mkdirs();
		return folder;
	}

	private String desambiguate() {
		return UUID.randomUUID().toString().replace("-", "_");
	}

	public void deleteTemporalFolder() {
		deleteAll(temporalFolder);
	}

	public String[] getOperatingSystems() {
		return normalize(new File(getBinariesPath()).list());
	}

	private boolean isJavaFMU() {
		return modelDescription.getGenerationTool() != null && modelDescription.getGenerationTool().startsWith(FrameworkManufacturer);
	}

	public String getLibraryPath() {
		if (libraryFile != null) return libraryFile.getAbsolutePath();
		getLibrary();
		return libraryFile.getAbsolutePath();
	}

	private File getLibrary() {
		return new File(getBinariesPath() + libraryForCurrentOperatingSystem() + modelDescription.getModelIdentifier() + getLibraryExtension());
	}

	private String[] normalize(String[] folders) {
		final List<String> result = new ArrayList<>();
		if (isJavaFMU()) result.add(OS.Java);
		if (folders != null) for (String folder : folders) result.add(normalize(folder));
		return result.toArray(new String[result.size()]);
	}

	private String normalize(String folder) {
		return folder.replace("darwin", "Mac").replace("win", "Windows").replace("linux", "Linux");
	}


	private String getBinariesPath() {
		return temporalFolder + separator + BINARIES_FOLDER + separator;
	}

	private String libraryForCurrentOperatingSystem() {
		return getLibraryFolder() + getSystemArchitecture() + separator;
	}

	private String getLibraryFolder() {
		if (systemIsMacOS(getSystemName())) return MAC_OS_FOLDER;
		else if (systemIsWindows(getSystemName())) return WINDOWS_FOLDER;
		else if (systemIsLinux(getSystemName())) return LINUX_FOLDER;
		throw new RuntimeException(MISSING_SYSTEM_MESSAGE);
	}

	private String getSystemArchitecture() {
		return is64bitsArchitecture(getProperty("os.arch").toLowerCase()) ? ARCH_64 : ARCH_32;
	}

	private String getLibraryExtension() {
		if (systemIsMacOS(getSystemName())) return MAC_OS_LIBRARY_EXTENSION;
		else if (systemIsWindows(getSystemName())) return WINDOWS_LIBRARY_EXTENSION;
		else if (systemIsLinux(getSystemName())) return LINUX_LIBRARY_EXTENSION;
		throw new RuntimeException(MISSING_SYSTEM_MESSAGE);
	}

	private String getSystemName() {
		return getProperty("os.name").toLowerCase();
	}

	private boolean systemIsMacOS(String operatingSystem) {
		return operatingSystem.startsWith("mac");
	}

	private boolean systemIsWindows(String operatingSystemName) {
		return operatingSystemName.startsWith("win");
	}

	private boolean systemIsLinux(String operatingSystemName) {
		return operatingSystemName.startsWith("linux");
	}

	private boolean is64bitsArchitecture(String architecture) {
		return architecture.contains(ARCH_64);
	}

	public String getWorkingDirectoryPath() {
		return temporalFolder.getAbsolutePath();
	}

	private void deleteAll(File file) {
		if (file.isDirectory()) asList(file.listFiles()).forEach(this::deleteAll);
		file.delete();
	}

	public ModelDescription getModelDescription() {
		return modelDescription;
	}

	private ModelDescription loadModelDescription() {
		File modelDescriptionFile = new File(temporalFolder, MODEL_DESCRIPTION_FILENAME);
		return isV1(modelDescriptionFile) ?
				deserialize(modelDescriptionFile).asV1().build() :
				deserialize(modelDescriptionFile).asV2().build();
	}

	private boolean isV1(File modelDescriptionFile) {
		return fmuVersion(modelDescriptionFile).equals(FmiVersion.One);
	}

	private String fmuVersion(File modelDescriptionFile) {
		return peekVersion(modelDescriptionFile);
	}


	public String getResourcesDirectoryPath() {
		if (resourcesDirectory == null) resourcesDirectory = getResourcesDirectory();
		return resourcesDirectory.getAbsolutePath();
	}

	public File getResourcesDirectory() {
		File resources = new File(temporalFolder, "resources");
		return resources.exists() ? resources : Files.createDirectory(temporalFolder, "resources");
	}
}
