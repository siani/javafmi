	/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.proxy;


import org.javafmi.kernel.OS;
import org.javafmi.modeldescription.FmiVersion;
import org.javafmi.modeldescription.ModelDescription;
import org.javafmi.proxy.v2.Fmu;
import org.javafmi.proxy.v2.JavaFmuProxy;
import org.javafmi.proxy.v3.FmuProxy;

import java.io.File;

import static org.javafmi.kernel.Convention.FrameworkManufacturer;
import static org.javafmi.kernel.Convention.FrameworkVersion;

public class ProxyFactory {

	public static FmiProxy createProxy(FmuFile fmuFile) {
		if(!new File(fmuFile.getLibraryPath()).exists()) throw new RuntimeException("FMU is not compatible with this system " + OS.name() + OS.architecture() + ". File not found: " + fmuFile.getLibraryPath());
		if (fmuFile.getModelDescription().getFmiVersion().equals(FmiVersion.One))
			return new org.javafmi.proxy.v1.FmuProxy(fmuFile);
		else {
			org.javafmi.modeldescription.v2.ModelDescription md = (org.javafmi.modeldescription.v2.ModelDescription) fmuFile.getModelDescription();
			if (isJavaFMU(md) && isSameVersion(md)) return new JavaFmuProxy(md);
            else if(md.getFmiVersion().equals(FmiVersion.Two) && !isHybridCoSim(md))
				return new org.javafmi.proxy.v2.FmuProxy(new Fmu(fmuFile), md);
			else if(md.getFmiVersion().equals(FmiVersion.TwoHybrid) || isHybridCoSim(md))
				return new FmuProxy(new org.javafmi.proxy.v3.Fmu(fmuFile), md);
		}
        System.out.println("FMI version not found " + fmuFile.getModelDescription().getFmiVersion());
        return null;
    }

	private static boolean isHybridCoSim(org.javafmi.modeldescription.v2.ModelDescription md) {
		return !md.getVendorAnnotations().isEmpty() && md.getVendorAnnotations().get(0).getHybridCoSim() != null;
	}

	private static boolean isJavaFMU(ModelDescription modelDescription) {
		return modelDescription.getGenerationTool().startsWith(FrameworkManufacturer);
	}

	private static boolean isSameVersion(ModelDescription modelDescription) {
		return warn(modelDescription.getGenerationTool().endsWith(FrameworkVersion));
	}

	private static boolean warn(boolean sameVersion) {
		if (!sameVersion) System.out.println("WARNING: This wrapper version is not compatible with this JavaFMU. Performance will be worse");
		return sameVersion;
	}
}
