/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.proxy.v1;

import com.sun.jna.*;

import java.util.HashSet;
import java.util.Set;

interface Callbacks extends Library {

	interface FMIStatus {

		int fmiOK = 0;
		int fmiWarning = 1;
		int fmiDiscard = 2;
		int fmiError = 3;
		int fmiFatal = 4;
		int fmiPending = 5;
	}
	interface FMIStatusKind {

		int fmiDoStepStatus = 0;
		int fmiPendingStatus = 1;
		int fmiLastSuccessfulTime = 2;
	}

	interface CallbackLogger extends Callback {

		void record(Pointer fmiComponent, String instanceName, int status, String category, String message, Pointer parameters);
	}
	interface CallbackAllocateMemory extends Callback {

		Pointer allocate(Callbacks.Size_T numberOfObjects, Callbacks.Size_T size);
	}
	interface CallbackFreeMemory extends Callback {

		void free(Pointer object);
	}
	interface CallbackStepFinished extends Callback {

		void apply(Pointer fmiComponent, int status);
	}

	class Logger implements CallbackLogger {

		@Override
		public void record(Pointer fmiComponent, String logSource, int status, String category, String message, Pointer parameters) {
			System.out.print(logSource + " reports: " + category);
			System.out.format(" with status %d \n", status);
			if (parameters != null)
				System.out.println("There was a problem inside the fmu, but some information cannot be retrieved");
			System.out.println(message);
		}
	}

	class AllocateMemory implements CallbackAllocateMemory {

		public static Set<Pointer> onMemoryAllocatedObjects = new HashSet<>();
		private int MEMORY_OFFSET = 4;

		@Override
		public Pointer allocate(Size_T numberOfObjects, Size_T objectSize) {
			Memory memoryToShare = new Memory(fixNumberOfObjects(numberOfObjects.intValue()) * objectSize.intValue());
			memoryToShare.align(MEMORY_OFFSET);
			memoryToShare.clear();
			onMemoryAllocatedObjects.add(memoryToShare);
			return memoryToShare;
		}

		private int fixNumberOfObjects(int numberOfObjects) {
			return (numberOfObjects <= 0) ? 1 : numberOfObjects;
		}
	}

	class FreeMemory implements CallbackFreeMemory {

		@Override
		public void free(Pointer object) {
			AllocateMemory.onMemoryAllocatedObjects.remove(object);
			System.gc();
		}
	}

	class StepFinished implements CallbackStepFinished {

		@Override
		public void apply(Pointer fmiComponent, int status) {
		}
	}

	class Size_T extends IntegerType {

		public Size_T() {
			this(0);
		}

		public Size_T(long size) {
			super(com.sun.jna.Native.SIZE_T_SIZE, size);
		}
	}
}
