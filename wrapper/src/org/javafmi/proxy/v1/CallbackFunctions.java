/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.proxy.v1;

import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;

public class CallbackFunctions extends Structure {

    public Callbacks.CallbackLogger logger;
    public Callbacks.CallbackAllocateMemory allocateMemory;
    public Callbacks.CallbackFreeMemory freeMemory;
    public Callbacks.CallbackStepFinished stepFinished;

    public CallbackFunctions() {
        super();
        this.logger = new Callbacks.Logger();
        this.allocateMemory = new Callbacks.AllocateMemory();
        this.freeMemory = new Callbacks.FreeMemory();
        this.stepFinished = new Callbacks.StepFinished();
        setAlignType(Structure.ALIGN_GNUC);
    }

    @Override
    protected List getFieldOrder() {
        return Arrays.asList("logger", "allocateMemory", "freeMemory", "stepFinished");
    }

    public static class ByValue extends CallbackFunctions implements Structure.ByValue {

        public ByValue() {
            super();
        }
    }
}
