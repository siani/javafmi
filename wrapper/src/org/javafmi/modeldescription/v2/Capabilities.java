/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.modeldescription.v2;

import java.util.ArrayList;
import java.util.List;

public class Capabilities {
    public static final Capability NeedsExecutionTool = new Capability("needsExecutionTool");
    public static final Capability CanHandleVariableCommunicationStepSize = new Capability("canHandleVariableCommunicationStepSize");
    public static final Capability CanInterpolateInputs = new Capability("canInterpolateInputs");
    public static final Capability CanRunAsynchronuously = new Capability("canRunAsynchronuously");
    public static final Capability CanBeInstantiatedOnlyOncePerProcess = new Capability("canBeInstantiatedOnlyOncePerProcess");
    public static final Capability CanNotUseMemoryManagementFunctions = new Capability("canNotUseMemoryManagementFunctions");
    public static final Capability CanGetAndSetFMUState = new Capability("canGetAndSetFMUState");
    public static final Capability CanSerializeFMUState = new Capability("canSerializeFMUState");
    public static final Capability ProvidesDirectionalDerivative = new Capability("providesDirectionalDerivative");
    private List<Capability> capabilities;

    public Capabilities() {
        this.capabilities = new ArrayList<>();
    }

    public boolean check(Capability capability) {
        return capabilities.contains(capability);
    }

    public void add(Capability capability) {
        capabilities.add(capability);
    }

    public static class Capability {
        private String name;

        public Capability(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }
}
