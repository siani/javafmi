/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.modeldescription.v2;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root(strict = false)
public class Unknown {

    @Attribute(required = true)
    private int index;
    @Attribute(required = false)
    private String dependencies;
    @Attribute(required = false)
    private String dependenciesKind;

    public int getIndex() {
        return index;
    }

    public String getDependencies() {
        return dependencies;
    }

    public String getDependenciesKind() {
        return dependenciesKind;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public void setDependencies(String dependencies) {
        this.dependencies = dependencies;
    }

    public void setDependenciesKind(String dependenciesKind) {
        this.dependenciesKind = dependenciesKind;
    }
}
