/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.modeldescription.v2;

import org.simpleframework.xml.Attribute;

public class DefaultExperiment {

    public static final double DEFAULT_TOLERANCE = 1e-6;
    @Attribute(required = false)
    private double tolerance = DEFAULT_TOLERANCE;
    @Attribute(required = false)
    private double startTime;
    @Attribute(required = false)
    private double stopTime;
    @Attribute(required = false)
    private double stepSize;

    public double getStartTime() {
        return startTime;
    }

    public double getStopTime() {
        return stopTime;
    }

    public double getTolerance() {
        return tolerance;
    }

    public double getStepSize() {
        return stepSize;
    }
}
