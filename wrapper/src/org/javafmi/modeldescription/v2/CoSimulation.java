/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.modeldescription.v2;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

@Root(strict = false)
public class CoSimulation {

    @Attribute(required = true)
    private String modelIdentifier;
    @Attribute(required = false)
    private boolean needsExecutionTool;
    @Attribute(required = false)
    private boolean canHandleVariableCommunicationStepSize;
    @Attribute(required = false)
    private boolean canInterpolateInputs;
    @Attribute(required = false)
    private int maxOutputDerivativeOrder;
    @Attribute(required = false)
    private boolean canRunAsynchronuously;
    @Attribute(required = false)
    private boolean canBeInstantiatedOnlyOncePerProcess;
    @Attribute(required = false)
    private boolean canNotUseMemoryManagementFunctions;
    @Attribute(required = false)
    private boolean canGetAndSetFMUstate;
    @Attribute(required = false)
    private boolean canSerializeFMUstate;
    @Attribute(required = false)
    private boolean providesDirectionalDerivative;
    @ElementList(required = false)
    private List<SourceFile> sourceFiles;
    private Capabilities capabilities;

    public String getModelIdentifier() {
        return modelIdentifier;
    }

    public Capabilities capabilities() {
        if (capabilities != null) return capabilities;
        capabilities = new Capabilities();
        if (needsExecutionTool) capabilities.add(Capabilities.NeedsExecutionTool);
        if (canHandleVariableCommunicationStepSize)
            capabilities.add(Capabilities.CanHandleVariableCommunicationStepSize);
        if (canInterpolateInputs) capabilities.add(Capabilities.CanInterpolateInputs);
        if (canRunAsynchronuously) capabilities.add(Capabilities.CanRunAsynchronuously);
        if (canBeInstantiatedOnlyOncePerProcess)
            capabilities.add(Capabilities.CanBeInstantiatedOnlyOncePerProcess);
        if (canNotUseMemoryManagementFunctions) capabilities.add(Capabilities.CanNotUseMemoryManagementFunctions);
        if (canGetAndSetFMUstate) capabilities.add(Capabilities.CanGetAndSetFMUState);
        if (canSerializeFMUstate) capabilities.add(Capabilities.CanSerializeFMUState);
        if (providesDirectionalDerivative) capabilities.add(Capabilities.ProvidesDirectionalDerivative);
        return capabilities;
    }

    public int getMaxOutputDerivativeOrder() {
        return maxOutputDerivativeOrder;
    }

    public List<SourceFile> getSourceFiles() {
        return sourceFiles;
    }
}
