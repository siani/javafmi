/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.modeldescription;

import org.simpleframework.xml.core.Persister;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class ModelDescriptionDeserializer {

	public static DeserializeRequest deserialize(File modelDescriptionFile) {
		try {
			return new DeserializeRequest(new FileInputStream(modelDescriptionFile));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static DeserializeRequest deserialize(InputStream stream) {
		return new DeserializeRequest(stream);
	}

	public static class DeserializeRequest {
		private final InputStream inputStream;

		public DeserializeRequest(InputStream inputStream) {
			this.inputStream = inputStream;
		}

		public org.javafmi.modeldescription.v2.ModelDescription asV2() {
			return (org.javafmi.modeldescription.v2.ModelDescription) read(org.javafmi.modeldescription.v2.ModelDescription.class);
		}

		public org.javafmi.modeldescription.v1.ModelDescription asV1() {
			return (org.javafmi.modeldescription.v1.ModelDescription) read(org.javafmi.modeldescription.v1.ModelDescription.class);
		}

		private ModelDescription read(Class<? extends ModelDescription> modelDescription) {
			try {
				return new Persister().read(modelDescription, inputStream).build();
			} catch (Exception e) {
				throw new RuntimeException("impossible to deserialize model description file", e);
			}
		}

		public ModelDescription as(String version) {
			return (FmiVersion.One.equals(version)) ? asV1() : asV2();
		}
	}
}
