/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.modeldescription;

import org.javafmi.modeldescription.v2.BooleanType;
import org.javafmi.modeldescription.v2.Capabilities;
import org.junit.Test;
import org.simpleframework.xml.core.Persister;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class ModelDescriptionTest {

    private static final String ModelDescriptionPath_v1 = "../wrapper/test-res/Model Description/v1/";
    private static final String ModelDescriptionPath_v2 = "../wrapper/test-res/Model Description/v2/";
    private static final String filename = "modelDescription.xml";

    @Test
    public void parsesCheckAllV1ModelDescriptionExamples() throws Exception {
        parseModelDescriptionFiles(getModelDescriptionFiles(new File(ModelDescriptionPath_v1)), FmiVersion.One);
    }

    @Test
    public void parsesAllV2ModelDescriptionExamples() throws Exception {
        parseModelDescriptionFiles(getModelDescriptionFiles(new File(ModelDescriptionPath_v2)), FmiVersion.Two);
    }

    @Test
    public void defaultToleranceIsAsDescribedInXML() {
        org.javafmi.modeldescription.v2.ModelDescription modelDescription = ModelDescriptionDeserializer.deserialize(new File(ModelDescriptionPath_v2 + "cosimulation/linux_64_V2_tankv3/modelDescription.xml")).asV2();
        assertThat(modelDescription.getDefaultExperiment().getTolerance(), is(0.02));
    }

    private ArrayList<File> getModelDescriptionFiles(File file) throws FileNotFoundException {
        if (!file.exists()) throw new FileNotFoundException(file.getPath());
        ArrayList<File> fileList = new ArrayList<>();
        if (file.isDirectory())
            for (File child : file.listFiles())
                fileList.addAll(getModelDescriptionFiles(child));
        else if (file.getName().equals(filename))
            fileList.add(file);
        return fileList;
    }

    private void parseModelDescriptionFiles(ArrayList<File> modelDescriptionFiles, String version) {
        for (File file : modelDescriptionFiles) {
            try {
                if (ModelDescriptionDeserializer.deserialize(file).as(version) == null) {
                    System.out.println(file.getAbsolutePath());
                    fail();
                }
            } catch (Exception e) {
                System.err.println(file.getAbsolutePath());
                e.printStackTrace();
                fail();
            }
        }
    }

    @Test
    public void whenVariableIsNotDefinedV1ModelDescriptionThrowsAnException() {
        try {
            org.javafmi.modeldescription.v1.ModelDescription modelDescription = new org.javafmi.modeldescription.v1.ModelDescription().build();
            modelDescription.getModelVariable("NotDefinedVariable");
            assertTrue("Should throw an exception", false);
        } catch (NoSuchVariableException e) {
            assertTrue(true);
        }
    }

    @Test
    public void whenVariableIsNotDefinedV2ModelDescriptionThrowsAnException() {
        try {
            org.javafmi.modeldescription.v2.ModelDescription modelDescription = new org.javafmi.modeldescription.v2.ModelDescription().build();
            modelDescription.getModelVariable("NotDefinedVariable");
            assertTrue("Should throw an exception", false);
        } catch (NoSuchVariableException e) {
            assertTrue(true);
        }
    }

    @Test
    public void checkingV2Capabilities() {
        File modelDescriptionFile = new File(ModelDescriptionPath_v2 + "cosimulation/linux_64_V2_tankv3/" + filename);
        org.javafmi.modeldescription.v2.ModelDescription modelDescription = ModelDescriptionDeserializer.deserialize(modelDescriptionFile).asV2();
        assertEquals(false, modelDescription.check(Capabilities.NeedsExecutionTool));
        assertEquals(true, modelDescription.check(Capabilities.CanHandleVariableCommunicationStepSize));
        assertEquals(true, modelDescription.check(Capabilities.CanInterpolateInputs));
        assertEquals(false, modelDescription.check(Capabilities.CanRunAsynchronuously));
        assertEquals(true, modelDescription.check(Capabilities.CanBeInstantiatedOnlyOncePerProcess));
        assertEquals(false, modelDescription.check(Capabilities.CanNotUseMemoryManagementFunctions));
        assertEquals(false, modelDescription.check(Capabilities.CanSerializeFMUState));
        assertEquals(true, modelDescription.check(Capabilities.ProvidesDirectionalDerivative));
    }

    @Test
    public void shouldBeAbleToReadABooleanAsTrueOrFalseAnd1or0() throws Exception {
        assertThat(new Persister().read(BooleanType.class, "<Boolean start=\"true\"/>").getStart(), is(true));
        assertThat(new Persister().read(BooleanType.class, "<Boolean start=\"false\"/>").getStart(), is(false));
        assertThat(new Persister().read(BooleanType.class, "<Boolean start=\"1\"/>").getStart(), is(true));
        assertThat(new Persister().read(BooleanType.class, "<Boolean start=\"0\"/>").getStart(), is(false));
    }

    @Test
    public void shouldBeAbleToReadAModelDescriptionIncludingModelStructure() throws Exception {
        File modelDescriptionFile = new File(ModelDescriptionPath_v2 + "cosimulation/with_model_structure/" + filename);
        org.javafmi.modeldescription.v2.ModelDescription modelDescription = ModelDescriptionDeserializer.deserialize(modelDescriptionFile).asV2();
    }
}
