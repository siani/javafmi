/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.modeldescription;

import org.javafmi.modeldescription.v2.UndefinedInitial;
import org.junit.Assert;
import org.junit.Test;

import static org.javafmi.modeldescription.v2.UndefinedInitial.UNDEFINED;
import static org.junit.Assert.assertEquals;

public class UndefinedInitialTest {

    @Test
    public void testWhenUndefined() throws Exception {
        Assert.assertEquals(UNDEFINED, new UndefinedInitial().defineFrom("parameter", "constant"));
        assertEquals(UNDEFINED, new UndefinedInitial().defineFrom("calculatedParameter", "constant"));
        assertEquals(UNDEFINED, new UndefinedInitial().defineFrom("input", "constant"));
        assertEquals(UNDEFINED, new UndefinedInitial().defineFrom("independent", "constant"));
        assertEquals(UNDEFINED, new UndefinedInitial().defineFrom("input", "fixed"));
        assertEquals(UNDEFINED, new UndefinedInitial().defineFrom("output", "fixed"));
        assertEquals(UNDEFINED, new UndefinedInitial().defineFrom("independent", "fixed"));
        assertEquals(UNDEFINED, new UndefinedInitial().defineFrom("input", "tunable"));
        assertEquals(UNDEFINED, new UndefinedInitial().defineFrom("output", "tunable"));
        assertEquals(UNDEFINED, new UndefinedInitial().defineFrom("independent", "tunable"));
        assertEquals(UNDEFINED, new UndefinedInitial().defineFrom("parameter", "discrete"));
        assertEquals(UNDEFINED, new UndefinedInitial().defineFrom("calculatedParameter", "discrete"));
        assertEquals(UNDEFINED, new UndefinedInitial().defineFrom("input", "discrete"));
        assertEquals(UNDEFINED, new UndefinedInitial().defineFrom("independent", "discrete"));
        assertEquals(UNDEFINED, new UndefinedInitial().defineFrom("parameter", "continuous"));
        assertEquals(UNDEFINED, new UndefinedInitial().defineFrom("calculatedParameter", "continuous"));
        assertEquals(UNDEFINED, new UndefinedInitial().defineFrom("input", "continuous"));
        assertEquals(UNDEFINED, new UndefinedInitial().defineFrom("independent", "continuous"));
    }

    @Test
    public void testWhenExact() throws Exception {
        assertEquals("exact", new UndefinedInitial().defineFrom("output", "constant"));
        assertEquals("exact", new UndefinedInitial().defineFrom("local", "constant"));
        assertEquals("exact", new UndefinedInitial().defineFrom("parameter", "fixed"));
        assertEquals("exact", new UndefinedInitial().defineFrom("parameter", "tunable"));
    }

    @Test
    public void testWhenCalculated() throws Exception {
        assertEquals("calculated", new UndefinedInitial().defineFrom("calculatedParameter", "fixed"));
        assertEquals("calculated", new UndefinedInitial().defineFrom("calculatedParameter", "tunable"));
        assertEquals("calculated", new UndefinedInitial().defineFrom("local", "fixed"));
        assertEquals("calculated", new UndefinedInitial().defineFrom("local", "tunable"));
        assertEquals("calculated", new UndefinedInitial().defineFrom("output", "discrete"));
        assertEquals("calculated", new UndefinedInitial().defineFrom("local", "discrete"));
        assertEquals("calculated", new UndefinedInitial().defineFrom("output", "continuous"));
        assertEquals("calculated", new UndefinedInitial().defineFrom("local", "continuous"));
    }
}
