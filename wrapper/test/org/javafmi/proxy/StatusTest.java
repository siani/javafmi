/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.proxy;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StatusTest {

    @Test
    public void testConvert() {
        assertEquals(Status.OK, Status.translateStatus(0));
        assertEquals(Status.WARNING, Status.translateStatus(1));
        assertEquals(Status.DISCARD, Status.translateStatus(2));
        assertEquals(Status.ERROR, Status.translateStatus(3));
        assertEquals(Status.FATAL, Status.translateStatus(4));
        assertEquals(Status.PENDING, Status.translateStatus(5));
    }
}
