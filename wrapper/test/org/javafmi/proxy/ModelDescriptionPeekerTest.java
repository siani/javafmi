/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.proxy;

import org.javafmi.modeldescription.FmiVersion;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;

public class ModelDescriptionPeekerTest {

    private static final String ModelDescriptionPath = "../wrapper/test-res/Model Description/";
    private static final String ModelDescriptionPath_v1 = ModelDescriptionPath + "v1/cosimulation/";
    private static final String ModelDescriptionPath_v2 = ModelDescriptionPath + "v2/cosimulation/";

    @Test
    public void testIdentifyVersionOne() {
        ModelDescriptionPeeker identifier = new ModelDescriptionPeeker();
        assertEquals(FmiVersion.One, identifier.peekVersion(new File(ModelDescriptionPath_v1 + "SFR/modelDescription.xml")));
        assertEquals(FmiVersion.One, identifier.peekVersion(new File(ModelDescriptionPath_v1 + "Strange/modelDescription.xml")));
        assertEquals(FmiVersion.One, identifier.peekVersion(new File(ModelDescriptionPath_v1 + "bouncingball/modelDescription.xml")));
    }

    @Test
    public void testIdentifyVersionTwo() {
        ModelDescriptionPeeker identifier = new ModelDescriptionPeeker();
        assertEquals(FmiVersion.Two, identifier.peekVersion(new File(ModelDescriptionPath_v2 + "linux_64_V2_tankv3/modelDescription.xml")));
        assertEquals(FmiVersion.Two, identifier.peekVersion(new File(ModelDescriptionPath_v2 + "linux_64_V2RC2_input_output_tankv5/modelDescription.xml")));
    }

    @Test
    public void testIdentifyVersionTwoWhenVersionAttributeIsPresent() {
        ModelDescriptionPeeker identifier = new ModelDescriptionPeeker();
        assertEquals(FmiVersion.Two, identifier.peekVersion(new File(ModelDescriptionPath_v2 + "with_version_attribute_same_line/modelDescription.xml")));
    }
}
