/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.proxy;

import org.javafmi.modeldescription.ModelDescription;
import org.junit.Test;

import java.io.File;

import static org.hamcrest.Matchers.notNullValue;
import static org.javafmi.kernel.OS.isLinux32;
import static org.javafmi.kernel.OS.isWin32;
import static org.javafmi.proxy.fmus.Index.*;
import static org.junit.Assert.*;
import static org.junit.Assume.assumeTrue;

public class FmuFileTest {

    @Test
    public void testExtractingModelDescription() {
        testExtractModelDescription(Win32_v2);
        testExtractModelDescription(Linux32);
    }

    @Test
    public void testExtractingLibrary() {
        assumeTrue(isWin32() || isLinux32());
        if (isWin32()) testExtractLibrary(Win32_v2, "bouncingBall", ".dll");
        else if (isLinux32()) testExtractLibrary(Linux32, "plusun", ".so");
    }

    @Test
    public void testExtractingAnFMUWithEmptyResourcesDirectory() {
        FmuFile fmu = createFmuFile(WithResourcesButEmpty);
        File resources = new File(fmu.getResourcesDirectoryPath());
        File[] files = resources.listFiles();
        assertEquals(resources.getPath(), fmu.getResourcesDirectoryPath());
        assertTrue(resources.getPath().endsWith("resources"));
        assertEquals(0, files.length);
        fmu.deleteTemporalFolder();
    }

    @Test
    public void testExtractingAnFMUWithSeveralFilesInResourcesDirectory() {
        FmuFile fmu = createFmuFile(WithResources);
        File resources = new File(fmu.getResourcesDirectoryPath());
        File[] files = resources.listFiles();
        assertEquals(resources.getPath(), fmu.getResourcesDirectoryPath());
        assertTrue(resources.getPath().endsWith("resources"));
        assertEquals(3, files.length);
        fmu.deleteTemporalFolder();
    }


    @Test
    public void testDeletionOfTemporalFile() {
        assumeTrue(isWin32() || isLinux32());
        if (isWin32()) this.testDeletionOfTemporalFile(Win32_v2);
        else if (isLinux32()) testDeletionOfTemporalFile(Linux32);
    }

    private void testExtractModelDescription(String fmuPath) {
        FmuFile fmuFile = createFmuFile(fmuPath);
        ModelDescription modelDescription = fmuFile.getModelDescription();
        assertNotNull(modelDescription);
        assertThat(modelDescription.getModelName(), notNullValue());
    }

    private void testDeletionOfTemporalFile(String fmuPath) {
        FmuFile fmuFile = createFmuFile(fmuPath);
        assertTrue(new File(fmuFile.getWorkingDirectoryPath()).exists());
        fmuFile.deleteTemporalFolder();
        assertTrue(!new File(fmuFile.getWorkingDirectoryPath()).exists());
    }

    private void testExtractLibrary(String fmuPath, String modelName, String libraryExtension) {
        FmuFile fmuFile = createFmuFile(fmuPath);
        String libraryFile = fmuFile.getLibraryPath().toString();
        assertNotNull(libraryFile);
        assertTrue(libraryFile, libraryFile.endsWith(modelName + libraryExtension));
    }
}
