/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.proxy.v1;

import org.javafmi.proxy.FmuFile;
import org.javafmi.proxy.FmiProxy;
import org.javafmi.proxy.Status;
import org.junit.Test;

import static org.javafmi.kernel.OS.isWin32;
import static org.javafmi.proxy.fmus.Index.*;
import static org.junit.Assert.*;
import static org.junit.Assume.assumeTrue;

public class FmuProxyTransferOfInputOutputTest {

    @Test
    public void testGetReal() {
        assumeTrue(isWin32());
        testGetRealInWin32CapableFMU();
    }

    @Test
    public void testGetInteger() {
        assumeTrue(isWin32());
        testGetIntegerInWin32CapableFMU();
    }

    @Test
    public void testGetBoolean() {
        assumeTrue(isWin32());
        testGetBooleanInWin32CapableFMU();
    }

    @Test
    public void testGetString() {
        assumeTrue(isWin32());
        testGetStringInWin32CapableFMU();
    }

    @Test
    public void testGetEnumeration() {
        assumeTrue(isWin32());
        testGetEnumerationInWin32CapableFMU();
    }

    @Test
    public void testSetReal() {
        assumeTrue(isWin32());
        testSetRealInWin32CapableFMU();
    }

    @Test
    public void testSetInteger() {
        assumeTrue(isWin32());
        testSetIntegerInWin32CapableFMU();
    }

    private void testGetRealInWin32CapableFMU() {
        FmuFile fmuFile = createFmuFile(WithReadableReal);
        FmiProxy fmiProxy = createProxy(fmuFile);
        double startTime = 0.;
        double stopTime = 1.;
        fmiProxy.instantiate(fmuFile.getResourcesDirectoryPath());
        fmiProxy.initialize(startTime, stopTime);
        double[] realValues = fmiProxy.getReal(0, 2, 3, 4);
        fmiProxy.terminate();
        fmiProxy.freeInstance();

        assertEquals(1.0, realValues[0], 0.01);
        assertEquals(0.0, realValues[1], 0.01);
        assertEquals(-9.81, realValues[2], 0.01);
        assertEquals(0.7, realValues[3], 0.01);

    }

    private void testGetIntegerInWin32CapableFMU() {
        FmuFile fmuFile = createFmuFile(WithReadableInteger);
        FmiProxy fmiProxy = createProxy(fmuFile);
        double startTime = 0.;
        double stopTime = 1.;
        fmiProxy.instantiate(fmuFile.getResourcesDirectoryPath());
        fmiProxy.initialize(startTime, stopTime);
        int[] integerValues = fmiProxy.getInteger(0);
        fmiProxy.terminate();
        fmiProxy.freeInstance();

        assertEquals(1, integerValues[0]);
    }

    private void testGetBooleanInWin32CapableFMU() {
        FmuFile fmuFile = createFmuFile(WithReadableBoolean);
        FmiProxy fmiProxy = createProxy(fmuFile);
        double startTime = 0.;
        double stopTime = 1.;
        fmiProxy.instantiate(fmuFile.getResourcesDirectoryPath());
        fmiProxy.initialize(startTime, stopTime);
        boolean[] booleanValue = fmiProxy.getBoolean(0);
        fmiProxy.terminate();
        fmiProxy.freeInstance();
        assertTrue(booleanValue[0]);
    }

    private void testGetStringInWin32CapableFMU() {
        FmuFile fmuFile = createFmuFile(WithReadableString);
        FmiProxy fmiProxy = createProxy(fmuFile);
        double startTime = 0.;
        double stopTime = 1.;
        fmiProxy.instantiate(fmuFile.getResourcesDirectoryPath());
        fmiProxy.initialize(startTime, stopTime);
        String[] stringValue = fmiProxy.getString(1);
        fmiProxy.terminate();
        fmiProxy.freeInstance();
        assertEquals("jan", stringValue[0]);
    }

    private void testGetEnumerationInWin32CapableFMU() {
        FmuFile fmuFile = createFmuFile(WithReadableEnumeration);
        FmiProxy fmiProxy = createProxy(fmuFile);
        double startTime = 0.;
        double stopTime = 1.;
        fmiProxy.instantiate(fmuFile.getResourcesDirectoryPath());
        fmiProxy.initialize(startTime, stopTime);
        Object enumerationValue = fmiProxy.getEnumeration(fmuFile.getModelDescription().getModelVariable("regulation_vitesse.transferFunction.initType"));
        fmiProxy.terminate();
        fmiProxy.freeInstance();
        assertEquals("InitialOutput", enumerationValue);
    }

    private void testSetRealInWin32CapableFMU() {
        FmuFile fmuFile = createFmuFile(WithWritableReal);
        FmiProxy fmiProxy = createProxy(fmuFile);
        double startTime = 0.0;
        double stopTime = 1.0;
        fmiProxy.instantiate(fmuFile.getResourcesDirectoryPath());
        Status setRealStatus = fmiProxy.setReal(new int[]{0}, new double[]{50.0});
        fmiProxy.initialize(startTime, stopTime);
        double realValue = fmiProxy.getReal(0)[0];
        fmiProxy.terminate();
        fmiProxy.freeInstance();

        assertEquals(Status.OK, setRealStatus);
        assertEquals(50, realValue, 0.01);
    }

    private void testSetIntegerInWin32CapableFMU() {
        FmuFile fmuFile = createFmuFile(WithWritableInteger);
        FmiProxy fmiProxy = createProxy(fmuFile);
        double startTime = 0;
        double stopTime = 1.;
        fmiProxy.instantiate(fmuFile.getResourcesDirectoryPath());
        Status setIntegerStatus = fmiProxy.setInteger(new int[]{0}, new int[]{5});
        fmiProxy.initialize(startTime, stopTime);
        int[] integerValues = fmiProxy.getInteger(0);
        fmiProxy.terminate();
        fmiProxy.freeInstance();

        assertEquals(Status.OK, setIntegerStatus);
        assertEquals(5, integerValues[0]);
    }
}
