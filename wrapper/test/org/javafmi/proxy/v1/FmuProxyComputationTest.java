/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.proxy.v1;

import org.javafmi.proxy.FmuFile;
import org.javafmi.proxy.FmiProxy;
import org.javafmi.proxy.Status;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static org.javafmi.kernel.OS.isLinux32;
import static org.javafmi.kernel.OS.isWin32;
import static org.javafmi.proxy.fmus.Index.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeTrue;

public class FmuProxyComputationTest {

    private static final double START_TIME = 0.;
    private static final double STOP_TIME = 10.;

    @Test
    public void testDoStep() {
        assumeTrue(isWin32() || isLinux32());
        if (isWin32()) testDoStepsForWin32Fmus();
        else if (isLinux32()) testDoStepsForLinux32Fmus();
    }

    @Test
    public void testStepCantBeCancelled() {
        assumeTrue(isWin32() || isLinux32());
        if (isWin32()) testCancelStepsForWin32Fmus();
        else if (isLinux32()) testCancelStepsForLinux32Fmus();
    }

    private void testDoStepsForWin32Fmus() {
        testDoStepForBouncingBall();
        testDoStepForIncrements();
    }

    private void testDoStepsForLinux32Fmus() {
        testDoStepForPlusun();
    }

    private void testDoStepForBouncingBall() {
        double[] height = {1.0, 0.955855, 0.813609, 0.573264, 0.234819, 0.105131};
        double stepSize = 0.1;
        FmuFile fmuFile = createFmuFile(Win32_v2);
        FmiProxy fmiProxy = createProxy(fmuFile);
        fmiProxy.instantiate(fmuFile.getResourcesDirectoryPath());
        Status initializeStatus = fmiProxy.initialize(START_TIME, STOP_TIME);
        boolean doubleValuesAreEquals = true;
        List<Status> doStepStatuses = new LinkedList<>();
        double simulationTime = 0;
        for (double i : height) {
            doubleValuesAreEquals &= (i - fmiProxy.getReal(0)[0] < 0.001);
            doStepStatuses.add(fmiProxy.doStep(simulationTime, stepSize));
            simulationTime += stepSize;
        }
        Status terminateStatus = fmiProxy.terminate();
        fmiProxy.freeInstance();

        assertEquals(Status.OK, initializeStatus);
        assertTrue(doubleValuesAreEquals);
        for (Status doStepStatus : doStepStatuses)
            assertEquals(Status.OK, doStepStatus);
        assertEquals(Status.OK, terminateStatus);
    }

    private void testDoStepForIncrements() {
        FmuFile fmuFile = createFmuFile(WithIncrements);
        FmiProxy fmiProxy = createProxy(fmuFile);
        fmiProxy.instantiate(fmuFile.getResourcesDirectoryPath());
        Status initializeStatus = fmiProxy.initialize(START_TIME, STOP_TIME);
        List<Status> doStepStatuses = new LinkedList<>();
        double simulationTime = 0;
        for (int i = 0; i < 10; i++) {
            doStepStatuses.add(fmiProxy.doStep(simulationTime, 1));
            simulationTime += 1;
        }
        Status terminateStatus = fmiProxy.terminate();
        fmiProxy.freeInstance();

        assertEquals(Status.OK, initializeStatus);
        for (Status doStepStatus : doStepStatuses) {
            assertEquals(Status.OK, doStepStatus);
        }
        assertEquals(Status.OK, terminateStatus);
    }

    private void testDoStepForPlusun() {
        FmuFile fmuFile = createFmuFile(Linux32);
        FmiProxy fmiProxy = createProxy(fmuFile);
        fmiProxy.instantiate(fmuFile.getResourcesDirectoryPath());
        Status initializeStatus = fmiProxy.initialize(START_TIME, STOP_TIME);
        double stepSize = 0.12;
        List<Status> doStepStatuses = new LinkedList<>();
        double simulationTime = 0;
        for (int i = 0; i < 1000; i++) {
            doStepStatuses.add(fmiProxy.doStep(simulationTime, stepSize));
            simulationTime += stepSize;
        }
        Status terminateStatus = fmiProxy.terminate();
        fmiProxy.freeInstance();


        assertEquals(Status.OK, initializeStatus);
        for (Status doStepStatus : doStepStatuses)
            assertEquals(Status.OK, doStepStatus);
        assertEquals(Status.OK, terminateStatus);
    }

    private void testCancelStepsForWin32Fmus() {
        testCancelStepsForBouncingBall();
    }

    private void testCancelStepsForBouncingBall() {
        double stepSize = 0.1;
        FmuFile fmuFile = createFmuFile(Win32_v2);
        FmiProxy fmiProxy = createProxy(fmuFile);
        fmiProxy.instantiate(fmuFile.getResourcesDirectoryPath());
        Status initializeStatus = fmiProxy.initialize(START_TIME, STOP_TIME);
        Status doStepStatus1 = fmiProxy.doStep(START_TIME, stepSize);
        Status cancelStepStatus = fmiProxy.cancelStep();
        Status doStepStatus2 = fmiProxy.doStep(START_TIME, stepSize);
        Status terminateStatus = fmiProxy.terminate();
        fmiProxy.freeInstance();

        assertEquals(Status.OK, initializeStatus);
        assertEquals(Status.OK, doStepStatus1);
        assertEquals(Status.ERROR, cancelStepStatus);
        assertEquals(Status.OK, doStepStatus2);
        assertEquals(Status.OK, terminateStatus);
    }

    private void testCancelStepsForLinux32Fmus() {
        testCancelStepsForPlusun();
    }

    private void testCancelStepsForPlusun() {
        double startTime = 0.;
        double stopTime = 5.;
        double stepSize = 1.;
        FmuFile fmuFile = createFmuFile(Linux32);
        FmiProxy fmiProxy = createProxy(fmuFile);
        fmiProxy.instantiate(fmuFile.getResourcesDirectoryPath());
        Status initializeStatus = fmiProxy.initialize(startTime, stopTime);
        Status doStepStatus1 = fmiProxy.doStep(startTime, stepSize);
        Status cancelStepStatus = fmiProxy.cancelStep();
        Status doStepStatus2 = fmiProxy.doStep(startTime, stepSize);
        Status terminateStatus = fmiProxy.terminate();

        assertEquals(Status.OK, initializeStatus);
        assertEquals(Status.OK, doStepStatus1);
        assertEquals(Status.ERROR, cancelStepStatus);
        assertEquals(Status.OK, doStepStatus2);
        assertEquals(Status.OK, terminateStatus);
        fmiProxy.freeInstance();
    }
}
