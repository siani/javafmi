/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.proxy.v1;

import org.javafmi.proxy.FmuFile;
import org.javafmi.proxy.Status;
import org.javafmi.proxy.fmus.Index;
import org.junit.Test;

import static org.javafmi.kernel.OS.isLinux32;
import static org.javafmi.kernel.OS.isWin32;
import static org.javafmi.proxy.fmus.Index.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assume.assumeTrue;

public class FmuProxyCreationAndDestructionTest {

    private static final double START_TIME = 0.0;
    private static final double STOP_TIME = 10.0;
    private static final double STEP_SIZE = 0.2;

    @Test
    public void createAndDestroyInstance() {
        assumeTrue(isWin32() || isLinux32());
        FmuFile fmuFile = createFmuFile(Index.getFmuNameForThisOS());
        FmuProxy fmuProxy = (FmuProxy) createProxy(fmuFile);
        fmuProxy.instantiate(fmuFile.getResourcesDirectoryPath());
        fmuProxy.initialize(START_TIME, STOP_TIME);
        Status terminateStatus = fmuProxy.terminate();
        fmuProxy.freeInstance();
        assertEquals(Status.OK, terminateStatus);
    }

    @Test
    public void testSetDebugSlave() {
        assumeTrue(isWin32() || isLinux32());
        testDebugForCapableFmu();
    }

    @Test
    public void testDoStepAndReset() {
        assumeTrue(isWin32() || isLinux32());
        FmuFile fmuFile = createFmuFile(Index.getFmuNameForThisOS());
        FmuProxy fmuProxy = (FmuProxy) createProxy(fmuFile);
        fmuProxy.instantiate(fmuFile.getResourcesDirectoryPath());
        fmuProxy.initialize(START_TIME, STOP_TIME);
        fmuProxy.doStep(START_TIME, STEP_SIZE);
        Status resetStatus = fmuProxy.reset();
        fmuProxy.terminate();
        fmuProxy.freeInstance();

        assertEquals(Status.OK, resetStatus);
    }

    private void testDebugForCapableFmu() {
        FmuFile fmuFile = createFmuFile(Win32_v2);
        org.javafmi.proxy.v1.FmuProxy proxy = createProxy(fmuFile);
        proxy.instantiate(fmuFile.getResourcesDirectoryPath());
        Status setLoggingStatus = proxy.setIsDoingLogging(true);
        proxy.terminate();
        proxy.freeInstance();
        assertEquals(Status.OK, setLoggingStatus);
    }
}
