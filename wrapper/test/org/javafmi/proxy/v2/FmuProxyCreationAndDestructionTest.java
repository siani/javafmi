/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.proxy.v2;

import org.javafmi.kernel.OS;
import org.javafmi.proxy.FmuFile;
import org.javafmi.proxy.Status;
import org.junit.Ignore;
import org.junit.Test;

import static org.javafmi.proxy.fmus.Index.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assume.assumeTrue;

@Ignore
public class FmuProxyCreationAndDestructionTest {

    public static final boolean STOP_TIME_DEFINED = true;
    public static final boolean LOGGING_ON = true;
    private static final double TIMEOUT = 1000;
    private static final double START_TIME = 0.;
    private static final double STOP_TIME = 10.;
    private static final boolean TOLERANCE_DEFINED = false;

    @Test
    public void createAndDestroyLinux64Instance() {
        assumeTrue(OS.isLinux64());
        FmuFile fmuFile = createFmuFile(Linux64_v2);
        FmuProxy proxy = createV2FmuProxy(fmuFile);
        double tolerance = 0.001;
        String categories[] = {"Severe", "Warning"};

        proxy.instantiate(fmuFile.getResourcesDirectoryPath());
        Status setUpExperimentStatus = proxy.setUpExperiment(TOLERANCE_DEFINED, tolerance, START_TIME, STOP_TIME_DEFINED, STOP_TIME);
        Status enterInitStatus = proxy.enterInitializationMode();
        Status exitInitStatus = proxy.exitInitializationMode();
        Status setDebugStatus = proxy.setDebugLogging(LOGGING_ON, categories);
        Status terminateStatus = proxy.terminate();
        proxy.freeInstance();

        assertEquals(Status.OK, setUpExperimentStatus);
        assertEquals(Status.OK, enterInitStatus);
        assertEquals(Status.OK, exitInitStatus);
        assertEquals(Status.OK, terminateStatus);
        assertEquals(Status.OK, setDebugStatus);
    }
}
