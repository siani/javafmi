/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.proxy.v2;

import org.javafmi.modeldescription.v2.CoSimulation;
import org.javafmi.modeldescription.v2.DefaultExperiment;
import org.javafmi.modeldescription.v2.ModelDescription;
import org.javafmi.proxy.Status;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.anyVararg;
import static org.mockito.Mockito.*;

public class AnFmuProxy {

    private FmuProxy proxy;
    private Fmu library;
    private ModelDescription modelDescription;

    @Before
    public void setUp() {
        this.library = mock(Fmu.class);
        this.modelDescription = mock(ModelDescription.class);
        when(modelDescription.getCoSimulation()).thenReturn(new CoSimulation());

        this.proxy = new FmuProxy(library, modelDescription);
    }

    @Test
    public void calls_to_get_and_set_real_on_library() {
        int valueReference = 3;
        proxy.getReal(valueReference);
        proxy.setReal(new int[]{1, 2}, new double[]{1., 2.});
        verify(library).getReal(valueReference);
        verify(library).setReal(anyVararg(), anyVararg());
    }

    @Test
    public void calls_to_get_and_set_integer_on_library() {
        int valueReference = 3;
        proxy.getInteger(valueReference);
        proxy.setInteger(new int[]{1, 2}, new int[]{1, 2});
        verify(library).getInteger(valueReference);
        verify(library).setInteger(anyVararg(), anyVararg());
    }

    @Test
    public void calls_to_get_and_set_boolean_on_library() {
        int valueReference = 3;
        proxy.getBoolean(valueReference);
        proxy.setBoolean(new int[]{1, 2}, new boolean[]{true, false});
        verify(library).getBoolean(valueReference);
        verify(library).setBoolean(anyVararg(), anyVararg());
    }

    @Test
    public void calls_to_get_and_set_string_on_library() {
        int valueReference = 3;
        proxy.getString(valueReference);
        proxy.setString(new int[]{1, 2}, new String[]{"foo", "bar"});
        verify(library).getString(valueReference);
        verify(library).setString(anyVararg(), anyVararg());
    }

    @Test
    public void doubleInitShouldNotHaveEffect() {
        when(modelDescription.getDefaultExperiment()).thenAnswer(invocationOnMock -> {
            DefaultExperiment answer = mock(DefaultExperiment.class);
            when(answer.getTolerance()).thenReturn(1.);
            return answer;
        });
        proxy.instantiate("");
        Status firstInit = proxy.initialize(0., 1.);
        Status secondInit = proxy.initialize(0., 1.);
        verify(library, times(1)).setUpExperiment(anyBoolean(), anyDouble(), anyDouble(), anyBoolean(), anyDouble());
        verify(library, times(1)).enterInitializationMode();
        verify(library, times(1)).exitInitializationMode();
        assertThat(firstInit, is(Status.OK));
        assertThat(secondInit, is(Status.WARNING));
    }

}
