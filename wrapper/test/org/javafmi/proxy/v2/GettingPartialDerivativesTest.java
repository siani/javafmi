/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.proxy.v2;

import org.javafmi.proxy.FmuFile;
import org.junit.Ignore;
import org.junit.Test;

import static org.javafmi.kernel.OS.isWin32;
import static org.javafmi.kernel.OS.isWin64;
import static org.javafmi.proxy.fmus.Index.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assume.assumeTrue;


public class GettingPartialDerivativesTest {

    @Test
    @Ignore("Ignored because there are no a v2 FMU with this capability")
    public void testGetDirectionalDerivative() {
        assumeTrue(isWin32() || isWin64());
        int[] unknownVariablesValueReferences = new int[1];
        int[] knownVariablesValueReferences = new int[1];
        double[] knownDifferential = new double[1];
        FmuFile fmuFile = createFmuFile(WithDirectionalDerivatives);
        FmuProxy proxy = createV2FmuProxy(fmuFile);
        proxy.instantiate(fmuFile.getResourcesDirectoryPath());
        proxy.enterInitializationMode();
        proxy.exitInitializationMode();
        double[] directionalDerivatives = proxy.getDirectionalDerivative(unknownVariablesValueReferences, knownVariablesValueReferences, knownDifferential);
        proxy.terminate();
        proxy.freeInstance();
        assertEquals(knownDifferential.length, directionalDerivatives.length);
    }
}
