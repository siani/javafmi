/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.proxy.v2.nativeaccess;

import com.sun.jna.Memory;
import com.sun.jna.Pointer;
import org.javafmi.modeldescription.v2.ModelDescription;
import org.javafmi.proxy.v2.CallbackFunctions;
import org.javafmi.proxy.v2.Fmu;
import org.javafmi.proxy.v2.Callbacks;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Ignore;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.javafmi.kernel.OS.*;
import static org.javafmi.proxy.Status.OK;
import static org.javafmi.proxy.Status.PENDING;
import static org.junit.Assert.*;

public @Ignore class FmuTest {

    public static final boolean NO_SET_FMU_STATE_PRIOR_TO_CURRENT_POINT = false;
    private static final String LIBRARY_PATH = "../wrapper/test-res/fmu/cosimulation/v2test/out/";
    private static final String INSTANCE_NAME = "simplefmu";
    private static final String GUID = "simpleGuID";
    private static final String RESOURCES_LOCATION = "file://";
    private static final boolean VISIBLE = false;
    private static final boolean LOGGING_ON = false;
    private static final boolean STOP_TIME_DEFINED = true;
    private static final boolean TOLERANCE_DEFINED = false;
    private static final int DO_STEP_STATUS = 0;
    private static final int PENDING_STATUS = 1;
    private static final int LAST_SUCCESSFUL_TIME = 2;
    private static final int TERMINATED_STATUS = 3;
    private static final int ANY_STATUS = 0;
    private static Map<String, String> extensions;
    private Fmu fmu;

    @BeforeClass
    public static void beforeClass() {
        extensions = new HashMap<>();
        extensions.put(Mac, ".dylib");
        extensions.put(Windows, ".dll");
        extensions.put(Linux, ".so");
    }


    @Before
    public void setUp() throws Exception {
//        fmu = new Fmu(getLibraryPath(), modelDescription());
    }

    private ModelDescription modelDescription() {
        return new ModelDescription();
    }

    private String getLibraryPath() {
        try {
            return new File(LIBRARY_PATH + INSTANCE_NAME + name() + architecture() + extensions.get(name())).getCanonicalPath();
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }

    @After
    public void clean() {
        fmu.free();
    }

    @Test
    public void createsAndDestroysLibrary() {
        try {
            fmu.instantiate(INSTANCE_NAME, Callbacks.FmiType.fmiCoSimulation, GUID, RESOURCES_LOCATION, new CallbackFunctions(), VISIBLE, LOGGING_ON);
            fmu.enterInitializationMode();
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void setDebugLogging() {
//        assertEquals(OK.getCode(), new Fmu(getLibraryPath(), modelDescription()).setDebugLogging(LOGGING_ON, "categories..."));
    }

    @Test
    public void makeCallsRelatedWithInitTerminateAndReset() {
        double startTime = 0.;
        double stopTime = 1.;
        double tolerance = 1E-6;
        assertEquals(OK.getCode(), fmu.setUpExperiment(TOLERANCE_DEFINED, tolerance, startTime, STOP_TIME_DEFINED, stopTime));
        assertEquals(OK.getCode(), fmu.enterInitializationMode());
        assertEquals(OK.getCode(), fmu.exitInitializationMode());
        assertEquals(OK.getCode(), fmu.terminate());
        assertEquals(OK.getCode(), fmu.reset());
    }

    @Test
    public void handleRealOperations() {
        int[] valueReferences = new int[]{10, 11};
        double[] doubleValues = new double[]{3.141592, 2.71828};
        assertEquals(OK.getCode(), fmu.setReal(valueReferences, doubleValues));
        assertEquals(doubleValues[0], fmu.getReal(valueReferences)[0], 0.01);
        assertEquals(doubleValues[1], fmu.getReal(valueReferences)[1], 0.01);
    }

    @Test
    public void handleIntegerOperations() {
        int[] valueReferences = new int[]{20, 21, 22};
        int[] integerValues = new int[]{1989, 9, 3};
        assertEquals(OK.getCode(), fmu.setInteger(valueReferences, integerValues));
        assertEquals(integerValues[0], fmu.getInteger(valueReferences)[0]);
        assertEquals(integerValues[1], fmu.getInteger(valueReferences)[1]);
        assertEquals(integerValues[2], fmu.getInteger(valueReferences)[2]);
    }

    @Test
    public void handleBooleanOperations() {
        int[] valueReferences = new int[]{30, 31, 32};
        boolean[] booleanValues = new boolean[]{false, true, false};
        assertEquals(OK.getCode(), fmu.setBoolean(valueReferences, booleanValues));
        assertEquals(booleanValues[0], fmu.getBoolean(valueReferences)[0]);
        assertEquals(booleanValues[1], fmu.getBoolean(valueReferences)[1]);
        assertEquals(booleanValues[2], fmu.getBoolean(valueReferences)[2]);
    }

    @Test
    public void handleStringOperations() {
        int[] valueReferences = new int[]{40, 41, 42};
        String[] stringValues = new String[]{"string", "variables", "can be written"};
        assertEquals(OK.getCode(), fmu.setString(valueReferences, stringValues));
        assertEquals(stringValues[0], fmu.getString(valueReferences)[0]);
        assertEquals(stringValues[1], fmu.getString(valueReferences)[1]);
        assertEquals(stringValues[2], fmu.getString(valueReferences)[2]);
    }

    @Test
    public void callsToGetStateFunctions() {
        Pointer firstState = fmu.getFmuState(null);
        Pointer secondState = fmu.getFmuState(firstState);
//TODO        assertEquals(firstState, secondState);
        assertEquals(OK.getCode(), fmu.setFmuState(secondState));
        assertEquals(OK.getCode(), fmu.freeFmuState(secondState));
    }

    @Test
    public void callsToSerializeStateFunctions() {
        Pointer fmuState = fmu.getFmuState(new Memory(Pointer.SIZE));
        byte[] serializedState = fmu.serializeFmuState(fmuState);
        assertEquals(fmu.serializedFmuStateSize(fmuState), serializedState.length);
        assertNotNull(fmu.deSerializeFMUState(serializedState));
    }

    @Test
    public void callsToGetDirectionalDerivative() {
        int[] unknowns = new int[]{1, 2};
        int[] known = new int[]{3, 4};
        double[] knownDerivatives = new double[]{11.2, 3.56};
        double[] directionalDerivative = fmu.getDirectionalDerivative(unknowns, known, knownDerivatives);
        assertEquals(2, directionalDerivative.length);
    }

    @Test
    public void callsToSetRelInputDerivatives() {
        int[] valueReferences = new int[]{20, 21, 22};
        int[] orders = new int[]{1, 2, 1};
        double[] values = new double[]{3.1, 0.2, 0.};
        assertEquals(OK.getCode(), fmu.setRealInputDerivatives(valueReferences, orders, values));
    }

    @Test
    public void callsToGetRealOutputDerivative() {
        int[] valueReferences = new int[]{20, 21, 22};
        int[] orders = new int[]{1, 2, 1};
        assertEquals(3, fmu.getRealOutputDerivatives(valueReferences, orders).length);
    }

    @Test
    public void callsToDoStep() {
        double currentCommunicationPoint = 1.3;
        double communicationStepSize = 0.2;
        assertEquals(OK.getCode(), fmu.doStep(currentCommunicationPoint, communicationStepSize, NO_SET_FMU_STATE_PRIOR_TO_CURRENT_POINT));
    }

    @Test
    public void cancelStep() {
        assertEquals(OK.getCode(), fmu.cancelStep());
    }

    @Test
    public void retrievesStatus() {
        assertEquals(PENDING.getCode(), fmu.getStatus(DO_STEP_STATUS));
        assertEquals(0., fmu.getRealStatus(LAST_SUCCESSFUL_TIME), 0.01);
        assertEquals(1, fmu.getIntegerStatus(ANY_STATUS));
        assertEquals(false, fmu.getBooleanStatus(TERMINATED_STATUS));
        assertEquals("fake status", fmu.getStringStatus(PENDING_STATUS));
    }
}
