/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.wrapper.v2;

import org.javafmi.wrapper.Simulation;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.javafmi.kernel.OS.isWin64;
import static org.junit.Assume.assumeTrue;

@Ignore
public class Simulation_ {

    @Test
    public void opening_more_than_125_instances_using_one_fmu() throws Exception {
        assumeTrue(isWin64());
        List<Simulation> simulations = new ArrayList<>();
        try {
            for (int i = 0; i < 125; i++) {
                Simulation simulation = new Simulation(fileOf("/fmu/cosimulation/v2test/stochasticHeatedRoomwin3264.fmu"));
                simulation.init(0);
                simulations.add(simulation);
            }
        } catch (Throwable e) {
            assertFalse(true);
        }
    }

    @Test
    public void different_instances_of_same_dll_keeps_different_contexts() throws Exception {
        assumeTrue(isWin64());
        List<Simulation> simulations = new ArrayList<>();
        try {
            for (int i = 0; i < 2; i++) {
                Simulation simulation = new Simulation(fileOf("/fmu/cosimulation/v2test/stochasticHeatedRoomwin3264.fmu"));
                simulation.init(0);
                simulations.add(simulation);
            }
            assertEquals(17.0, simulations.get(0).read("heaterController.temperatureInput").asDouble());
            simulations.get(0).write("heaterController.temperatureInput").with(18.0);
            assertEquals(18.0, simulations.get(0).read("heaterController.temperatureInput").asDouble());
            assertEquals(17.0, simulations.get(1).read("heaterController.temperatureInput").asDouble());
            simulations.get(1).write("heaterController.temperatureInput").with(18.0);
            assertEquals(18.0, simulations.get(1).read("heaterController.temperatureInput").asDouble());
            assertEquals(18.0, simulations.get(0).read("heaterController.temperatureInput").asDouble());
        } catch (Throwable e) {
            assertFalse(true);
        }
    }

    private String fileOf(String path) {
        return Simulation_.class.getResource(path).getFile();
    }
}