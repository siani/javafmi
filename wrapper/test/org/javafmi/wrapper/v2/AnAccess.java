/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.wrapper.v2;


import org.javafmi.modeldescription.v2.ModelDescription;
import org.javafmi.modeldescription.v2.ScalarVariable;
import org.javafmi.proxy.Status;
import org.javafmi.proxy.v2.FmuProxy;
import org.javafmi.wrapper.Simulation;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.javafmi.modeldescription.v2.Capabilities.*;
import static org.mockito.Mockito.*;

public class AnAccess {

    private final int[] valueReferences = new int[]{1, 2, 3};
    private final double[] doubleValues = new double[]{1., 2., 3.};
    private final int[] intValues = {1, 2, 3};
    private final boolean[] booleanValues = {false, true, false};
    private final String[] stringValues = {"foo", "bar", "foobar"};
    private FmuProxy proxy;
    private Access access;
    private ModelDescription modelDescription;

    @Before
    public void setUp() throws Exception {
        proxy = mock(FmuProxy.class);
        modelDescription = mock(ModelDescription.class);
        when(modelDescription.getModelVariables()).thenReturn(new ScalarVariable[0]);
        access = new Access(proxy, modelDescription, null);
    }

    @Test
    public void should_provide_the_model_description() {
        ModelDescription result = access.getModelDescription();
        assertThat(result, is(modelDescription));
    }

    @Test
    public void should_provide_the_model_variables() {
        ScalarVariable[] result = access.getModelVariables();
        verify(modelDescription).getModelVariables();
    }

    @Test
    public void should_provide_a_model_variable_by_name() {
        String varName = "variable.name";
        ScalarVariable variable = access.getModelVariable(varName);
        verify(modelDescription).getModelVariable(varName);
    }

    @Test
    public void should_check_model_capabilities() {
        access.check(NeedsExecutionTool);
        access.check(CanHandleVariableCommunicationStepSize);
        access.check(CanInterpolateInputs);
        access.check(CanRunAsynchronuously);
        access.check(CanBeInstantiatedOnlyOncePerProcess);
        access.check(CanNotUseMemoryManagementFunctions);
        access.check(CanGetAndSetFMUState);
        access.check(CanSerializeFMUState);
        access.check(ProvidesDirectionalDerivative);

        verify(modelDescription).check(NeedsExecutionTool);
        verify(modelDescription).check(CanHandleVariableCommunicationStepSize);
        verify(modelDescription).check(CanInterpolateInputs);
        verify(modelDescription).check(CanRunAsynchronuously);
        verify(modelDescription).check(CanBeInstantiatedOnlyOncePerProcess);
        verify(modelDescription).check(CanNotUseMemoryManagementFunctions);
        verify(modelDescription).check(CanGetAndSetFMUState);
        verify(modelDescription).check(CanSerializeFMUState);
        verify(modelDescription).check(ProvidesDirectionalDerivative);
    }

    @Test
    public void can_be_created_from_a_simulation() throws Exception {
        Simulation simulation = mock(Simulation.class);
        Field simulationProxy = Simulation.class.getDeclaredField("proxy");
        simulationProxy.setAccessible(true);
        simulationProxy.set(simulation, proxy);
        when(simulation.getModelDescription()).thenReturn(modelDescription);

        new Access(simulation);

        verify(simulation).getFmuFile();
        verify(simulation).getModelDescription();
    }

    @Test
    public void is_an_fmu_view() {
        assertThat(new Access(proxy, modelDescription, null), instanceOf(FmuView.class));
    }

    @Test
    public void can_read_and_write_doubles_in_batch() {
        when(proxy.setReal(valueReferences, doubleValues)).thenReturn(Status.OK);
        when(proxy.getReal(valueReferences)).thenReturn(doubleValues);

        assertThat(access.setReal(valueReferences, doubleValues), is(Status.OK));
        assertThat(access.getReal(valueReferences).length, is(valueReferences.length));

        verify(proxy).setReal(valueReferences, doubleValues);
        verify(proxy).getReal(valueReferences);
    }

    @Test
    public void can_read_and_write_integers_in_batch() {
        when(proxy.setInteger(valueReferences, intValues)).thenReturn(Status.OK);
        when(proxy.getInteger(valueReferences)).thenReturn(intValues);

        assertThat(access.setInteger(valueReferences, intValues), is(Status.OK));
        assertThat(access.getInteger(valueReferences).length, is(valueReferences.length));

        verify(proxy).setInteger(valueReferences, intValues);
        verify(proxy).getInteger(valueReferences);
    }

    @Test
    public void can_read_and_write_booleans_in_batch() {
        when(proxy.setBoolean(valueReferences, booleanValues)).thenReturn(Status.OK);
        when(proxy.getBoolean(valueReferences)).thenReturn(booleanValues);

        assertThat(access.setBoolean(valueReferences, booleanValues), is(Status.OK));
        assertThat(access.getBoolean(valueReferences).length, is(valueReferences.length));

        verify(proxy).setBoolean(valueReferences, booleanValues);
        verify(proxy).getBoolean(valueReferences);
    }

    @Test
    public void can_read_and_write_strings_in_batch() {
        when(proxy.setString(valueReferences, stringValues)).thenReturn(Status.OK);
        when(proxy.getString(valueReferences)).thenReturn(stringValues);

        assertThat(access.setString(valueReferences, stringValues), is(Status.OK));
        assertThat(access.getString(valueReferences).length, is(valueReferences.length));

        verify(proxy).setString(valueReferences, stringValues);
        verify(proxy).getString(valueReferences);
    }

    @Test
    public void provides_the_directional_derivative() {
        int[] unknownValueReferences = {1, 2, 3};
        int[] knownValueReferences = {4, 5, 6};
        double[] knownDerivative = {1., 2., 1.};
        double[] result = {0.1, 0.2, 0.3};
        when(proxy.getDirectionalDerivative(unknownValueReferences, knownValueReferences, knownDerivative)).thenReturn(result);
        assertThat(access.getDirectionalDerivative(unknownValueReferences, knownValueReferences, knownDerivative),
                is(result));
        verify(proxy).getDirectionalDerivative(unknownValueReferences, knownValueReferences, knownDerivative);
    }
}
