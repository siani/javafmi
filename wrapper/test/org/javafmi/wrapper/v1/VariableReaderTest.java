/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.wrapper.v1;

import org.javafmi.modeldescription.ModelDescription;
import org.javafmi.modeldescription.v1.*;
import org.javafmi.proxy.FmiProxy;
import org.javafmi.wrapper.variables.VariableReader;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.*;

public class VariableReaderTest {

    private VariableReader reader;
    private FmiProxy mockProxy;
    private ModelDescription mockedModelDescription;

    @Before
    public void setUp() {
        mockProxy = mock(org.javafmi.proxy.v1.FmuProxy.class);
        mockedModelDescription = mock(ModelDescription.class);
        reader = new VariableReader(mockProxy, mockedModelDescription);
    }

    @Test
    public void readingARealCallsToGetRealInProxy() {
        String realVariableName = "";
        int[] realVariableValueReference = new int[]{0};
        configureRealTest(new RealVariable(realVariableName).withValueReference(realVariableValueReference[0]));
        verify(mockProxy).getReal(realVariableValueReference);
    }

    @Test
    public void readingAnIntegerCallsToGetIntegerInProxy() {
        String intVariableName = "";
        Integer intVariableValueReference = 0;
        configureIntTest(new IntegerVariable(intVariableName).withValueReference(intVariableValueReference));
        verify(mockProxy).getInteger(intVariableValueReference);
    }

    @Test
    public void readingABooleanCallsToGetBooleanInProxy() {
        String booleanVariableName = "";
        Integer booleanVariableValueReference = 0;
        configureBooleanTest(new BooleanVariable(booleanVariableName).withValueReference(booleanVariableValueReference));
        verify(mockProxy).getBoolean(booleanVariableValueReference);
    }

    @Test
    public void readingAStringCallsToGetStringInProxy() {
        String stringVariableName = "";
        Integer stringVariableValueReference = 0;
        configureStringTest(new StringVariable(stringVariableName).withValueReference(stringVariableValueReference));
        verify(mockProxy).getString(stringVariableValueReference);
    }

    private void configureRealTest(ScalarVariable variable) {
        when(mockProxy.getReal(anyInt())).thenReturn(new double[]{anyDouble()});
        configure(variable);
    }

    private void configureIntTest(ScalarVariable variable) {
        when(mockProxy.getInteger(anyInt())).thenReturn(new int[]{anyInt()});
        configure(variable);
    }

    private void configureBooleanTest(ScalarVariable variable) {
        when(mockProxy.getBoolean(anyInt())).thenReturn(new boolean[]{anyBoolean()});
        configure(variable);
    }

    private void configureStringTest(ScalarVariable variable) {
        when(mockProxy.getString(anyInt())).thenReturn(new String[]{anyString()});
        configure(variable);
    }

    private void configure(ScalarVariable variable) {
        when(mockedModelDescription.getModelVariable(variable.getName())).thenReturn(variable);
        mockProxy.initialize(0, 0);
        reader.read(variable.getName());
    }

}
