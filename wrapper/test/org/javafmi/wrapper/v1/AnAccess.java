/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.wrapper.v1;


import org.javafmi.modeldescription.v1.ModelDescription;
import org.javafmi.modeldescription.v1.ScalarVariable;
import org.javafmi.proxy.Status;
import org.javafmi.proxy.v1.FmuProxy;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.*;

public class AnAccess {

    private final int[] valueReferences = new int[]{1, 2, 3};
    private final double[] doubleValues = new double[]{1., 2., 3.};
    private final int[] intValues = {1, 2, 3};
    private final boolean[] booleanValues = {false, true, false};
    private final String[] stringValues = {"foo", "bar", "foobar"};
    private FmuProxy proxy;
    private Access access;
    private ModelDescription modelDescription;

    @Before
    public void setUp() throws Exception {
        proxy = mock(FmuProxy.class);
        modelDescription = mock(ModelDescription.class);
        when(modelDescription.getModelVariables()).thenReturn(new ScalarVariable[0]);
        access = new Access(proxy, modelDescription, null);
    }

    @Test
    public void is_an_fmu_view() {
        assertThat(new Access(proxy, modelDescription, null), instanceOf(FmuView.class));
    }

    @Test
    public void can_read_and_write_real_variables() {
        when(proxy.setReal(valueReferences, doubleValues)).thenReturn(Status.OK);
        when(proxy.getReal(valueReferences)).thenReturn(doubleValues);

        assertThat(access.setReal(valueReferences, doubleValues), is(Status.OK));
        assertThat(access.getReal(valueReferences).length, is(valueReferences.length));

        verify(proxy).setReal(valueReferences, doubleValues);
        verify(proxy).getReal(valueReferences);
    }

    @Test
    public void can_read_and_write_integer_variables() {
        when(proxy.setInteger(valueReferences, intValues)).thenReturn(Status.OK);
        when(proxy.getInteger(valueReferences)).thenReturn(intValues);

        assertThat(access.setInteger(valueReferences, intValues), is(Status.OK));
        assertThat(access.getInteger(valueReferences).length, is(valueReferences.length));

        verify(proxy).setInteger(valueReferences, intValues);
        verify(proxy).getInteger(valueReferences);
    }

    @Test
    public void can_read_and_write_boolean_variables() {
        when(proxy.setBoolean(valueReferences, booleanValues)).thenReturn(Status.OK);
        when(proxy.getBoolean(valueReferences)).thenReturn(booleanValues);

        assertThat(access.setBoolean(valueReferences, booleanValues), is(Status.OK));
        assertThat(access.getBoolean(valueReferences).length, is(valueReferences.length));

        verify(proxy).setBoolean(valueReferences, booleanValues);
        verify(proxy).getBoolean(valueReferences);
    }

    @Test
    public void can_read_and_write_string_variables() {
        when(proxy.setString(valueReferences, stringValues)).thenReturn(Status.OK);
        when(proxy.getString(valueReferences)).thenReturn(stringValues);

        assertThat(access.setString(valueReferences, stringValues), is(Status.OK));
        assertThat(access.getString(valueReferences).length, is(valueReferences.length));

        verify(proxy).setString(valueReferences, stringValues);
        verify(proxy).getString(valueReferences);
    }

    @Test
    public void should_provide_the_model_description() {
        ModelDescription result = access.getModelDescription();
        assertThat(result, is(modelDescription));
    }

    @Test
    public void should_provide_the_model_variables() {
        ScalarVariable[] result = access.getModelVariables();
        verify(modelDescription).getModelVariables();
    }

    @Test
    public void should_provide_a_model_variable_by_name() {
        String varName = "variable.name";
        ScalarVariable variable = access.getModelVariable(varName);
        verify(modelDescription).getModelVariable(varName);
    }
}
