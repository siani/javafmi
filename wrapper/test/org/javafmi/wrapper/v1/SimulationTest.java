/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.wrapper.v1;

import org.javafmi.proxy.Status;
import org.javafmi.wrapper.Simulation;
import org.junit.Assert;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static org.javafmi.kernel.OS.isLinux32;
import static org.javafmi.kernel.OS.isWin32;
import static org.javafmi.kernel.OS.isWin64;
import static org.javafmi.wrapper.CommonsWrapper.*;
import static org.junit.Assert.*;
import static org.junit.Assume.assumeTrue;

public class SimulationTest {

    private static final double StartTime = 0.0;
    private static final double StopTime = 50.0;
    private static final double TimeStep = 1.0;

    @Test
    public void doubleInitShouldNotHaveEffect() {
        Simulation simulation = createSimulation(getFmuNameForThisOS());
        Status firstInitStatus = simulation.init(StartTime);
        Status secondInitStatus = simulation.init(StartTime);
        simulation.terminate();
        Assert.assertEquals(Status.OK, firstInitStatus);
        Assert.assertEquals(Status.WARNING, secondInitStatus);
    }

    @Test
    public void testReset() {
        Simulation simulation = createSimulation(getFmuNameForThisOS());
        Status firstInitStatus = simulation.init(StartTime);
        Status doStepStatus = simulation.doStep(TimeStep);
        double firstSimulationTime = simulation.getCurrentTime();
        Status secondDoStepStatus = simulation.doStep(TimeStep);
        double secondSimulationTime = simulation.getCurrentTime();
        Status resetStatus = simulation.reset();
        double thirdSimulationTime = simulation.getCurrentTime();
        Status secondInit = simulation.init(StartTime, StopTime);
        double fourthSimulationTime = simulation.getCurrentTime();
        Status terminateStatus = simulation.terminate();

        assertEquals(Status.OK, firstInitStatus);
        assertEquals(Status.OK, doStepStatus);
        assertEquals(TimeStep, firstSimulationTime, 0.1);
        assertEquals(Status.OK, secondDoStepStatus);
        assertEquals(TimeStep + TimeStep, secondSimulationTime, 0.1);
        assertEquals(Status.OK, resetStatus);
        assertEquals(0.0, thirdSimulationTime, 0.1);
        assertEquals(Status.OK, secondInit);
        assertEquals(0.0, fourthSimulationTime, 0.1);
        assertEquals(Status.OK, terminateStatus);
    }

    @Test
    public void testCoSimulationStepsForWindowsFmu() {
        assumeTrue(isWin32());
        Simulation simulation = createSimulation(WINDOWS_32_V1_FMU);
        Status initStatus = simulation.init(StartTime, StopTime);
        Status doStepStatus = simulation.doStep(TimeStep);
        Status writeVariableStatus = simulation.writeVariable("Pd", 0.3);
        Status doStepStatus2 = simulation.doStep(TimeStep);
        double pdValue = (double) simulation.readVariable("Pd").getValue();
        List<Status> doStepOnTimeStatuses = new LinkedList<>();
        for (int i = 2; i < 50; i++)
            doStepOnTimeStatuses.add(simulation.doStep(TimeStep));
        List<Status> doStepOutOfTimeStatuses = new LinkedList<>();
        for (int i = 0; i < 10; i++)
            doStepOutOfTimeStatuses.add(simulation.doStep(TimeStep));
        Status terminateStatus = simulation.terminate();

        assertEquals(Status.OK, initStatus);
        assertEquals(Status.OK, doStepStatus);
        assertEquals(Status.OK, writeVariableStatus);
        assertEquals(Status.OK, doStepStatus2);
        assertEquals(0.3, pdValue, 0.01);
        for (Status doStepOnTimeStatus : doStepOnTimeStatuses)
            assertEquals(Status.OK, doStepOnTimeStatus);
        for (Status doStepOutOfTimeStatus : doStepOutOfTimeStatuses)
            assertNotSame(Status.OK, doStepOutOfTimeStatus);
        assertEquals(Status.OK, terminateStatus);
    }

    @Test
    public void testCoSimulationStepsForLinuxFmu() {
        assumeTrue(isLinux32());
        Simulation simulation = createSimulation(LINUX_32_FMU);
        Status initStatus = simulation.init(StartTime, StopTime);
        Status doStepStatus1 = simulation.doStep(TimeStep);
        String writableVariableName = "plusun_int_in";
        Status writeVariableStatus = simulation.writeVariable(writableVariableName, 4);
        Status doStepStatus2 = simulation.doStep(TimeStep);
        int variableValue = (int) simulation.readVariable(writableVariableName).getValue();
        List<Status> doStepOnTimeStatuses = new LinkedList<>();
        for (int i = 2; i < 50; i++)
            doStepOnTimeStatuses.add(simulation.doStep(TimeStep));
        List<Status> doStepOutOfTimeStatuses = new LinkedList<>();
        for (int i = 0; i < 10; i++)
            doStepOnTimeStatuses.add(simulation.doStep(TimeStep));
        Status terminateStatus = simulation.terminate();

        assertEquals(Status.OK, initStatus);
        assertEquals(Status.OK, doStepStatus1);
        assertEquals(Status.OK, writeVariableStatus);
        assertEquals(Status.OK, doStepStatus2);
        assertEquals(4, variableValue);
        for (Status doStepStatus : doStepOnTimeStatuses)
            assertEquals(Status.OK, doStepStatus);
        for (Status doStepOutOfTimeStatus : doStepOutOfTimeStatuses)
            assertTrue(doStepOutOfTimeStatus == simulation.doStep(TimeStep));
        assertEquals(Status.OK, terminateStatus);
    }

    @Test
    public void readEnumerationFromWin32Fmu() {
        assumeTrue(isWin32());
        String enumerationVariable = "regulation_vitesse.limIntegrator.initType";
        String enumeratedExpectedValue = "InitialOutput";
        double startTime = 0.0;
        Simulation simulation = createSimulation(WINDOWS_32_ENUMERATION_READABLE_FMU);
        String enumerationValue = simulation.readVariable(enumerationVariable).getValue().toString();
        Status initStatus = simulation.init(startTime);
        String enumerationValueAfterInit = simulation.readVariable(enumerationVariable).getValue().toString();
        Status terminateStatus = simulation.terminate();

        assertEquals(enumeratedExpectedValue, enumerationValue);
        assertEquals(Status.OK, initStatus);
        assertEquals(enumeratedExpectedValue, enumerationValueAfterInit);
        assertEquals(Status.OK, terminateStatus);
    }

    @Test
    public void readVanDerPolFmu(){
        assumeTrue(isWin32() || isWin64());
        Simulation simulation = createSimulation("vanDerPol2.fmu");
        simulation.init(0);
        System.out.println(simulation.read("x0").asDouble());
        simulation.doStep(1);
        System.out.println(simulation.read("x0").asDouble());
    }

}
