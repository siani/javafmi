/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.wrapper;

import org.javafmi.modeldescription.v2.ModelDescription;
import org.javafmi.proxy.FmiProxy;
import org.javafmi.proxy.Status;
import org.javafmi.wrapper.Simulation;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.javafmi.wrapper.CommonsWrapper.createSimulation;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class Simulation_ {

    private static final double START_TIME = 0.0;
    private static final double TIME_STEP = 0.01;
    private static final double STOP_TIME = 10.0;
    private final String name = "name";
    private final int valueReference = 1;
    private final String[] names = {"name1", "name2"};
    private final int[] valueReferences = {1, 2};
    private final int[] intValues = {1, 2};
    private final boolean[] boolValues = new boolean[]{false, true};
    private final String[] stringValues = new String[]{"value1", "value2"};
    private FmiProxy proxy;
    private Simulation simulation;
    private double[] doubleValues = {1., 2.};

    @Before
    public void setUp() throws Exception {
        proxy = mock(FmiProxy.class);
        ModelDescription modelDescription = mock(ModelDescription.class);
        simulation = new Simulation();
        inject(modelDescription, simulation, "modelDescription");
        inject(proxy, simulation, "proxy");
        when(modelDescription.getValueReferences(names)).thenReturn(valueReferences);
        when(modelDescription.getValueReference(name)).thenReturn(valueReference);
    }

    private void inject(Object object, Simulation simulation, String fieldName) throws Exception {
        Field declaredField = Simulation.class.getDeclaredField(fieldName);
        declaredField.setAccessible(true);
        declaredField.set(simulation, object);
    }

    @Test
    public void can_write_and_read_double_variables_one_by_one() {
        double doubleValue = 3.14;
        when(proxy.getReal(valueReference)).thenReturn(new double[]{doubleValue});
        when(proxy.setReal(anyVararg(), anyVararg())).thenReturn(Status.OK);

        assertThat(simulation.write(name).with(doubleValue), is(Status.OK));
        assertThat(simulation.read(name).asDouble(), is(3.14));

        verify(proxy).getReal(valueReference);
        verify(proxy).setReal(anyVararg(), anyVararg());
    }

    @Test
    public void can_write_and_read_integer_variables_one_by_one() {
        int intValue = 1;
        when(proxy.getInteger(valueReference)).thenReturn(new int[]{intValue});
        when(proxy.setInteger(anyVararg(), anyVararg())).thenReturn(Status.OK);

        assertThat(simulation.write(name).with(intValue), is(Status.OK));
        assertThat(simulation.read(name).asInteger(), is(intValue));

        verify(proxy).getInteger(valueReference);
        verify(proxy).setInteger(anyVararg(), anyVararg());
    }

    @Test
    public void can_write_and_read_boolean_variables_one_by_one() {
        when(proxy.getBoolean(valueReference)).thenReturn(new boolean[]{false});
        when(proxy.setBoolean(anyVararg(), anyVararg())).thenReturn(Status.OK);

        boolean boolValue = false;
        assertThat(simulation.write(name).with(boolValue), is(Status.OK));
        assertThat(simulation.read(name).asBoolean(), is(false));

        verify(proxy).getBoolean(valueReference);
        verify(proxy).setBoolean(anyVararg(), anyVararg());
    }

    @Test
    public void can_write_and_read_string_variables_one_by_one() {
        String stringValue = "value";
        when(proxy.getString(valueReference)).thenReturn(new String[]{stringValue});
        when(proxy.setString(anyVararg(), anyVararg())).thenReturn(Status.OK);

        assertThat(simulation.write(name).with(stringValue), is(Status.OK));
        assertThat(simulation.read(name).asString(), is(stringValue));

        verify(proxy).getString(valueReference);
        verify(proxy).setString(anyVararg(), anyVararg());
    }

    @Test
    public void can_write_and_read_double_variables_in_batch() {
        when(proxy.setReal(valueReferences, doubleValues)).thenReturn(Status.OK);
        when(proxy.getReal(valueReferences)).thenReturn(doubleValues);

        assertThat(simulation.write(names).with(doubleValues), is(Status.OK));
        assertThat(simulation.read(names).asDoubles(), is(doubleValues));

        verify(proxy).setReal(valueReferences, doubleValues);
        verify(proxy).getReal(valueReferences);
    }

    @Test
    public void can_write_and_read_integer_variables_in_batch() {
        when(proxy.setInteger(valueReferences, intValues)).thenReturn(Status.OK);
        when(proxy.getInteger(valueReferences)).thenReturn(intValues);

        assertThat(simulation.write(names).with(intValues), is(Status.OK));
        assertThat(simulation.read(names).asInteger().length, is(valueReferences.length));
        verify(proxy).setInteger(valueReferences, intValues);
        verify(proxy).getInteger(valueReferences);
    }

    @Test
    public void can_write_and_read_boolean_variables_in_batch() {
        when(proxy.setBoolean(valueReferences, boolValues)).thenReturn(Status.OK);
        when(proxy.getBoolean(valueReferences)).thenReturn(boolValues);

        assertThat(simulation.write(names).with(boolValues), is(Status.OK));
        assertThat(simulation.read(names).AsBoolean().length, is(valueReferences.length));

        verify(proxy).setBoolean(valueReferences, boolValues);
        verify(proxy).getBoolean(valueReferences);
    }

    @Test
    public void can_write_and_read_string_variables_in_batch() {
        when(proxy.setString(valueReferences, stringValues)).thenReturn(Status.OK);
        when(proxy.getString(valueReferences)).thenReturn(stringValues);

        assertThat(simulation.write(names).with(stringValues), is(Status.OK));
        assertThat(simulation.read(names).AsString().length, is(valueReferences.length));

        verify(proxy).setString(valueReferences, stringValues);
        verify(proxy).getString(valueReferences);
    }

    public void testReset() {
        Simulation simulation = createSimulation("");
        Status firstInitStatus = simulation.init(START_TIME);
        double firstSimulationTime = simulation.getCurrentTime();
        Status resetStatus = simulation.reset();
        Status secondInitStatus = simulation.init(START_TIME, STOP_TIME);
        Status terminateStatus = simulation.terminate();
        assertEquals(Status.OK, firstInitStatus);
        assertEquals(TIME_STEP, firstSimulationTime, 0.1);
        assertEquals(Status.OK, resetStatus);
        assertEquals(Status.OK, secondInitStatus);
        assertEquals(Status.OK, terminateStatus);
    }

}
