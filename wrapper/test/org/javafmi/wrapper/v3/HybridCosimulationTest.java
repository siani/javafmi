/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.wrapper.v3;

import org.javafmi.modeldescription.v2.ModelDescription;
import org.javafmi.proxy.FmiProxyV3;
import org.javafmi.proxy.FmiProxyV3.HybridDoStepStatus;
import org.javafmi.proxy.v2.State;
import org.javafmi.wrapper.Simulation;
import org.junit.Ignore;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;
import static org.javafmi.kernel.OS.isWin64;
import static org.junit.Assume.assumeTrue;

@Ignore
public class HybridCosimulationTest {

	static int C1 = 0, TANK = 2, BARREL = 3, C2 = 1;

	@Test
	public void hybrid_test_using_c1() throws Exception {
		assumeTrue(isWin64());
		Simulation simulation = simOf("/fmu/cosimulation/v3/C1.fmu");
		Access access = new Access(simulation);
		simulation.init(0);

		double currentTime = 0;
		for (int step = 0; step < 10; step++) {
			System.out.println("Next event will be at: " + access.getNextEventTime());
			HybridDoStepStatus hybridDoStepStatus = access.hybridDoStep(currentTime, 1);
			currentTime = hybridDoStepStatus.getEndTime();
			System.out.print("HybridDoStep status --> " + hybridDoStepStatus.getStatus());
			System.out.print("\toccurredEvent --> " + hybridDoStepStatus.isOccurredEvent());
			System.out.print("\tendTime --> " + hybridDoStepStatus.getEndTime());
			if (hybridDoStepStatus.isOccurredEvent()) {
				FmiProxyV3.RealsAtEvent valve = access.getRealAtEvent(currentTime, simulation.getModelDescription().getValueReference("Valve"));
				System.out.print("\tValve before event --> " + valve.getBeforeEvent()[0]);
				System.out.println("\tValve after event --> " + valve.getAfterEvent()[0]);
			} else
				System.out.println();
		}

		simulation.terminate();
	}

	@Test
	public void hybrid_test_using_c1_with_more_variables() throws Exception {
		assumeTrue(isWin64());
		Simulation simulation = simOf("/fmu/cosimulation/v3/C1MoreVariables.fmu");
		Access access = new Access(simulation);
		simulation.init(0);

		double currentTime = 0;
		for (int step = 0; step < 10; step++) {
			System.out.println("Next event will be at: " + access.getNextEventTime());
			HybridDoStepStatus hybridDoStepStatus = access.hybridDoStep(currentTime, 1);
			currentTime = hybridDoStepStatus.getEndTime();
			System.out.print("HybridDoStep status --> " + hybridDoStepStatus.getStatus());
			System.out.print("\toccurredEvent --> " + hybridDoStepStatus.isOccurredEvent());
			System.out.print("\tendTime --> " + hybridDoStepStatus.getEndTime());
			if (hybridDoStepStatus.isOccurredEvent()) {
				System.out.print("\tbefore values(valve=" + simulation.read("Valve").asBoolean() + ", size=" +
						simulation.read("Size").asInteger() + ", level=" + simulation.read("Level").asDouble() + ")");
				access.hybridDoStep(currentTime, 0);
				System.out.println("\tafter values(valve=" + simulation.read("Valve").asBoolean() + ", size=" +
						simulation.read("Size").asInteger() + ", level=" + simulation.read("Level").asDouble() + ")");
			} else
				System.out.println();
		}
		simulation.terminate();
	}

	@Test
	public void newDiscreteStates() throws Exception {
		assumeTrue(isWin64());
		Simulation simulation = simOf("/fmu/cosimulation/v3/C1NewDiscreteEvents.fmu");
		Access access = new Access(simulation);
		simulation.init(0);

		double currentTime = 0;
		for (int step = 0; step < 10; step++) {
			System.out.println("Next event will be at: " + access.getNextEventTime());
			HybridDoStepStatus hybridDoStepStatus = access.hybridDoStep(currentTime, 1);
			//access.newDiscreteStates(info);
			currentTime = hybridDoStepStatus.getEndTime();
			System.out.print("HybridDoStep status --> " + hybridDoStepStatus.getStatus());
			System.out.print("\toccurredEvent --> " + hybridDoStepStatus.isOccurredEvent());
			System.out.println("\tendTime --> " + hybridDoStepStatus.getEndTime());

			if (hybridDoStepStatus.isOccurredEvent()) {
				FmiProxyV3.EventInfo info = new FmiProxyV3.EventInfo(true, false, false, false, false, 0.0);
				while (info.newDiscreteStatesNeeded) {
					System.out.print("\tValve before value --> " + simulation.read("Valve").asDouble());
					access.newDiscreteStates(info);
					System.out.print("\tValve after value --> " + simulation.read("Valve").asDouble());
					System.out.println("\tNext event time--> " + info.nextEventTime());
				}
			}
		}
		simulation.terminate();
	}

	@Test
	public void newDiscreteStatesWithStatesManagement() throws Exception {
		assumeTrue(isWin64());
		Simulation simulation = simOf("/fmu/cosimulation/v3/C1NewDiscreteEvents.fmu");
		Access access = new Access(simulation);
		simulation.init(0);
		double fixedStepSize = 1;
		double stepSize = 1;
		double currentTime = 0;
		State state = access.getState(State.newEmptyState());
		while (currentTime < 10) {
			HybridDoStepStatus status = access.hybridDoStep(currentTime, stepSize);
			if (status.isOccurredEvent()) {
				if (status.getEndTime() < currentTime + stepSize) {
					access.setState(state);
					stepSize = status.getEndTime() - currentTime;
					System.out.println("Rolling back at " + status.getEndTime());
				} else {
					currentTime = status.getEndTime();
					stepSize = fixedStepSize - stepSize;
					FmiProxyV3.EventInfo info = new FmiProxyV3.EventInfo(true, false, false, false, false, 0.0);
					while (info.newDiscreteStatesNeeded) access.newDiscreteStates(info);
				}
			} else {
				stepSize = fixedStepSize;
				state = access.getState(state);
				currentTime = status.getEndTime();
				System.out.println("Advanced at " + status.getEndTime());
			}
		}
		simulation.terminate();
	}


	@Test
	public void toDelete() {
		Simulation simulation = new Simulation("D:\\bati.fmu");
		simulation.init(0);
		simulation.doStep(2);
	}

	@Test
	public void tank_barrel_case_in_v21() throws Exception {
		assumeTrue(isWin64());
		String path = "/fmu/cosimulation/v3/tank-barrel/";
		List<String> paths = asList(path + "C1.fmu", path + "C2.fmu", path + "TankG.fmu", path + "BarrelG.fmu");
		List<Simulation> simulations = paths.stream().map(this::simOf).collect(toList());
		List<Access> accesses = simulations.stream().map(this::accessOf).collect(toList());
		simulations.forEach(sim -> sim.init(0));
		List<State> states = accesses.stream().map(this::stateOf).collect(toList());
		BigDecimal fixedStepSize = BigDecimal.valueOf(0.1);
		BigDecimal stepSize = new BigDecimal(fixedStepSize.doubleValue());
		BigDecimal currentTime = BigDecimal.valueOf(0);
		System.out.println("Time" + ";" + "C1 Valve" + ";" + "C2 Abort" + ";" + "C2 Size" + ";" + "TANK Valve" + ";" + "TANK outFlow" + ";" + "BARREL Abort" + ";" + "BARREL Size" + ";" + "BARREL inFlow" + ";" + "BARREL Water");
		while (currentTime.doubleValue() < 18d) {
			simulations.get(TANK).write("Valve").with(simulations.get(C1).read("Valve").asBoolean());
			simulations.get(BARREL).write("Abort").with(simulations.get(C2).read("Abort").asBoolean());
			simulations.get(BARREL).write("Size").with(simulations.get(C2).read("Size").asInteger());
			simulations.get(BARREL).write("inFlow").with(simulations.get(TANK).read("outFlow").asDouble());

			final BigDecimal finalStepSize = stepSize, finalCurrentTime = currentTime;
			List<Status> statuses = accesses.stream()
					.map(acc -> statusOf(acc, acc.hybridDoStep(finalCurrentTime.doubleValue(), finalStepSize.doubleValue())))
					.filter(status -> status.occurredEvent)
					.sorted((o1, o2) -> Double.compare(o1.endTime, o2.endTime)).collect(toList());
			if (!statuses.isEmpty()) {
				if (statuses.get(0).endTime < currentTime.add(stepSize).doubleValue()) {
					stepSize = new BigDecimal(statuses.get(0).endTime).subtract(currentTime);
					range(0, 4).forEach(i -> accesses.get(i).setState(states.get(i)));
					System.out.println("Rolling back at " + currentTime +
							". Events on: " + String.join(", ", statuses.stream().map(s -> s.access.getModelName() + "(" + s.endTime + ")").collect(toList())));
				} else {
					currentTime = new BigDecimal(statuses.get(0).endTime);
					stepSize = fixedStepSize.subtract(stepSize);
					print(simulations, currentTime.doubleValue());
					statuses.forEach(status -> {
						FmiProxyV3.EventInfo info = new FmiProxyV3.EventInfo(true, false, false, false, false, 0.0);
						while (info.newDiscreteStatesNeeded) status.access.newDiscreteStates(info);
					});
					range(0, 4).forEach(i -> {
						states.remove(i);
						states.add(i, accesses.get(i).getState(State.newEmptyState()));
					});
					print(simulations, currentTime.doubleValue());
				}
			} else {
				stepSize = fixedStepSize;
				range(0, 4).forEach(i -> {
					states.remove(i);
					states.add(i, accesses.get(i).getState(State.newEmptyState()));
				});
				currentTime = currentTime.add(finalStepSize);
//				System.out.println("Advanced at " + currentTime);
				print(simulations, currentTime.doubleValue());
			}
		}
		simulations.forEach(Simulation::terminate);
	}

	@Test
	public void tank_barrel_case_in_v21_2017() throws Exception {
		assumeTrue(isWin64());
		simulateTankBarrelFrom("/fmu/cosimulation/v3/tank-barrel/");
	}

	@Test
	public void tank_barrel_case_in_v21_2019() throws Exception {
		assumeTrue(isWin64());
		simulateTankBarrelFrom("/fmu/cosimulation/v3/tank-barrel-2019/");
	}

	static class FMUSimulation{
		Simulation simulation;
		Access access;
		State state = null;
		Status ocurredEvent = null;
	}

	private void simulateTankBarrelFrom(String folder) {
		List<String> paths = asList(folder + "C1.fmu", folder + "TankG.fmu",folder + "BarrelG.fmu", folder + "C2.fmu");
		List<FMUSimulation> simulations = new ArrayList<>();
		for (String path : paths) {
			FMUSimulation simulation = new FMUSimulation();
			simulation.simulation = simOf(path);
			simulation.simulation.init(0);
			simulation.access = accessOf(simulation.simulation);
			simulation.state = stateOf(simulation.access);
			simulations.add(simulation);
		}
//		double fixedStepSize = 0.3;
		double fixedStepSize = 0.01;
		double stepSize = fixedStepSize;
		double currentTime = 0;
		System.out.println("Time;C1.Valve;TANK.Valve;TANK.outFlow;C2.Abort;C2.Size;BARREL.Abort;BARREL.Size;BARREL.inFlow;BARREL.Water");
		boolean rollbacked = false;
		while (currentTime < 18) {
			simulations.get(TANK).simulation.write("Valve").with(simulations.get(C1).simulation.read("Valve").asBoolean());
			simulations.get(BARREL).simulation.write("Abort").with(simulations.get(C2).simulation.read("Abort").asBoolean());
			simulations.get(BARREL).simulation.write("Size").with(simulations.get(C2).simulation.read("Size").asInteger());
			simulations.get(BARREL).simulation.write("inFlow").with(simulations.get(TANK).simulation.read("outFlow").asDouble());

			final double finalStepSize = stepSize, finalCurrentTime = currentTime;
			simulations.stream()
					.filter(s -> s.ocurredEvent == null)
					.forEach(s -> {
						Status status = statusOf(s.access, s.access.hybridDoStep(finalCurrentTime, finalStepSize));
						s.ocurredEvent = status.occurredEvent ? status : null;
					});
			if (simulations.stream().anyMatch(s -> s.ocurredEvent != null)) {
				if (!rollbacked) {
					simulations.stream()
							.filter(s -> s.ocurredEvent == null)
							.forEach(s -> s.access.setState(s.state));
					stepSize = simulations.stream().filter(s -> s.ocurredEvent != null).findFirst().get().ocurredEvent.endTime - currentTime; // TODO multi FMU eventsx
					rollbacked = true;
					System.out.println("Rolling back at " + currentTime +
							". Events on: " + simulations.stream().filter(s -> s.ocurredEvent != null).map(s -> s.access.getModelName() + "(" + s.ocurredEvent.endTime + ")").collect(joining(", ")));
				} else {
					currentTime = simulations.stream().filter(s -> s.ocurredEvent != null).findFirst().get().ocurredEvent.endTime; // TODO multi FMU events
					stepSize = fixedStepSize - stepSize;
					rollbacked = false;
//					print(simulations.stream().map(s -> s.simulation).collect(toList()), currentTime);
					simulations.stream().filter(s -> s.ocurredEvent != null).map(s -> s.ocurredEvent).forEach(status -> {
						FmiProxyV3.EventInfo info = new FmiProxyV3.EventInfo(true, false, false, false, false, 0.0);
						while (info.newDiscreteStatesNeeded) status.access.newDiscreteStates(info);
					});
					simulations.forEach(s -> s.access.getState(s.state));
//					print(simulations.stream().map(s -> s.simulation).collect(toList()), currentTime);
					simulations.forEach(s -> s.ocurredEvent = null);
				}
			} else {
				stepSize = fixedStepSize;
				simulations.forEach(s -> s.access.getState(s.state));
				currentTime += finalStepSize;
				System.out.println("Advanced at " + currentTime);
//				print(simulations.stream().map(s -> s.simulation).collect(toList()), currentTime);
			}
		}
		simulations.forEach(s -> s.simulation.terminate());
	}

	private void print(List<Simulation> simulations, double currentTime) {
		System.out.println(currentTime + ";" +
				simulations.get(C1).read("Valve").asDouble() + ";" +
				simulations.get(TANK).read("Valve").asDouble() + ";" +
				simulations.get(TANK).read("outFlow").asDouble() + ";" +
				simulations.get(BARREL).read("Abort").asDouble() + ";" +
				simulations.get(BARREL).read("Size").asDouble() + ";" +
				simulations.get(BARREL).read("inFlow").asDouble() + ";" +
				simulations.get(BARREL).read("Water").asDouble() + ";" +
				simulations.get(C2).read("Abort").asDouble() + ";" +
				simulations.get(C2).read("Size").asDouble());
	}

	@Test
	public void tank() throws Exception {
		Simulation tank = new Simulation(fileOf("/fmu/cosimulation/v3/tank-barrel/TankG.fmu"));
		System.out.println(((ModelDescription) tank.getModelDescription()).getGuid());
		Access tankAccess = new Access(tank);
		tank.init(0);
		tankAccess.hybridDoStep(0.0, 0.1);
		tankAccess.hybridDoStep(0.1, 0.1);
		tankAccess.hybridDoStep(0.2, 0.1);
		tankAccess.hybridDoStep(0.3, 0.1);
		tankAccess.hybridDoStep(0.4, 0.1);
		State tankState = tankAccess.getState(State.newEmptyState());
		System.out.println("Before set Valve value -> " + tank.read("Valve").asBoolean());
		tank.write("Valve").with(true);
		System.out.println("After set Valve value -> " + tank.read("Valve").asBoolean());
		HybridDoStepStatus status = tankAccess.hybridDoStep(0.5, 0.1);
		System.out.println("Event occurred " + status.isOccurredEvent() + " " + status.getEndTime());
		tankAccess.setState(tankState);
		System.out.println("Before set after rolling back Valve value -> " + tank.read("Valve").asBoolean());
		tank.write("Valve").with(true);
		System.out.println("After set and after rolling back Valve value -> " + tank.read("Valve").asBoolean());
		status = tankAccess.hybridDoStep(0.5, 0.1);
		System.out.println("Event occurred " + status.isOccurredEvent() + " " + status.getEndTime());
	}


	private Status statusOf(Access acc, HybridDoStepStatus hybridDoStepStatus) {
		return new Status(acc, hybridDoStepStatus);
	}


	private Access accessOf(Simulation simulation) {
		return new Access(simulation);
	}

	private Simulation simOf(String c1Path) {
		return new Simulation(fileOf(c1Path));
	}

	private String fileOf(String path) {
		return HybridCosimulationTest.class.getResource(path).getFile();
	}


	private State stateOf(Access access) {
		return access.getState(State.newEmptyState());
	}

	private class Status {

		Access access;
		boolean occurredEvent;
		double endTime;

		public Status(Access access, HybridDoStepStatus status) {
			this.access = access;
			this.occurredEvent = status.isOccurredEvent();
			this.endTime = status.getEndTime();
		}
	}
}