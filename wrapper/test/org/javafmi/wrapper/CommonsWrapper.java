/*
 *  Copyright 2013-2018 - Monentia
 *
 * Daccosim is a collaborative development effort between EDF (France),
 *  CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *  and Monentia S.L. (Spain)
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.wrapper;

import java.io.File;
import java.io.IOException;

import static org.javafmi.kernel.OS.isLinux32;
import static org.javafmi.kernel.OS.isWin32;
import static org.junit.Assume.assumeTrue;

public class CommonsWrapper {
    public static final String WINDOWS_32_V1_FMU = "win_32_SFR_CoSimulation_V1.fmu";
    public static final String ALL_PLATFORM_V1_FMU = "bouncingBall.fmu";
    public static final String LINUX_32_FMU = "linux_32_plusun.fmu";
    public static final String LINUX_64_V2_FMU = "linux_64_V2_tankv3.fmu";
    public static final String WINDOWS_32_ENUMERATION_READABLE_FMU = "win_32_GridSysPro_0V1_Examples_Validation_0with_0generator.fmu";
    public static final String INDOOR_HEAT_GAINS = "win_32_64_V2_PackageBuildingCases_Livraison4_01_IndoorHeatGains.fmu";

    public static Simulation createSimulation(String fmuName) {
        return new Simulation(getFmuPath(fmuName));
    }

    public static String getFmuPath(String fmuName) {
        try {
            return new File("../wrapper/test-res/fmu/cosimulation/" + fmuName).getCanonicalPath();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String getFmuNameForThisOS() {
        assumeTrue(isWin32() || isLinux32());
        if (isWin32()) return WINDOWS_32_V1_FMU;
        if (isLinux32()) return LINUX_32_FMU;
        throw new RuntimeException("There is not an FMU for this system");
    }
}
